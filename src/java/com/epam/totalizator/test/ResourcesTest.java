package com.epam.totalizator.test;

import org.junit.Assert;
import org.junit.Test;

import java.util.Locale;
import java.util.ResourceBundle;

public class ResourcesTest {
    @Test
    public void testResources(){
        Assert.assertNotNull(ResourceBundle.getBundle("pages"));
        Assert.assertNotNull(ResourceBundle.getBundle("text"));
        Assert.assertNotNull(ResourceBundle.getBundle("text", new Locale("en_EN")));
    }
}
