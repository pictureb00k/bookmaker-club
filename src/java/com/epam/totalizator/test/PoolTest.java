package com.epam.totalizator.test;

import com.epam.totalizator.pool.ConnectionPool;
import com.epam.totalizator.pool.PoolException;
import org.junit.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class PoolTest {
    private static ConnectionPool pool;

    @BeforeClass
    public static void init(){
        pool = ConnectionPool.getInstance();
    }

    @Test
    public void testProperties(){
        ResourceBundle bundle = ResourceBundle.getBundle("db");
        Assert.assertNotNull(bundle);
        int size = Integer.parseInt(bundle.getString("pool.size"));
        Assert.assertTrue(size > 0);
    }
    @Test
    public void testSingleton(){
        ConnectionPool anotherPool = ConnectionPool.getInstance();
        Assert.assertSame(pool, anotherPool);
    }
    @Test
    public void testConnection(){
        try {
            Connection connection = pool.takeConnection();
            Assert.assertNotNull(connection);
            connection.close();
        }catch (PoolException | SQLException e){
            Assert.fail();
        }
    }
}
