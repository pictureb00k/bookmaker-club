package com.epam.totalizator.test;

import com.epam.totalizator.entity.AccountBill;
import com.epam.totalizator.entity.Event;
import com.epam.totalizator.service.BetService;
import com.epam.totalizator.util.Validator;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class ValidatorTest {
    @Test
    public void testCheckNewAccount(){
        String login = "Login";
        String password = "123456";
        String email = "rizon-paxa@mail.ru";
        String firstName = "Pavel";
        String secondName = "Bortnik";
        Assert.assertTrue(Validator.checkNewAccount(login,password,email,firstName,secondName));
        login = "login";
        password = "1232454";
        email = "asldjfppoij@@asdf";
        firstName = "abra";
        secondName = "cadabra";
        Assert.assertFalse(Validator.checkNewAccount(login,password,email,firstName,secondName));
    }
    @Test
    public void testCheckAvailableMoney(){
        AccountBill accountBill = new AccountBill();
        accountBill.setBalance(new BigDecimal("20"));
        Assert.assertTrue(Validator.checkAvailableMoney(new BigDecimal("5"), accountBill));
        Assert.assertTrue(Validator.checkAvailableMoney(new BigDecimal("20"), accountBill));
        Assert.assertFalse(Validator.checkAvailableMoney(new BigDecimal("20.01"), accountBill));
    }
    @Test
    public void testCheckAge(){
       Date date = Date.valueOf("1996-12-07");
       boolean age = Validator.checkAge(date);
       Assert.assertTrue(age);
       Date date1 = Date.valueOf("2001-07-12");
       Assert.assertFalse(Validator.checkAge(date1));
    }
    @Test
    public void testValidatePassword(){
        String password = "123456";
        String password1 = "12345";
        String password2 = "pavelBortnik123";
        Assert.assertTrue(Validator.checkPassword(password));
        Assert.assertFalse(Validator.checkPassword(password1));
        Assert.assertTrue(Validator.checkPassword(password2));
    }
    @Test
    public void testCheckCoefficient(){
        String coef = "2.5";
        String coef1 = "0.5";
        Assert.assertEquals(coef, Validator.checkCoefficient(coef));
        Assert.assertNull(Validator.checkCoefficient(coef1));
    }
    @Test
    public void testCheckScore(){
        String score = "0";
        String score1 = "lasdjfkl";
        String score3 = "-2";
        Assert.assertEquals(0, Validator.checkScore(score));
        Assert.assertEquals(-1, Validator.checkScore(score1));
        Assert.assertEquals(-1, Validator.checkScore(score3));
    }
    @Test
    public void testCheckNewMatch(){
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.add(Calendar.MONTH, 1);
        calendar.add(Calendar.MINUTE, -1);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);
        StringBuilder date = new StringBuilder();
        date.append(year).append("-").append(month).append("-").append(day);
        StringBuilder time = new StringBuilder();
        time.append(hour).append(":").append(minutes);
        Assert.assertFalse(Validator.checkNewMatch(date.toString(), time.toString()));

        minutes += 3;
        time = new StringBuilder();
        time.append(hour).append(":").append(minutes);

        Assert.assertTrue(Validator.checkNewMatch(date.toString(), time.toString()));
    }

    @Test
    public void testCheckBetTime(){
        Event event = new Event();
        GregorianCalendar calendar = new GregorianCalendar();

        calendar.add(Calendar.MINUTE, -1);
        Date date = new Date(calendar.getTime().getTime());
        event.setStartDate(date);
        event.setStartTime(new Time(date.getTime()));

        Assert.assertFalse(Validator.checkEventTime(event));

        calendar.add(Calendar.MINUTE, 5);
        date = new Date(calendar.getTime().getTime());
        event.setStartDate(date);
        event.setStartTime(new Time(date.getTime()));

        Assert.assertTrue(Validator.checkEventTime(event));
    }
}
