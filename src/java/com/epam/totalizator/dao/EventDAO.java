package com.epam.totalizator.dao;

import com.epam.totalizator.entity.*;
import com.epam.totalizator.pool.ConnectionPool;
import com.epam.totalizator.pool.PoolException;
import com.epam.totalizator.pool.ProxyConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.totalizator.dao.Constants.*;

/**
 * The class (@code EventDAO) is responsible for
 * manipulating the events' data in the (@code event)
 * table.
 * @author Pavel
 * @see Event
 * @see Coefficients
 */

public class EventDAO extends AbstractDAO<Integer, Event> {
    private static final String SQL_SELECT_EVENTS_IS_PLAYED = "SELECT e.id, e.event_type, e.kind_of_sport," +
            " e.description, e.first_competitor, e.second_competitor, e.first_score, e.second_score, e.start_date," +
            " e.coef_id, e.is_played, c.id, c.win_first, c.win_second, c.nobody, c.first_or_nobody, c.second_or_nobody," +
            " c.first_or_second FROM event AS e JOIN coef AS c ON e.coef_id = c.id WHERE e.kind_of_sport = ?" +
            " AND c.id != 0 AND e.is_played = ?";

    private static final String SQL_SELECT_EVENT_BY_ID = "SELECT id, event_type, kind_of_sport, description," +
            " first_competitor, second_competitor, first_score, second_score, start_date, coef_id, is_played" +
            " FROM event WHERE id = ?";

    private static final String SQL_SELECT_EVENTS_OF_TYPE = "SELECT e.id, e.event_type, e.kind_of_sport," +
            " e.description, e.first_competitor, e.second_competitor, e.first_score, e.second_score, e.start_date," +
            " e.coef_id, e.is_played, c.id, c.win_first, c.win_second, c.nobody, c.first_or_nobody, c.second_or_nobody," +
            " c.first_or_second FROM event AS e JOIN coef AS c ON e.coef_id = c.id WHERE e.event_type = ?" +
            " AND c.id != 0 AND e.is_played = 0";

    private static final String SQL_SELECT_ALL = "SELECT e.id, e.event_type, e.kind_of_sport, e.description," +
            " e.first_competitor, e.second_competitor, e.first_score, e.second_score, e.start_date, e.coef_id," +
            " e.is_played, c.id, c.win_first, c.win_second, c.nobody, c.first_or_nobody, c.second_or_nobody," +
            " c.first_or_second FROM event AS e JOIN coef AS c ON e.coef_id = c.id WHERE e.is_played = ? " +
            "AND c.id != 0";
    private static final String SQL_INSERT_EVENT = "INSERT INTO event (event_type, kind_of_sport, description," +
            " first_competitor, second_competitor, start_date)  VALUES (?,?,?,?,?,?)";

    private static final String SQL_SELECT_WITHOUT_COEFFICIENTS = "SELECT id, event_type, kind_of_sport, description," +
            " first_competitor, second_competitor, first_score, second_score, start_date, coef_id, is_played " +
            "from event where coef_id = 0";

    private static final String SQL_UPDATE_EVENT = "UPDATE event SET description = ?, first_competitor = ?," +
            " second_competitor = ?, start_date = ?, kind_of_sport = ? WHERE id = ?";

    private static final String SQL_DELETE_EVENT = "DELETE FROM event WHERE id = ?";

    private static final String SQL_INSERT_RESULTS = "UPDATE event SET first_score = ?, second_score = ?," +
            " is_played = 1 WHERE event.id = ?";


    /**
     * Selects all events which are compiles to the kindOfSport.
     * @param kindOfSport the kind of sport
     * @param isPlayed match is finished or not.
     * @return ArrayList of events.
     * @throws DAOException
     */
    public ArrayList<Event> selectEventsOfKind(String kindOfSport, int isPlayed) throws DAOException{
        ArrayList<Event> events;
        try(
                ProxyConnection connection  = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_SELECT_EVENTS_IS_PLAYED)
        ){
            statement.setString(1, kindOfSport);
            statement.setInt(2, isPlayed);
            ResultSet set = statement.executeQuery();
            events = extractEventsAndCoefficients(set);
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
        return events;
    }

    /**
     * Selects all events which are compiles to the eventType.
     * @param eventType the type of event. Sport or cybersport
     * @return ArrayList of events.
     * @throws DAOException
     */
    public ArrayList<Event> selectEventsOfType(String eventType) throws DAOException{
        ArrayList events;
        try(
                ProxyConnection connection  = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_SELECT_EVENTS_OF_TYPE)
        ){
            statement.setString(1, eventType);
            ResultSet set = statement.executeQuery();
            events = extractEventsAndCoefficients(set);
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
        return events;
    }

    @Override
    public Event selectByKey(Integer eventID) throws DAOException{
        Event event;
        try(
                ProxyConnection connection  = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_SELECT_EVENT_BY_ID)
        ) {
            statement.setInt(1, eventID);
            ResultSet set = statement.executeQuery();
            event = extractEvents(set).get(0);
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
        return event;
    }

    /**
     * Selects all finished or not finished events.
     * @param isPlayed  match is finished or not.
     * @return ArrayList
     * @throws DAOException
     */
    public ArrayList<Event> selectAllEvents(int isPlayed) throws DAOException{
        ArrayList<Event> events;
        try(
                ProxyConnection connection  = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL)
        ){
            statement.setInt(1, isPlayed);
            ResultSet set = statement.executeQuery();
            events = extractEventsAndCoefficients(set);

        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
        return events;
    }

    /**
     * Inserts a new event to the database.
     * @param eventType Sport or Cybersport
     * @param kindOfSport kind of sport
     * @param description description of event
     * @param firstCompetitor first competitor of event
     * @param secondCompetitor second competitor of event
     * @param dateTime start date and time of event
     * @throws DAOException
     */
    public void addNewEvent(String eventType, String kindOfSport, String description, String firstCompetitor,
                            String secondCompetitor, String dateTime) throws DAOException{
        try(
                ProxyConnection connection  = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_INSERT_EVENT)
        ) {
            statement.setString(1, eventType);
            statement.setString(2, kindOfSport);
            statement.setString(3, description);
            statement.setString(4, firstCompetitor);
            statement.setString(5, secondCompetitor);
            statement.setString(6, dateTime);
            statement.executeUpdate();
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
    }

    @Override
    public List<Event> selectAll() throws DAOException {
        return selectMatchesWithoutCoefficients();
    }

    /**
     * Selects all matches that are without coefficients yet.
     * @return
     * @throws DAOException
     */
    public ArrayList<Event> selectMatchesWithoutCoefficients() throws DAOException{
        ArrayList<Event> events;
        try(
                ProxyConnection connection  = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_SELECT_WITHOUT_COEFFICIENTS)
        ){
            ResultSet set = statement.executeQuery();
            events = extractEvents(set);
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
        return events;
    }

    /**
     * Updates the event information with eventID.
     * @param eventID id of the event
     * @param eventType Sport or Cybersport
     * @param kindOfSport kind of sport
     * @param description description of event
     * @param firstCompetitor first competitor of event
     * @param secondCompetitor second competitor of event
     * @param dateTime start date and time of event
     * @throws DAOException
     */
    public void updateEvent(int eventID, String kindOfSport, String description, String firstCompetitor,
                            String secondCompetitor, String dateTime) throws DAOException{
        try(
                ProxyConnection connection  = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_EVENT)
        ){
            statement.setString(1, description);
            statement.setString(2, firstCompetitor);
            statement.setString(3, secondCompetitor);
            statement.setString(4, dateTime);
            statement.setString(5, kindOfSport);
            statement.setInt(6, eventID);
            statement.executeUpdate();
        }catch (PoolException | SQLException e){
            throw new DAOException(e);
        }
    }

    /**
     * Deletes the event with eventID
     * @param eventID id of the event.
     * @throws DAOException
     */
    public void deleteEventByID(int eventID) throws DAOException {
        try(
                ProxyConnection connection  = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_DELETE_EVENT)
        ){
            statement.setInt(1, eventID);
            statement.executeUpdate();
        }catch (PoolException | SQLException e){
            throw new DAOException(e);
        }
    }

    /**
     * Inserts the result of the event with eventID.
     * @param eventID id of the match
     * @param firstScore the score of the first competitor
     * @param secondScore the score of the second competitor
     * @throws DAOException
     */
    public void insertEventResults(int eventID, int firstScore, int secondScore) throws DAOException{
        try(
                ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_INSERT_RESULTS)
        ){
            statement.setInt(1, firstScore);
            statement.setInt(2, secondScore);
            statement.setInt(3, eventID);
            statement.executeUpdate();
        }catch (PoolException | SQLException e){
            throw new DAOException(e);
        }
    }

    private ArrayList<Event> extractEvents(ResultSet set) throws SQLException{
        ArrayList<Event> events = new ArrayList<>();
        Event event;
        while (set.next()) {
            event = new Event();
            event.setEventType(set.getString(EVENT_TYPE));
            event.setKindOfSport(set.getString(KIND_OF_SPORT));
            event.setDescription(set.getString(DESCRIPTION));
            event.setFirstCompetitor(set.getString(FIRST_COMPETITOR));
            event.setSecondCompetitor(set.getString(SECOND_COMPETITOR));
            event.setKindOfSport(set.getString(KIND_OF_SPORT));
            event.setStartDate(set.getDate(START_DATE));
            event.setStartTime(set.getTime(START_DATE));
            event.setFirstScore(set.getInt(FIRST_SCORE));
            event.setSecondScore(set.getInt(SECOND_SCORE));
            event.setId(set.getInt(ID));
            events.add(event);
        }
        return events;
    }
    private ArrayList<Event> extractEventsAndCoefficients(ResultSet set) throws SQLException{
        ArrayList<Event> events = new ArrayList<>();
        Event event;
        Coefficients coefs;
        while (set.next()){
            event = new Event();
            event.setEventType(set.getString(EVENT_TYPE));
            event.setKindOfSport(set.getString(KIND_OF_SPORT));
            event.setDescription(set.getString(DESCRIPTION));
            event.setFirstCompetitor(set.getString(FIRST_COMPETITOR));
            event.setSecondCompetitor(set.getString(SECOND_COMPETITOR));
            event.setKindOfSport(set.getString(KIND_OF_SPORT));
            event.setStartDate(set.getDate(START_DATE));
            event.setStartTime(set.getTime(START_DATE));
            event.setFirstScore(set.getInt(FIRST_SCORE));
            event.setSecondScore(set.getInt(SECOND_SCORE));
            event.setId(set.getInt(ID));
            coefs = new Coefficients();
            coefs.setWinFirst(set.getDouble(WIN_FIRST));
            coefs.setWinSecond(set.getDouble(WIN_SECOND));
            coefs.setFirstOrNobody(set.getDouble(FIRST_OR_NOBODY));
            coefs.setNobody(set.getDouble(NOBODY));
            coefs.setFirstOrSecond(set.getDouble(FIRST_OR_SECOND));
            coefs.setSecondOrNobody(set.getDouble(SECOND_OR_NOBODY));
            event.setCoefficients(coefs);
            events.add(event);
        }
        return events;
    }
}
