package com.epam.totalizator.dao;

import com.epam.totalizator.entity.Bet;
import com.epam.totalizator.entity.Coefficients;
import com.epam.totalizator.entity.Event;
import com.epam.totalizator.pool.ConnectionPool;
import com.epam.totalizator.pool.PoolException;
import com.epam.totalizator.pool.ProxyConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The class (@code CoefficientDAO) is responsible for
 * manipulating the events' coefficients in the (@code coefficient)
 * table.
 * @author Pavel
 * @see Coefficients
 * @see Event
 */

public class CoefficientDAO extends AbstractDAO<Integer, Coefficients> {
    private static final String SQL_INSERT_COEFFICIENTS = "INSERT INTO coef (win_first, win_second, nobody," +
            " first_or_nobody, second_or_nobody, first_or_second) VALUES (?,?,?,?,?,?)";

    private static final String SQL_SELECT_ID = "SELECT LAST_INSERT_ID() FROM coef";

    private static final String SQL_UPDATE_COEFFICIENTS = "UPDATE event AS e JOIN coef AS c ON e.coef_id = c.id" +
            " SET c.win_first=?, c.win_second=?, c.nobody=?, c.first_or_nobody = ?, c.second_or_nobody = ?," +
            " c.first_or_second = ? where e.id = ?";

    private static final String SQL_ARRANGE_COEFFICIENTS = "UPDATE event SET event.coef_id = ? WHERE event.id = ?";

    private static final String SQL_UPDATE_BETS_COEFFICIENTS = "UPDATE bet SET bet_coef = ?, expected_win = ?" +
            " WHERE bet_id = ?";


    /**
     * Inserts the coefficients to the table in the database.
     * @param first First win
     * @param nobody Dead heat
     * @param second Second win
     * @param fon First Or Nobody
     * @param fos First Or Second
     * @param son Second or Nobody
     * @return int the id of inserted row
     * @throws DAOException
     */
    public int insertCoefficients(String first, String nobody, String second,
                                  String fon, String fos, String son) throws DAOException{
        int insertID = -1;
        try(
                ProxyConnection connection  = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_INSERT_COEFFICIENTS);
                PreparedStatement idStatement = connection.prepareStatement(SQL_SELECT_ID)
        ) {
            statement.setString(1, first);
            statement.setString(2, second);
            statement.setString(3, nobody);
            statement.setString(4, fon);
            statement.setString(5, fos);
            statement.setString(6, son);
            statement.executeUpdate();
            ResultSet set = idStatement.executeQuery();
            if(set.next()){
                insertID = set.getInt(1);
            }
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
        return insertID;
    }

    /**
     * Update the coefficients of event with eventId.
     * @param eventId id of the event
     * @param first First win
     * @param nobody Dead heat
     * @param second Second win
     * @param fon First Or Nobody
     * @param fos First Or Second
     * @param son Second or Nobody
     * @return int the id of inserted row
     * @throws DAOException
     */
    public void updateCoefficients(int eventId, String first, String nobody, String second,
                                   String fon, String fos, String son) throws DAOException{
        try(
                ProxyConnection connection  = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_COEFFICIENTS)
        ) {
            statement.setString(1, first);
            statement.setString(2, second);
            statement.setString(3, nobody);
            statement.setString(4, fon);
            statement.setString(5, son);
            statement.setString(6, fos);
            statement.setInt(7, eventId);
            statement.executeUpdate();
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
    }

    /**
     * Arranges the existed coefficients to the match with eventID
     * @param eventID id of the event
     * @param coefficientID id of the coefficients
     * @throws DAOException
     */
    public void arrangeCoefficients(int eventID, int coefficientID) throws DAOException{
        try(
                ProxyConnection connection  = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_ARRANGE_COEFFICIENTS)
        ) {
            statement.setInt(1, coefficientID);
            statement.setInt(2, eventID);
            statement.executeUpdate();
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
    }

    /**
     * Updates the coefficients for bets that have been made already.
     * @param bets
     * @throws DAOException
     */
    public void updateCoefficientForBets(ArrayList<Bet> bets) throws DAOException{
        try(
                ProxyConnection connection  = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_BETS_COEFFICIENTS)
        ) {
            for (Bet bet: bets) {
                statement.setBigDecimal(1, bet.getCoefficient());
                statement.setBigDecimal(2, bet.getExpectedWin());
                statement.setInt(3, bet.getId());
                statement.executeUpdate();
            }
        }catch (PoolException | SQLException e){
            throw new DAOException(e);
        }
    }

    @Override
    public Coefficients selectByKey(Integer key) throws DAOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Coefficients> selectAll() throws DAOException {
        throw new UnsupportedOperationException();
    }
}
