package com.epam.totalizator.dao;

import com.epam.totalizator.pool.ProxyConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Statement;
import java.sql.SQLException;
import java.util.List;

/**
 * Abstract class that describes structure of classes that included in DAO layer
 * @author Pavel
 * @param <K> type of key
 * @param <T> type of entity
 */

public abstract class AbstractDAO<K, T> {
    private static final Logger LOG = LogManager.getLogger(AbstractDAO.class.getName());

    /**
     * Method that selects all entities.
     * @return List
     * @throws DAOException there were problems with database answer.
     */
    public abstract List<T> selectAll() throws DAOException;

    /**
     * Method that finds entity by unique field
     * @param key is any unique field that determines entity
     * @return entity
     * @throws DAOException there were problems with database answer.
     */
    public abstract T selectByKey(K key) throws DAOException;

    /**
     * Method for closing proxy connection
     * @param connection will be closed
     */
    static void closeConnection(ProxyConnection connection) {
        if(connection != null) {
            connection.close();
        }
    }

    /**
     * Method for closing prepared statement
     * @param statement
     */
    static void closeStatement(Statement statement){
        try {
            if(statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            LOG.error(e);
        }
    }
}
