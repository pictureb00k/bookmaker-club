package com.epam.totalizator.dao;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.AccountBill;
import com.epam.totalizator.entity.Bet;
import com.epam.totalizator.entity.Event;
import com.epam.totalizator.pool.ConnectionPool;
import com.epam.totalizator.pool.PoolException;
import com.epam.totalizator.pool.ProxyConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.totalizator.dao.Constants.*;

/**
 * The class (@code BetDAO) is responsible for
 * manipulating the bets' data in the (@code bet)
 * table. Also the class makes transactions between
 * the admin and client bills.
 * @author Pavel
 * @see Bet
 * @see AccountBill
 */
public class BetDAO extends AbstractDAO<Integer, Bet> {
    private static final Logger LOG = LogManager.getLogger(BetDAO.class.getName());

    private static final String SQL_INSERT_BET = "insert into totalizator.bet (account_login, event_id, amount," +
            " expected_win, bet_date, bet_coef, bet_type, bet_currency) values (?,?,?,?, NOW(),?,?,?)";
    private static final String SQL_UPDATE_BILL = "UPDATE bill SET bill.balance = ? WHERE account_login = ?";

    private static final String SQL_ADD_TO_ADMIN = "UPDATE bill SET bill.balance = bill.balance + ? " +
            "WHERE account_login = 'Admin' AND bill_currency = ?";

    private static final String SQL_SUBSTRACT_FROM_ADMIN = "UPDATE bill SET bill.balance = bill.balance - ? WHERE " +
            "account_login = 'Admin' AND bill_currency = ?";

    private static final String SQL_SELECT_EVENTS = "SELECT b.bet_id, b.account_login, b.event_id, b.amount," +
            " b.expected_win, b.result, b.bet_date, b.bet_coef, b.bet_type, b.bet_currency, e.id, e.event_type," +
            " e.kind_of_sport, e.description, e.first_competitor, e.second_competitor, e.first_score, e.second_score," +
            " e.start_date, e.coef_id, e.is_played FROM bet AS b JOIN event AS e ON b.event_id = e.id WHERE" +
            " b.account_login = ? AND b.result = ? ORDER BY b.bet_id";

    private static final String SQL_SELECT_ALL_OF_ACCOUNT = "SELECT b.bet_id, b.account_login, b.event_id, b.amount," +
            " b.expected_win, b.result, b.bet_date, b.bet_coef, b.bet_type, b.bet_currency, e.id, e.event_type," +
            " e.kind_of_sport, e.description, e.first_competitor, e.second_competitor, e.first_score, e.second_score," +
            " e.start_date, e.coef_id, e.is_played FROM bet AS b JOIN event AS e on b.event_id = e.id " +
            "WHERE b.account_login = ?";

    private static final String SQL_SELECT_ALL_BETS = "SELECT bet_id, account_login, event_id, amount, expected_win," +
            " result, bet_date, bet_coef, bet_type, bet_currency FROM bet WHERE event_id = ? AND result = ?";

    private static final String SQL_UPDATE_BETS_RESULTS = "UPDATE bet SET result = ? WHERE account_login = ? " +
            "AND bet_date = ?";

    private static final String SQL_SELECT_BETS_OF_CURRENCY = "SELECT b.bet_id, b.account_login, b.event_id, b.amount," +
            " b.expected_win, b.result, b.bet_date, b.bet_coef, b.bet_type, b.bet_currency, e.id, e.event_type," +
            " e.kind_of_sport, e.description, e.first_competitor, e.second_competitor, e.first_score, e.second_score," +
            " e.start_date, e.coef_id, e.is_played FROM bet AS b JOIN event AS e ON b.event_id = e.id " +
            "WHERE b.bet_currency = ? ORDER BY b.account_login";

    private static final String SQL_DELETE_BET = "DELETE FROM bet WHERE bet_id = ?";

    private static final String SQL_SELECT_BET_BY_ID = "SELECT b.bet_id, b.account_login, b.event_id, b.amount," +
            " b.expected_win, b.result, b.bet_date, b.bet_coef, b.bet_type, b.bet_currency, e.id, e.event_type," +
            " e.kind_of_sport, e.description, e.first_competitor, e.second_competitor, e.first_score, e.second_score," +
            " e.start_date, e.coef_id, e.is_played FROM bet AS b JOIN event AS e ON b.event_id = e.id WHERE b.bet_id = ?";

    private static final String SQL_SELECT_ID = "SELECT LAST_INSERT_ID() FROM bet";

    /**
     * Inserting the information contains three stages which
     * are confirmed as a single transaction. If something goes
     * wrong, the information doesn't commits.
     * @param account
     * @param bet
     * @return int the id of inserted bet
     * @throws DAOException
     */

    public int insertBet(Account account, Bet bet) throws DAOException{
        boolean flag = false;
        int insertID = 0;
        ProxyConnection connection = null;
        PreparedStatement statementBet = null;
        PreparedStatement statementUpdate = null;
        PreparedStatement statementTranslate = null;
        PreparedStatement idStatement = null;
        try{
            connection = ConnectionPool.getInstance().takeConnection();
            connection.setAutoCommit(false);
            statementBet = connection.prepareStatement(SQL_INSERT_BET);
            statementBet.setString(1, account.getLogin());
            statementBet.setInt(2, bet.getEvent().getId());
            statementBet.setBigDecimal(3, bet.getAmount());
            statementBet.setBigDecimal(4, bet.getExpectedWin());
            statementBet.setBigDecimal(5, bet.getCoefficient());
            statementBet.setString(6, bet.getTypeBet());
            statementBet.setString(7, bet.getCurrency());
            statementBet.executeUpdate();

            statementUpdate = connection.prepareStatement(SQL_UPDATE_BILL);
            BigDecimal newBalance = account.getBill().getBalance().subtract(bet.getAmount());
            statementUpdate.setBigDecimal(1, newBalance);
            statementUpdate.setString(2, account.getLogin());
            statementUpdate.executeUpdate();

            statementTranslate = connection.prepareStatement(SQL_ADD_TO_ADMIN);
            statementTranslate.setBigDecimal(1, bet.getAmount());
            statementTranslate.setString(2, bet.getCurrency());
            statementTranslate.executeUpdate();

            idStatement = connection.prepareStatement(SQL_SELECT_ID);
            ResultSet set = idStatement.executeQuery();
            if(set.next()){
                insertID = set.getInt(1);
            }else {
                throw new DAOException("The bet does not inserted.");
            }
            connection.commit();
            flag = true;
            account.getBill().setBalance(newBalance);
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
        finally {
            try {
                if (!flag){
                    connection.rollback();
                }
                connection.setAutoCommit(true);
            }catch (SQLException e){
                LOG.error(e);
            }
            closeStatement(statementBet);
            closeStatement(statementUpdate);
            closeStatement(statementTranslate);
            closeStatement(idStatement);
            closeConnection(connection);
        }
        return insertID;
    }

    /**
     * Deleting the bets contains three stages which
     * are confirmed as a single transaction. If something goes
     * wrong, the information doesn't commits.
     * @param account
     * @param id
     * @param amount
     * @throws DAOException
     */
    public void deleteBet(Account account, int id, BigDecimal amount) throws DAOException {
        boolean flag = false;
        ProxyConnection connection = null;
        PreparedStatement statement = null;
        PreparedStatement statementUpdate = null;
        PreparedStatement statementTranslate = null;
        try{
            connection = ConnectionPool.getInstance().takeConnection();
            connection.setAutoCommit(false);
            statement = connection.prepareStatement(SQL_DELETE_BET);
            statement.setInt(1, id);
            statement.executeUpdate();

            statementUpdate = connection.prepareStatement(SQL_UPDATE_BILL);
            BigDecimal newBalance = account.getBill().getBalance().add(amount);
            statementUpdate.setBigDecimal(1, newBalance);
            statementUpdate.setString(2, account.getLogin());
            statementUpdate.executeUpdate();

            statementTranslate = connection.prepareStatement(SQL_SUBSTRACT_FROM_ADMIN);
            statementTranslate.setBigDecimal(1, amount);
            statementTranslate.setString(2, account.getBill().getCurrency());
            statementTranslate.executeUpdate();

            connection.commit();
            flag = true;
            if (flag) {
                account.getBill().setBalance(newBalance);
            }
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
        finally {
            try {
                if (!flag){
                    connection.rollback();
                }
                connection.setAutoCommit(true);
            }catch (SQLException e){
                LOG.error(e);
            }
            closeStatement(statement);
            closeStatement(statementUpdate);
            closeStatement(statementTranslate);
            closeConnection(connection);
        }
    }

    /**
     * Selects playing bets of Account
     * @param account
     * @return ArrayList
     * @throws DAOException
     */
    public ArrayList<Bet> selectPlayingBetsOfAccount(Account account) throws DAOException{
        ArrayList<Bet> bets;
        bets = selectBetsWithResult(account, NOT_FINISHED_BET);
        return bets;
    }

    /**
     * Selects the winning bets for account.
     * @param account
     * @return ArrayList
     * @throws DAOException
     */
    public ArrayList<Bet> selectWonBetsOfAccount(Account account) throws DAOException{
        ArrayList<Bet> bets;
        bets = selectBetsWithResult(account, WON_BET);
        return bets;
    }

    /**
     * Selects the lost bets for account.
     * @param account
     * @return ArrayList
     * @throws DAOException
     */
    public ArrayList<Bet> selectLostBetsOfAccount(Account account) throws DAOException{
        ArrayList<Bet> bets;
        bets = selectBetsWithResult(account, LOST_BET);
        return bets;
    }

    private ArrayList<Bet> selectBetsWithResult(Account account, int result) throws DAOException{
        ArrayList<Bet> bets;
        try(
                ProxyConnection connection  = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_SELECT_EVENTS)
        ){
            statement.setString(1, account.getLogin());
            statement.setInt(2, result);
            ResultSet set = statement.executeQuery();
            bets = extractBets(set);
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
        return bets;
    }

    /**
     * Selects all bets which where made by account with login.
     * @param login
     * @return ArrayList
     * @throws DAOException
     */
    public ArrayList<Bet> selectAllBetsOfAccount(String login) throws DAOException {
        ArrayList<Bet> bets;
        try(
                ProxyConnection connection  = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_OF_ACCOUNT)
        ){
            statement.setString(1, login);
            ResultSet set = statement.executeQuery();
            bets = extractBets(set);
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
        return bets;
    }

    /**
     * Selects all bets where made on the match with (@code eventID)
     * @param eventID
     * @return ArrayList
     * @throws DAOException
     */
    public ArrayList<Bet> selectAllBetsOnMatch(int eventID) throws DAOException{
        ArrayList<Bet> bets = new ArrayList<>();
        Bet bet;
        try (
                ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_BETS)
                )
        {
            statement.setInt(1, eventID);
            statement.setInt(2, NOT_FINISHED_BET);
            ResultSet set = statement.executeQuery();
            while (set.next()){
                bet = new Bet();
                bet.setId(set.getInt(BET_ID));
                bet.setResult(set.getInt(RESULT));
                bet.setTypeBet(set.getString(BET_TYPE));
                bet.setAmount(set.getBigDecimal(AMOUNT));
                bet.setCoefficient(set.getBigDecimal(BET_COEFFICIENT));
                bet.setExpectedWin(set.getBigDecimal(EXPECTED_WIN));
                bet.setDateOfMatch(set.getString(BET_DATE));
                bet.setAccountLogin(set.getString(ACCOUNT_LOGIN));
                bet.setCurrency(set.getString(CURRENCY  ));
                bets.add(bet);
            }
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
        return bets;
    }

    /**
     * Updates the information, especially coefficients,
     * of bets if they have been changed.
     * @param allBetsOfMatch
     * @throws DAOException
     */
    public void updateBetsResults(ArrayList<Bet> allBetsOfMatch) throws DAOException{
        try(
                ProxyConnection connection  = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_BETS_RESULTS)
        ){
            for (Bet b: allBetsOfMatch){
                statement.setInt(1, b.getResult());
                statement.setString(2, b.getAccountLogin());
                statement.setString(3, b.getDateOfMatch());
                statement.executeUpdate();
            }
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
    }

    /**
     * Selects all bets which were made by accounts with the bills
     * of (@code currency)
     * @param currency
     * @return ArrayList
     * @throws DAOException
     */
    public ArrayList<Bet> selectBetsOfCurrency(String currency) throws DAOException{
        ArrayList<Bet> bets;
        try(
                ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BETS_OF_CURRENCY)
        ){
            statement.setString(1, currency);
            ResultSet set = statement.executeQuery();
            bets = extractBets(set);
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
        return bets;
    }

    @Override
    public Bet selectByKey(Integer id) throws DAOException {
        Bet bet;
        try(
                ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BET_BY_ID)
        ){
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();
            bet = extractBets(set).get(0);
        }catch (PoolException | SQLException e){
            throw new DAOException(e);
        }
        return bet;
    }

    @Override
    public List<Bet> selectAll() throws DAOException {
        throw new UnsupportedOperationException();
    }

    private ArrayList<Bet> extractBets(ResultSet set) throws SQLException{
        ArrayList<Bet> bets = new ArrayList<>();
        Bet bet;
        Event event;
        while (set.next()){
            bet = new Bet();
            event = new Event();
            event.setFirstCompetitor(set.getString(FIRST_COMPETITOR));
            event.setSecondCompetitor(set.getString(SECOND_COMPETITOR));
            event.setFirstScore(set.getInt(FIRST_SCORE));
            event.setSecondScore(set.getInt(SECOND_SCORE));
            event.setStartDate(set.getDate(START_DATE));
            event.setStartTime(set.getTime(START_DATE));
            event.setKindOfSport(set.getString(KIND_OF_SPORT));
            bet.setEvent(event);
            bet.setTypeBet(set.getString(BET_TYPE));
            bet.setExpectedWin(set.getBigDecimal(EXPECTED_WIN));
            bet.setAccountLogin(set.getString(ACCOUNT_LOGIN));
            bet.setBetDate(set.getDate(BET_DATE));
            bet.setBetTime(set.getTime(BET_DATE));
            bet.setAmount(set.getBigDecimal(AMOUNT));
            bet.setCoefficient(set.getBigDecimal(BET_COEFFICIENT));
            bet.setResult(set.getInt(RESULT));
            bet.setId(set.getInt(BET_ID));
            bets.add(0,bet);
        }
        return bets;
    }
}
