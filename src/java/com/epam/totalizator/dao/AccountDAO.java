package com.epam.totalizator.dao;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.AccountBill;
import com.epam.totalizator.pool.ConnectionPool;
import com.epam.totalizator.pool.PoolException;
import com.epam.totalizator.pool.ProxyConnection;
import com.mysql.jdbc.*;

import java.sql.*;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.util.ArrayList;

import static com.epam.totalizator.dao.Constants.*;

/**
 * The class (@code AccountDAO) provides the methods of
 * manipulating the accounts' data in the (@code account)
 * table.
 * @author Pavel
 * @see Account
 * @see AccountBill
 */
public class AccountDAO extends AbstractDAO<String, Account> {
    private static final String SQL_INSERT_ACCOUNT = "INSERT INTO account " +
            "(login, password, email, birthday, creating_date, role, first_name, last_name)" +
            "VALUES(?,?,?,?,NOW(),?,?,?)";
    private static final String SQL_SELECT_ACCOUNT = "SELECT a.login, a.password, a.email, a.birthday, a.creating_date,"+
            " a.role, a.first_name, a.last_name, b.id, b.bill_currency, b.balance FROM account AS a JOIN bill as b " +
            "ON a.login = b.account_login WHERE a.login = ? AND a.password = ?";
    private static final String SQL_INSERT_BILL = "INSERT INTO bill (bill_currency, account_login)" +
            "VALUES (?,?)";
    private static final String SQL_CHECK_IF_EXIST = "SELECT login FROM account WHERE login = ?";
    private static final String SQL_UPDATE_PASSWORD = "UPDATE account SET password = ? where login = ?";
    private static final String SQL_SELECT_ALL_ACCOUNTS = "SELECT login, role FROM account";

    @Override
    public Account selectByKey(String login) throws DAOException{
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_CHECK_IF_EXIST);)
        {
            statement.setString(1, login);
            ResultSet set = statement.executeQuery();
            if (set.next()){
                return new Account();
            }else {
                return null;
            }
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
    }

    /**
     * Authenticate the login and password. Checks if it is
     * in the database. If the database contains such account and
     * this account has (@code Client) rights, the method also
     * initializes the (@code AccountBill).
     * @param login the login of account
     * @param password the password of account
     * @return Account returns the full information about account
     * if it exists, else returns null
     * @throws DAOException
     */
    public Account authenticateAccount(String login, String password) throws DAOException{
        Account account = null;
        AccountBill bill;
        try(ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ACCOUNT))
        {
            statement.setString(1, login);
            statement.setString(2, password);
            ResultSet set = statement.executeQuery();
            if (set.next()){
                account = new Account();
                account.setLogin(set.getString(LOGIN));
                account.setPassword(set.getString(PASSWORD));
                account.setEmail(set.getString(EMAIL));
                account.setFirstName(set.getString(FIRST_NAME));
                account.setLastName(set.getString(LAST_NAME));
                account.setBirthday(set.getDate(BIRTHDAY));
                account.setRegDate(set.getDate(CREATING_DATE));
                account.setRole(Account.Role.valueOf(set.getString(ROLE)));
                if(account.getRole().equals(Account.Role.CLIENT)) {
                    bill = new AccountBill();
                    bill.setCurrency(set.getString(BILL_CURRENCY));
                    bill.setId(set.getInt(BILL_ID));
                    bill.setBalance(set.getBigDecimal(BALANCE));
                    account.setBill(bill);
                }
            }
        }catch (SQLException | PoolException e){
            throw new DAOException("Authenticate account exception: " + e);
        }
        return account;
    }

    /**
     * Inserts the new account ot the database. Has two stages.
     * The first stage is inserting the account and the second
     * stage is inserting the bill information.
     * @param account
     * @return boolean result of inserting.
     * @throws DAOException
     */
    public void insertAccount(Account account) throws DAOException{
        try(ProxyConnection connection =  ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_ACCOUNT);
            PreparedStatement billStatement = connection.prepareStatement(SQL_INSERT_BILL))
        {
            statement.setString(1, account.getLogin());
            statement.setString(2, account.getPassword());
            statement.setString(3, account.getEmail());
            statement.setDate(4, account.getBirthday());
            statement.setString(5, account.getRole().toString());
            statement.setString(6, account.getFirstName());
            statement.setString(7, account.getLastName());

            billStatement.setString(1, account.getBill().getCurrency());
            billStatement.setString(2, account.getLogin());

            statement.executeUpdate();
            billStatement.executeUpdate();
        }catch (SQLException | PoolException e){
            throw new DAOException("Creating new account Exception: " + e);
        }
    }

    /**
     * Updates the password of the account with login.
     * @param newPassword
     * @param login
     * @return boolean
     * @throws DAOException
     */
    public boolean updatePassword(String newPassword, String login) throws DAOException{
        boolean flag;
        try(ProxyConnection connection =  ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_PASSWORD))
        {
            statement.setString(1, newPassword);
            statement.setString(2, login);
            statement.executeUpdate();
            flag = true;
        }catch (SQLException | PoolException e){
            throw new DAOException("Updating password Exception" + e);
        }
        return flag;
    }

    @Override
    public ArrayList<Account> selectAll()  throws DAOException{
        ArrayList<Account> list = new ArrayList<>();
        try(ProxyConnection connection =  ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_ACCOUNTS))
        {
            ResultSet set = statement.executeQuery();
            Account account;
            while (set.next()){
                if (Account.Role.valueOf(set.getString(ROLE)) == Account.Role.CLIENT){
                    account = new Account();
                    account.setLogin(set.getString(LOGIN));
                    list.add(account);
                }
            }
            return list;
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
    }
}
