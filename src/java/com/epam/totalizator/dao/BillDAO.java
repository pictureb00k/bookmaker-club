package com.epam.totalizator.dao;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.AccountBill;
import com.epam.totalizator.entity.Bet;
import com.epam.totalizator.entity.Transaction;
import com.epam.totalizator.pool.ConnectionPool;
import com.epam.totalizator.pool.PoolException;
import com.epam.totalizator.pool.ProxyConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.totalizator.dao.Constants.*;

/**
 * The class (@code BillDAO) is responsible for
 * manipulating the bills' data in the (@code bill)
 * table.
 * @author Pavel
 * @see Account
 * @see AccountBill
 * @see Transaction
 */

public class BillDAO extends AbstractDAO<Integer, AccountBill>{
    private static final Logger LOG = LogManager.getLogger(BillDAO.class.getName());
    private static final String SQL_SELECT_DEPOSITS = "SELECT t.date, t.type, t.amount, t.id, t.bill_id, t.trans_type" +
            " FROM totalizator.transaction AS t WHERE t.bill_id = ? AND t.trans_type = ?";

    private static final String SQL_SELECT_ALL = "SELECT t.date, t.type, t.amount, t.id, t.bill_id, t.trans_type" +
            " FROM totalizator.transaction AS t WHERE t.bill_id = ?";

    private static final String SQL_SELECT_BILLS = "SELECT id, bill_currency, balance, account_login from bill" +
            " where account_login = 'Admin'";

    private static final String SQL_UPDATE_WON_BILLS = "UPDATE bill SET balance = balance + ? where account_login = ?";

    private static final String SQL_UPDATE_ADMIN_BILL = "UPDATE bill SET balance = balance - ? where account_login = 'Admin' " +
            "AND bill_currency = ?";

    private static final String SQL_REFILL_BILL = "UPDATE bill set balance = balance + ? where account_login = ?";

    private static final String SQL_REDUCE_BILL = "UPDATE bill set balance = balance - ? where account_login = ?";

    private static final String SQL_INSERT_TRANSACTION = "INSERT INTO totalizator.transaction (date, type, amount, bill_id, trans_type) " +
            "VALUES (NOW(), ?, ?, ?, ?)";


    /**
     * Selects the information about deposits that were made
     * by (@code account).
     * @param account
     * @return ArrayList of Transactions
     * @throws DAOException
     */
    public ArrayList<Transaction> selectAccountDeposits(Account account) throws DAOException{
        ArrayList<Transaction> result;
        int id = account.getBill().getId();
        try(
                ProxyConnection connection  = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_SELECT_DEPOSITS)
        ) {
            statement.setInt(1, id);
            statement.setString(2, Transaction.TransactionType.DEPOSIT.getValue());
            ResultSet set = statement.executeQuery();
            result = extractTransactions(set);
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
        return result;
    }

    /**
     * Selects the information about withdraws that were made
     * by (@code account).
     * @param account
     * @return ArrayList of Transactions
     * @throws DAOException
     */
    public ArrayList<Transaction> selectAccountWithdraws(Account account)throws DAOException {
        ArrayList<Transaction> result;
        int id = account.getBill().getId();
        try(
                ProxyConnection connection  = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_SELECT_DEPOSITS)
        ) {
            statement.setInt(1, id);
            statement.setString(2, Transaction.TransactionType.WITHDRAW.getValue());
            ResultSet set = statement.executeQuery();
            result = extractTransactions(set);
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
        return result;
    }

    /**
     * Selects the information about transactions that were made
     * by (@code account).
     * @param account
     * @return ArrayList of Transactions
     * @throws DAOException
     */
    public ArrayList<Transaction> selectAllTransactions(Account account) throws DAOException {
        ArrayList<Transaction> result;
        int id = account.getBill().getId();
        try(
                ProxyConnection connection  = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL)
        ) {
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();
            result = extractTransactions(set);
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
        return result;
    }

    private ArrayList<Transaction> extractTransactions(ResultSet set) throws SQLException{
        ArrayList<Transaction> result = new ArrayList<>();
        Transaction transaction;
        while (set.next()){
            transaction = new Transaction();
            transaction.setDate(set.getDate(DATE));
            transaction.setTime(set.getTime(DATE));
            transaction.setDepositType(set.getString(TYPE));
            transaction.setAmount(set.getBigDecimal(AMOUNT));
            transaction.setTransactionType(set.getString(TRANSACTION_TYPE));
            result.add(0,transaction);
        }
        return result;
    }

    @Override
    public List<AccountBill> selectAll() throws DAOException {
        return takeAdminBills();
    }

    @Override
    public AccountBill selectByKey(Integer key) throws DAOException {
        throw new UnsupportedOperationException();
    }

    /**
     * Selects the information about bills that admin has.
     * @return ArrayList of bills.
     * @throws DAOException
     */
    public ArrayList<AccountBill> takeAdminBills()throws DAOException {
        ArrayList<AccountBill> bills = new ArrayList<>();
        try(
                ProxyConnection connection  = ConnectionPool.getInstance().takeConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BILLS);
        ) {
            ResultSet set = statement.executeQuery();
            while (set.next()){
                AccountBill bill = new AccountBill();
                bill.setCurrency(set.getString(BILL_CURRENCY));
                bill.setBalance(set.getBigDecimal(BALANCE));
                bills.add(bill);
            }
        }
        catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
        return bills;
    }

    /**
     * Update bills that belong to accounts that made winning bets.
     * @param wonBets list of winning bets
     * @throws DAOException
     */
    public void updateWonBills(ArrayList<Bet> wonBets) throws DAOException{
        boolean flag = false;
        ProxyConnection connection  = null;
        PreparedStatement statement = null;
        PreparedStatement updStatement = null;
        try {
            connection = ConnectionPool.getInstance().takeConnection();
            statement = connection.prepareStatement(SQL_UPDATE_WON_BILLS);
            updStatement = connection.prepareStatement(SQL_UPDATE_ADMIN_BILL);
            connection.setAutoCommit(false);
            for (Bet b: wonBets) {
                statement.setBigDecimal(1, b.getExpectedWin());
                statement.setString(2, b.getAccountLogin());
                statement.executeUpdate();
                updStatement.setBigDecimal(1, b.getExpectedWin());
                updStatement.setString(2, b.getCurrency());
                updStatement.executeUpdate();
            }
            connection.commit();
            flag = true;
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
        finally {
            try{
                if (!flag){
                    connection.rollback();
                }
                connection.setAutoCommit(true);
            }catch (SQLException e){
                LOG.error(e);
            }
            closeStatement(statement);
            closeStatement(updStatement);
            closeConnection(connection);
        }
    }

    /**
     * The method which update the bills information when the account with login
     * made a deposit.
     * @param account
     * @param type type of transaction. Deposit or withdraw.
     * @param amount
     * @return boolean
     * @throws DAOException
     */
    public boolean makeDeposit(Account account, String type, BigDecimal amount) throws DAOException{
        boolean flag = false;
        ProxyConnection connection = null;
        PreparedStatement statementBill = null;
        PreparedStatement statementDeposit = null;
        try{
            connection = ConnectionPool.getInstance().takeConnection();
            connection.setAutoCommit(false);
            statementBill = connection.prepareStatement(SQL_REFILL_BILL);
            statementBill.setString(1, amount.toString());
            statementBill.setString(2, account.getLogin());
            statementDeposit = connection.prepareStatement(SQL_INSERT_TRANSACTION);
            statementDeposit.setString(1, type);
            statementDeposit.setString(2, amount.toString());
            statementDeposit.setString(3, String.valueOf(account.getBill().getId()));
            statementDeposit.setString(4, Transaction.TransactionType.DEPOSIT.getValue());
            statementBill.executeUpdate();
            statementDeposit.executeUpdate();
            connection.commit();
            flag = true;
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }
        finally {
            try{
                if (!flag){
                    connection.rollback();
                }
                connection.setAutoCommit(true);
            }catch (SQLException e){
                LOG.error(e);
            }

            closeStatement(statementDeposit);
            closeStatement(statementBill);
            closeConnection(connection);
        }
        return flag;
    }

    /**
     * The method which update the bills information when the account with login
     * made a withdraw.
     * @param account
     * @param type type of transaction. Deposit or withdraw.
     * @param amount
     * @return boolean
     * @throws DAOException
     */
    public boolean withdraw(Account account, String type, BigDecimal amount) throws DAOException{
        boolean flag = false;
        ProxyConnection connection = null;
        PreparedStatement statementBill = null;
        PreparedStatement statementWithdraw = null;
        try{
            connection = ConnectionPool.getInstance().takeConnection();
            statementBill = connection.prepareStatement(SQL_REDUCE_BILL);
            statementWithdraw = connection.prepareStatement(SQL_INSERT_TRANSACTION);
            connection.setAutoCommit(false);

            statementBill.setString(1, amount.toString());
            statementBill.setString(2, account.getLogin());

            statementWithdraw.setString(1, type);
            statementWithdraw.setString(2, amount.toString());
            statementWithdraw.setString(3, String.valueOf(account.getBill().getId()));
            statementWithdraw.setString(4, Transaction.TransactionType.WITHDRAW.getValue());
            statementBill.executeUpdate();
            statementWithdraw.executeUpdate();
            connection.commit();
            flag = true;
        }catch (SQLException | PoolException e){
            throw new DAOException(e);
        }finally {
            try {
                if (!flag){
                    connection.rollback();
                }
                connection.setAutoCommit(true);
            }catch (SQLException e){
                LOG.error(e);
            }
            closeStatement(statementBill);
            closeStatement(statementWithdraw);
            closeConnection(connection);
        }
        return flag;
    }
}
