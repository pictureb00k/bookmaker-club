package com.epam.totalizator.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * Created by Pavel on 01.10.2016.
 */
@SuppressWarnings("serial")
public class InfoTag extends TagSupport{
        private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
        public int doStartTag() throws JspException {
        if(!message.equals("")){
           return EVAL_BODY_INCLUDE;
        }else {
            return SKIP_BODY;
        }
        }
        @Override
        public int doEndTag() throws JspException {
            return EVAL_PAGE;
        }
}
