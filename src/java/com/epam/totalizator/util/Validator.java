package com.epam.totalizator.util;

import com.epam.totalizator.entity.AccountBill;
import com.epam.totalizator.entity.Event;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Pattern;

/**
 * The class (@code Validator) checks all input information
 * @author Pavel
 */

public class Validator {
    private static final String LOGIN_REGEX = "[A-z0-9_-]{3,16}";
    private static final String PASSWORD_REGEX = "[A-z0-9_-]{6,18}";
    private static final String EMAIL_REGEX = "[A-z0-9_\\.-]+@[a-z]+\\.[a-z]{2,4}";
    private static final String FIRST_NAME_REGEX = "[A-zА-я]+";
    private static final String LAST_NAME_REGEX = "[A-zА-я]+";
    private static final String COEFFICIENT = "\\d+([.,])?\\d{0,2}";
    private static final String SCORE = "\\d{1,4}";
    private static final double MIN_COEFFICIENT = 1.01;
    private static final int MIN_AGE = 18;
    private static final String MINIMUM_AMOUNT = "0.01";

    /**
     * Checks input information about new account using
     * regex.
     * @param login LOGIN_REGEX
     * @param password PASSWORD_REGEX
     * @param email EMAIL_REGEX
     * @param firstName FIRST_NAME_REGEX
     * @param lastName LAST_NAME_REGEX
     * @return boolean true, if the information is correct
     */
    public static boolean checkNewAccount(String login, String password, String email, String firstName,
                                          String lastName){
        boolean flag;
        flag = Pattern.matches(LOGIN_REGEX, login)
                && Pattern.matches(PASSWORD_REGEX, password)
                && Pattern.matches(EMAIL_REGEX, email)
                && Pattern.matches(FIRST_NAME_REGEX, firstName)
                && Pattern.matches(LAST_NAME_REGEX, lastName);
        return flag;
    }

    /**
     * Checks the age of new account.
     * @param date birthday
     * @return boolean true, if the age is over than MIN_AGE
     */
    public static boolean checkAge(Date date) {
        java.util.Date currentDate = new java.util.Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        calendar.setTime(date);
        int birthdayYear = calendar.get(Calendar.YEAR);
        int birthdayMonth = calendar.get(Calendar.MONTH);
        int birthdayDay = calendar.get(Calendar.DAY_OF_MONTH);
        GregorianCalendar bday = new GregorianCalendar(birthdayYear, birthdayMonth, birthdayDay);
        GregorianCalendar current = new GregorianCalendar(year, month, day);
        int years = calculateYears(bday, current);
        return years >= MIN_AGE;
    }

    private static int calculateYears(GregorianCalendar bday, GregorianCalendar current) {
        int years = current.get(Calendar.YEAR) - bday.get(Calendar.YEAR);
        int currentMonth = current.get(Calendar.MONTH);
        int bdayMonth = bday.get(Calendar.MONTH);
        if (bdayMonth > currentMonth){
            years--;
        }else if(bdayMonth == currentMonth && bday.get(Calendar.DAY_OF_MONTH) > current.get(Calendar.DAY_OF_MONTH)){
            years--;
        }
        return years;
    }

    /**
     * Checks the password
     * @param newPassword password
     * @return boolean true, if the password is correct
     */
    public static boolean checkPassword(String newPassword) {
        return Pattern.matches(PASSWORD_REGEX, newPassword);
    }

    /**
     * Check the coefficient using the COEFFICIENT_REGEX.
     * Example: "2.32" is a correct coefficient
     * @param coefficient coefficient to check
     * @return boolean
     */
    public static String checkCoefficient(String coefficient){
        if(Pattern.matches(COEFFICIENT, coefficient) && Double.parseDouble(coefficient) >= MIN_COEFFICIENT){
            return coefficient;
        }else {
            return null;
        }
    }

    /**
     * Checks if the account has more than amount money on the bill
     * @param amount need amount
     * @param bill account bill
     * @return boolean
     */
    public static boolean checkAvailableMoney(BigDecimal amount, AccountBill bill){
        if (amount.compareTo(new BigDecimal(MINIMUM_AMOUNT)) < 0){
            return false;
        }
        BigDecimal balance =  bill.getBalance();
        return balance.compareTo(amount) >= 0;
    }

    /**
     * Checks the score using SCORE regex and parses it
     * if it correct score.
     * @param score score to check
     * @return int score, if it is correct and -1 if it is not
     */
    public static int checkScore(String score) {
        if (Pattern.matches(SCORE, score)){
            return Integer.parseInt(score);
        }else {
            return -1;
        }
    }

    /**
     * Checks a new match time. It shouldn't be earlier than the date
     * of adding this match.
     * @param date match date
     * @param time match time
     * @return boolean
     */
    public static boolean checkNewMatch(String date, String time){
        GregorianCalendar current = new GregorianCalendar();

        Calendar calendar = Calendar.getInstance();
        Date matchDate = Date.valueOf(date);
        calendar.setTime(matchDate);
        String[] values = time.split(":");

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = Integer.parseInt(values[0]);
        int minutes = Integer.parseInt(values[1]);

        GregorianCalendar match = new GregorianCalendar(year, month, day, hour, minutes);

        return match.after(current);
    }

    /**
     * Checks if the current date for the event is earlier
     * than the match date
     * @param event event to check
     * @return boolean
     */
    public static boolean checkEventTime(Event event) {
        GregorianCalendar current = new GregorianCalendar();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(event.getStartDate());
        String[] values = event.getStartTime().toString().split(":");

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = Integer.parseInt(values[0]);
        int minutes = Integer.parseInt(values[1]);

        GregorianCalendar match = new GregorianCalendar(year, month, day, hour, minutes);
        return match.after(current);
    }
}
