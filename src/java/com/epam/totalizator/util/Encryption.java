package com.epam.totalizator.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Class (@Encryption) is util class that
 * encrypts the string to the MD5
 * @author Pavel
 */
public class Encryption {
    private static final Logger LOG = LogManager.getLogger(Encryption.class.getName());

    /**
     * Converts the string to MD5.
     * @param value string to convert
     * @return String converted string
     */
    public static String encrypt(String value) {
        String password = null;
        if(value == null) {return null;}
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(value.getBytes(), 0, value.length());
            password = new BigInteger(1, digest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            LOG.error(e);
        }
        return password;
    }
}
