package com.epam.totalizator.pool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The class (@code ConnectionPool) realizes such design pattern as thread-safe Singleton.
 * Opening and maintaining a database connection for each user, especially requests
 * made to a dynamic database-driven website application, is costly and wastes resources.
 * In pooling, after a connection is created, it is placed in the (@code ConnectionPool) and it is used again
 * so that a new connection does not have to be established. If all the connections are being used,
 * a new connection is made and is added to the pool. Pool is realized using a (@Code ArrayBlockingQueue).
 * @see ArrayBlockingQueue
 * @author Pavel Bortnik
 */

public class ConnectionPool {
    private static final Logger LOG = LogManager.getLogger(ConnectionPool.class.getName());
    private static ConnectionPool instance;
    private static AtomicBoolean isCreated = new AtomicBoolean(false);
    private static AtomicBoolean isClosed = new AtomicBoolean(false);
    private static Lock lock = new ReentrantLock();
    private final Properties properties;
    private final String URL;
    private int size;
    private final int WAITING_TIME;
    private BlockingQueue<ProxyConnection> connections;

    /**
     * Constructor initializes the properties of the connection, registers driver,
     * creates and adds to queue connections.
     * @throws RuntimeException if there were fatal problems with creating pool
     */
    private ConnectionPool() {
        ResourceBundle bundle = ResourceBundle.getBundle("db");
        size = Integer.parseInt(bundle.getString("pool.size"));
        URL = bundle.getString("db.url");
        WAITING_TIME = Integer.parseInt(bundle.getString("pool.waitingTime"));

        properties = new Properties();
        properties.put("user", bundle.getString("db.user"));
        properties.put("password", bundle.getString("db.pass"));
        properties.put("autoReconnect",  bundle.getString("db.autoReconnect"));
        properties.put("characterEncoding", bundle.getString("db.encoding"));
        properties.put("useUnicode", bundle.getString("db.useUnicode"));
        properties.put("useSSL", bundle.getString("db.useSSL"));

        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (SQLException e) {
            LOG.fatal("Fatal error when registering DriverManager");
            throw new RuntimeException("Error while driver register.");
        }
        connections = new ArrayBlockingQueue<>(size);
        for (int i = 0; i < size; i++) {
            try {
                addNewConnection();
            } catch (PoolException e){
                LOG.error(e);
            }
        }
        if (connections.isEmpty()) {
            LOG.fatal("Fatal error when creating Connection Pool.");
            throw new RuntimeException("Error in pool creation");
        }
    }

    /**
     * Thread-safe getting ot the single pool. Thread-safe is
     * provided by Double checked locking.
     * @return ConnectionPool
     */
    public static ConnectionPool getInstance() {
        if (!isCreated.get()) {
            lock.lock();
            if (instance == null) {
                try {
                    instance = new ConnectionPool();
                    isCreated.getAndSet(true);
                } finally {
                    lock.unlock();
                }
            }
        }
        return instance;
    }

    /**
     * Establishes a new connection to the database, adds it
     * to the list of opened connections.
     * @return ProxyConnection connection that is established
     * @throws PoolException
     */
    private void addNewConnection() throws PoolException{
        try {
            Connection connection = DriverManager.getConnection(URL, properties);
            ProxyConnection proxyConnection = new ProxyConnection(connection);
            connections.add(proxyConnection);
        } catch (SQLException e) {
            LOG.error("Connection is not created");
            throw new PoolException(e);
        }
    }

    /**
     * Takes a connection from the pool. If all the connections are being used,
     * a new connection is made and is added to the pool.
     * @return ProxyConnection connection is taken
     * @throws PoolException
     */
    public ProxyConnection takeConnection() throws PoolException {
        if (!isClosed.get()) {
            try {
                ProxyConnection connection = connections.poll(WAITING_TIME, TimeUnit.SECONDS);
                if (connection == null) {
                    connection = expandPoolAndTakeNewConnection();
                }
                return connection;
            } catch (InterruptedException e) {
                throw new PoolException("Interrupted.");
            }
        }
        throw new PoolException("Pool is not active.");
    }

    private ProxyConnection expandPoolAndTakeNewConnection() throws InterruptedException, PoolException{
        lock.lock();
        try {
            size++;
            ArrayBlockingQueue<ProxyConnection> expanded = new ArrayBlockingQueue<>(size);
            if (!connections.isEmpty()) {
                connections.drainTo(expanded);
            }
            connections = expanded;
            addNewConnection();
            return connections.take();
        } finally {
            lock.unlock();
        }
    }

    /**
     * Returns the connection back to the list of connections.
     * @param connection to be retruned
     * @throws PoolException
     */
    void returnConnection(ProxyConnection connection) throws PoolException{
        if (connection != null) {
            connections.add(connection);
        }else {
            throw new PoolException("Broken connection");
        }
    }

    /**
     * Closes all connections and destroy the pool.
     */
    public void closePool(){
        try {
            while (!connections.isEmpty()) {
                takeConnection().closeConnection();
            }
            instance = null;
            connections = null;
            isClosed.getAndSet(true);
        }catch (PoolException | SQLException e){
            LOG.error(e);
        }
    }
}
