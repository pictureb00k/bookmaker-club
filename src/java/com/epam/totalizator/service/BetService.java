package com.epam.totalizator.service;

import com.epam.totalizator.dao.BetDAO;
import com.epam.totalizator.dao.CoefficientDAO;
import com.epam.totalizator.dao.DAOException;
import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.Bet;
import com.epam.totalizator.entity.Event;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static com.epam.totalizator.service.Constants.*;

/**
 * Services bets.
 * @author Pavel Bortnik
 */
public class BetService {
    private static final int SCALE = 2;

    private BetDAO betDAO;
    private CoefficientDAO coefficientDAO;

    public BetService() {
        betDAO = new BetDAO();
        coefficientDAO = new CoefficientDAO();
    }

    /**
     * Services and make new bet.
     * @param account account that is making bet
     * @param type type of bet
     * @param coef coefficient
     * @param amount amount of bet
     * @param event event to be bet
     * @return Bet serviced bet
     * @throws ServiceException
     */
    public Bet makeNewBet(Account account, String type, BigDecimal coef,
                           BigDecimal amount, Event event) throws ServiceException{
        Bet bet;
        try {
            bet = serviceNewBet(account, type, event, coef, amount);
            int betId = betDAO.insertBet(account, bet);
            bet.setId(betId);
            account.addBet(bet);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
        return bet;
    }

    /**
     * Takes all won bets of account.
     * @param account take for
     * @return ArrayList of won bets
     * @throws ServiceException
     */
    public ArrayList<Bet> takeWonBets(Account account) throws ServiceException{
        try {
            return betDAO.selectWonBetsOfAccount(account);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    /**
     * Takes all lost bets of account.
     * @param account take for
     * @return ArrayList of lost bets
     * @throws ServiceException
     */
    public ArrayList<Bet> takeLostBets(Account account) throws ServiceException{
        try {
            return betDAO.selectLostBetsOfAccount(account);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    /**
     * Takes all lost and won bets of account.
     * @param account take for
     * @return ArrayList of lost and won bets
     * @throws ServiceException
     */
    public ArrayList<Bet> takeWonAndLostBets(Account account) throws ServiceException{
        ArrayList<Bet> result;
        try{
            result = betDAO.selectAllBetsOfAccount(account.getLogin());
        }
        catch (DAOException e){
            throw new ServiceException(e);
        }
        return result;
    }

    private Bet serviceNewBet(Account account, String type, Event event,
                              BigDecimal coef, BigDecimal amount) throws DAOException{
        BigDecimal expectedWin = amount.multiply(coef);
        expectedWin = expectedWin.setScale(SCALE, RoundingMode.HALF_DOWN);
        Bet bet = new Bet();
        bet.setEvent(event);
        bet.setAmount(amount);
        bet.setCoefficient(coef);
        bet.setExpectedWin(expectedWin);
        bet.setTypeBet(type);
        bet.setCurrency(account.getBill().getCurrency());
        return bet;
    }

    /**
     * Takes bets which were made by accounts with currency.
     * @param currency of bet
     * @return ArrayList
     * @throws ServiceException
     */
    public ArrayList<Bet> takeAllBetsOfCurrency(String currency) throws ServiceException{
        try{
            return betDAO.selectBetsOfCurrency(currency);
        }
        catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    /**
     * Takes the bet by id
     * @param id of the bet
     * @return Bet
     * @throws ServiceException
     */
    public Bet takeBetById(int id) throws ServiceException{
        try {
            return betDAO.selectByKey(id);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    /**
     * Cancels the bet with id and returns money to the account.
     * @param account
     * @param id
     * @param amount
     * @throws ServiceException
     */
    public void cancelBet(Account account, int id, BigDecimal amount) throws ServiceException{
        try{
            betDAO.deleteBet(account, id, amount);
            deleteBet(account.getBets(), id);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    private void deleteBet(ArrayList<Bet> bets, int id) {
        for (int i = 0; i < bets.size(); i++){
            if (bets.get(i).getId() == id){
                bets.remove(i);
                i--;
            }
        }
    }

    /**
     * Updates the coefficients of all bets that where made to
     * the event with eventId.
     * @param eventID id of the event
     * @param first first win
     * @param nobody dead heat
     * @param second second win
     * @param fon first or nobody
     * @param fos first of second
     * @param son second of nobody
     * @throws ServiceException
     */
    public void updateCoefficients(int eventID, String first, String nobody, String second, String fon, String fos, String son) throws ServiceException{
        try {
            ArrayList<Bet> bets = betDAO.selectAllBetsOnMatch(eventID);
            for(Bet bet: bets){
                updateCoefficient(bet, first, nobody, second, fon, fos, son);
            }
            coefficientDAO.updateCoefficientForBets(bets);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    private void updateCoefficient(Bet bet, String first, String nobody, String second, String fon, String fos, String son) throws ServiceException {
        switch (bet.getTypeBet()) {
            case TYPE_FIRST_VICTORY:
                bet.setCoefficient(new BigDecimal(first));
                bet.setExpectedWin(bet.getAmount().multiply(new BigDecimal(first)));
                break;
            case TYPE_SECOND_VICTORY:
                bet.setCoefficient(new BigDecimal(second));
                bet.setExpectedWin(bet.getAmount().multiply(new BigDecimal(second)));
                break;
            case TYPE_DEAD_HEAT:
                bet.setCoefficient(new BigDecimal(nobody));
                bet.setExpectedWin(bet.getAmount().multiply(new BigDecimal(nobody)));
                break;
            case TYPE_FIRST_OR_NOBODY:
                bet.setCoefficient(new BigDecimal(fon));
                bet.setExpectedWin(bet.getAmount().multiply(new BigDecimal(fon)));
                break;
            case TYPE_SECOND_OR_NOBODY:
                bet.setCoefficient(new BigDecimal(son));
                bet.setExpectedWin(bet.getAmount().multiply(new BigDecimal(son)));
                break;
            case TYPE_FIRST_OR_SECOND:
                bet.setCoefficient(new BigDecimal(fos));
                bet.setExpectedWin(bet.getAmount().multiply(new BigDecimal(fos)));
                break;
            default:
                throw new ServiceException("No such typeBet when determining the result.");
        }
    }

    public ArrayList<Bet> takeAllBetsOfAccount(String login) throws ServiceException{
        try {
            return betDAO.selectAllBetsOfAccount(login);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }
}
