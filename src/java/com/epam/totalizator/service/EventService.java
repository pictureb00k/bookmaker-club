package com.epam.totalizator.service;

import com.epam.totalizator.dao.CoefficientDAO;
import com.epam.totalizator.dao.DAOException;
import com.epam.totalizator.dao.EventDAO;
import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.Event;
import com.epam.totalizator.entity.SameDescriptionEvents;
import com.epam.totalizator.entity.SameTypeEvents;
import com.epam.totalizator.manager.PagesManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static com.epam.totalizator.service.Constants.NOT_FINISHED;
import static com.epam.totalizator.service.Constants.FINISHED;

/**
 * Services events.
 * @author Pavel Bortnik
 */
public class EventService {
    private EventDAO eventDAO;
    private CoefficientDAO coefficientDAO;

    public EventService(){
        eventDAO = new EventDAO();
        coefficientDAO = new CoefficientDAO();
    }

    /**
     * Selects all events which are compiles to the eventType.
     * Forms the list of events combined by description.
     * @param eventType the type of event. Sport or cybersport
     * @return SameTypeEvents
     * @throws DAOException
     */
    public SameTypeEvents takeAllEventsOfType(String eventType) throws ServiceException{
        SameTypeEvents events;
        try {
            ArrayList<Event> list = eventDAO.selectEventsOfType(eventType);
            sortByIncreasingTime(list);
            events = formListOfMatchesGroupedByDescriptions(list);
            events.setEventType(eventType);
        }catch (DAOException e){
            throw new ServiceException("EventService exception." + e);
        }
        return events;
    }

    /**
     * Selects all events which are compiles to the kind of sport.
     * Forms the list of events combined by description.
     * @param kindOfSport the kind of sport
     * @return SameTypeEvents
     * @throws DAOException
     */
    public SameTypeEvents takeMatchesOfType(String kindOfSport) throws ServiceException{
        SameTypeEvents events;
        try {
            ArrayList<Event> list = eventDAO.selectEventsOfKind(kindOfSport, NOT_FINISHED);
            sortByIncreasingTime(list);
            events = formListOfMatchesGroupedByDescriptions(list);
            events.setEventType(kindOfSport);
        }catch (DAOException e){
            throw new ServiceException("EventService exception." + e);
        }
        return events;
    }

    /**
     * Selects all finished events. Sorts by increasing time.
     * Forms the list of events combined by description.
     * @return SameTypeEvents
     * @throws DAOException
     */
    public SameTypeEvents takeAllFinishedMatches() throws ServiceException{
        SameTypeEvents events;
        try {
            ArrayList<Event> list = eventDAO.selectAllEvents(FINISHED);
            sortByIncreasingTime(list);
            events = formListOfMatchesGroupedByDescriptions(list);
        }catch (DAOException e){
            throw new ServiceException("EventService exception." + e);
        }
        return events;
    }

    /**
     * Selects all unfinished events. Sorts by increasing time.
     * Forms the list of events combined by description.
     * @return SameTypeEvents
     * @throws DAOException
     */
    public SameTypeEvents takeAllUnfinishedMatches() throws ServiceException{
        SameTypeEvents events;
        try {
            ArrayList<Event> list = eventDAO.selectAllEvents(NOT_FINISHED);
            sortByIncreasingTime(list);
            events = formListOfMatchesGroupedByDescriptions(list);
        }catch (DAOException e){
            throw new ServiceException("EventService exception." + e);
        }
        return events;
    }

    /**
     * Take results of events combined by kind of sport.Sorts by reduction time.
     * Forms the list of events combined by description.
     * @param kindOfSport kind of sport.
     * @return SameTypeEvents
     * @throws ServiceException
     */
    public SameTypeEvents takeResultsOfKind(String kindOfSport) throws ServiceException {
        SameTypeEvents events;
        try {
            ArrayList<Event> list = eventDAO.selectEventsOfKind(kindOfSport, FINISHED);
            sortByTimeReduction(list);
            events = formListOfMatchesGroupedByDescriptions(list);
        }catch (DAOException e){
            throw new ServiceException("EventService exception." + e);
        }
        return events;
    }

    /**
     * Takes matches without coefficients.Sorts by increasing time.
     * Forms the list of events combined by description.
     * @return
     * @throws ServiceException
     */
    public SameTypeEvents takeMatchesWithoutCoefficients() throws ServiceException {
        SameTypeEvents events;
        try {
            ArrayList<Event> list = eventDAO.selectMatchesWithoutCoefficients();
            sortByIncreasingTime(list);
            events = formListOfMatchesGroupedByDescriptions(list);
        }catch (DAOException e){
            throw new ServiceException("EventService exception." + e);
        }
        return events;
    }
    
    private void sortByIncreasingTime(ArrayList<Event> events){
        Collections.sort(events, new Comparator<Event>() {
            @Override
            public int compare(Event o1, Event o2) {
                if (o1.getStartDate().equals(o2.getStartDate())){
                    return o1.getStartTime().compareTo(o2.getStartTime());
                }else {
                    return o1.getStartDate().compareTo(o2.getStartDate());
                }
            }
        });
    }

    private void sortByTimeReduction(ArrayList<Event> events){
        Collections.sort(events, new Comparator<Event>() {
            @Override
            public int compare(Event o1, Event o2) {
                if (o1.getStartDate().equals(o2.getStartDate())){
                    return o2.getStartTime().compareTo(o1.getStartTime());
                }else {
                    return o2.getStartDate().compareTo(o1.getStartDate());
                }
            }
        });
    }
    
    private SameTypeEvents formListOfMatchesGroupedByDescriptions(ArrayList<Event> events){
        SameTypeEvents sameTypeEvents = new SameTypeEvents();
        for (Event e: events){
            SameDescriptionEvents sameDescriptionEvents = containsDescription(sameTypeEvents, e.getDescription());
            if (sameDescriptionEvents != null){
                sameDescriptionEvents.addEvent(e);
            }else {
                sameDescriptionEvents = new SameDescriptionEvents();
                sameDescriptionEvents.setKindOfSport(e.getKindOfSport());
                sameDescriptionEvents.setDescription(e.getDescription());
                sameDescriptionEvents.addEvent(e);
                sameTypeEvents.addListOfSameDescriptionEvents(sameDescriptionEvents);
            }
        }
        return sameTypeEvents;
    }

    private SameDescriptionEvents containsDescription(SameTypeEvents events, String description){
        if(events != null) {
            for (SameDescriptionEvents e : events.getDescriptions()) {
                if (e.getDescription().equals(description)) {
                    return e;
                }
            }
        }
        return null;
    }

    /**
     * Determines the page where shows the results of
     * matches.
     * @param account to check the status
     * @return String page
     */
    public String determineResultsPage (Account account){
        String page;
        if (account != null) {
            if (account.getRole() == Account.Role.CLIENT) {
                page = PagesManager.getPage(PagesManager.RESULTS_CLIENT);
            }else{
                page = PagesManager.getPage(PagesManager.ADMIN_RESULTS);
            }
        }else {
            page = PagesManager.getPage(PagesManager.RESUTLS_PAGE);
        }
        return page;
    }


    public Event takeEventById(int eventID) throws ServiceException{
        Event event;
        try {
            event = eventDAO.selectByKey(eventID);
        }catch (DAOException e){
            throw new ServiceException("EventService exception." + e);
        }
        return event;
    }

    public void addNewEvent(String eventType, String kindOfSport, String description,
                               String firstCompetitor, String secondCompetitor, String dateTime) throws ServiceException{
        try {
            eventDAO.addNewEvent(eventType, kindOfSport, description, firstCompetitor,
                    secondCompetitor, dateTime);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public void arrangeCoefficients(int eventId, String first, String nobody, String second,
                                    String fon, String fos, String son) throws ServiceException{
        try {
            int coefficientID = coefficientDAO.insertCoefficients(first,nobody,second,fon,fos,son);
            if (coefficientID >= 0) {
                coefficientDAO.arrangeCoefficients(eventId, coefficientID);
            }else {
                throw new ServiceException("Incorrect last insert id.");
            }
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public void updateCoefficients(int eventID, String first, String nobody, String second,
                                   String fon, String fos, String son) throws ServiceException{
        try {
            coefficientDAO.updateCoefficients(eventID, first, nobody, second, fon, fos, son);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public void updateEvent(int eventID, String kindOfSport, String description, String firstCompetitor, String secondCompetitor, String dateTime) throws ServiceException {
        try{
            eventDAO.updateEvent(eventID, kindOfSport, description, firstCompetitor, secondCompetitor, dateTime);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public void deleteEventByID(int eventID) throws ServiceException {
        try {
            eventDAO.deleteEventByID(eventID);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public Event selectEventById(int eventID) throws ServiceException{
        try {
            return eventDAO.selectByKey(eventID);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }
}
