package com.epam.totalizator.service;

import com.epam.totalizator.dao.BillDAO;
import com.epam.totalizator.dao.DAOException;
import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.AccountBill;
import com.epam.totalizator.entity.Transaction;
import com.epam.totalizator.util.Validator;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Services bills.
 * @author Pavel Bortnik
 */
public class BillService {
    private BillDAO billDAO;

    public BillService() {
        billDAO = new BillDAO();
    }

    /**
     * Takes all deposits of the account
     * @param account to serve
     * @return ArrayList of deposits
     * @throws ServiceException
     */
    public ArrayList<Transaction> takeAccountDeposits(Account account) throws ServiceException{
        try {
            return billDAO.selectAccountDeposits(account);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    /**
     * Takes all withdraws of the account
     * @param account to serve
     * @return ArrayList of withdraws
     * @throws ServiceException
     */
    public ArrayList<Transaction> takeAccountWithdraws(Account account) throws ServiceException {
        try {
            return billDAO.selectAccountWithdraws(account);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    /**
     * Takes all transactions of the account
     * @param account to serve
     * @return ArrayList of transactions
     * @throws ServiceException
     */
    public ArrayList<Transaction> takeAllTransactions(Account account) throws ServiceException{
        try {
            return billDAO.selectAllTransactions(account);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    /**
     * Takes all bills that belong to the Admin.
     * @return ArrayList of admin bills
     * @throws ServiceException
     */
    public ArrayList<AccountBill> takeAdminBills()throws ServiceException {
        try {
            return billDAO.takeAdminBills();
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    /**
     * Makes a new deposit for account.
     * @param account to serve
     * @param type type of transaction: Deposit
     * @param amount amount of deposit
     * @return Transaction
     * @throws ServiceException
     */
    public Transaction makeDeposit(Account account, String type, BigDecimal amount) throws ServiceException{
        Transaction transaction;
        try {
            BigDecimal currentBalance = account.getBill().getBalance();
            BigDecimal newBalance = currentBalance.add(amount);
            boolean flag;
            flag = billDAO.makeDeposit(account, type, amount);
            if(flag){
                account.getBill().setBalance(newBalance);
                transaction = new Transaction();
                transaction.setAmount(amount);
                transaction.setDepositType(type);
            }else {
                throw new ServiceException("Problem with transaction (Make Deposit).");
            }
        }catch (DAOException e){
            throw new ServiceException(e);
        }
        return transaction;
    }

    /**
     * Makes a new withdraw for account.
     * @param account to serve
     * @param type type of transaction: Withdraw
     * @param amount amount of withdraw
     * @return Transaction
     * @throws ServiceException
     */
    public Transaction withdraw(Account account, String type, BigDecimal amount) throws ServiceException {
        Transaction transaction;
        try {
            BigDecimal currentBalance = account.getBill().getBalance();
            BigDecimal newBalance = currentBalance.subtract(amount);
            boolean flag;
            if (Validator.checkAvailableMoney(amount, account.getBill())) {
                flag = billDAO.withdraw(account, type, amount);
            } else {
                return null;
            }
            if (flag) {
                account.getBill().setBalance(newBalance);
                transaction = new Transaction();
                transaction.setAmount(amount);
                transaction.setDepositType(type);
            } else {
                throw new ServiceException("Problem with withdraw.");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return transaction;
    }
}
