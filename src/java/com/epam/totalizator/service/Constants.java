package com.epam.totalizator.service;

/**
 * Created by Pavel on 16.09.2016.
 */
public class Constants {
    static final int NOT_FINISHED = 0;
    static final int FINISHED = 1;
    static final String TYPE_FIRST_VICTORY = "First victory";
    static final String TYPE_SECOND_VICTORY = "Second victory";
    static final String TYPE_DEAD_HEAT = "Dead heat";
    static final String TYPE_FIRST_OR_NOBODY = "First victory or dead heat";
    static final String TYPE_FIRST_OR_SECOND = "First or Second victory";
    static final String TYPE_SECOND_OR_NOBODY = "Second victory or dead heat";
    static final int WON_BET = 1;
    static final int LOST_BET = 0;
}
