package com.epam.totalizator.service;

import com.epam.totalizator.dao.AccountDAO;
import com.epam.totalizator.dao.BetDAO;
import com.epam.totalizator.dao.DAOException;
import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.AccountBill;
import com.epam.totalizator.util.Encryption;

import java.sql.Date;
import java.util.ArrayList;

/**
 * Services accounts.
 * @author Pavel Bortnik
 */
public class AccountService {
    private AccountDAO accountDAO;
    private BetDAO betDAO;

    public AccountService(){
        accountDAO = new AccountDAO();
        betDAO = new BetDAO();
    }

    /**
     * Creates a new object of (@code Account). Authenticate the login
     * and password. Checks if it is in the database. Also, it takes
     * all playing bets and adds that to the object.
     * @param login
     * @param pass
     * @return Account
     * @throws ServiceException
     */
    public Account authenticateAccount(String login, String pass) throws ServiceException{
        Account account;
        try {
            pass = Encryption.encrypt(pass);
            account = accountDAO.authenticateAccount(login, pass);
            if (account != null && account.getRole() == Account.Role.CLIENT) {
                account.setBets(betDAO.selectPlayingBetsOfAccount(account));
            }
        }catch (DAOException e){
            throw new ServiceException(e);
        }
        return account;
    }

    /**
     * Checks if the account with login is existed.
     * @param login to check
     * @return boolean
     * @throws ServiceException
     */
    public boolean checkAccountExist(String login) throws ServiceException{
        try {
            Account account = accountDAO.selectByKey(login);
            return account != null;
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    /**
     * Creates the new account with parameters.
     * @param login login
     * @param password pass
     * @param email email
     * @param firstName first name
     * @param lastName last name
     * @param birthday birthday
     * @param curr currency of bill
     * @return boolean
     * @throws ServiceException
     */
    public void createNewAccount(String login, String password, String email, String firstName,
                                    String lastName, Date birthday, String curr) throws ServiceException{
        try {
            Account account = new Account();
            AccountBill bill = new AccountBill();
            bill.setCurrency(curr);
            account.setLogin(login);
            account.setPassword(password);
            account.setEmail(email);
            account.setFirstName(firstName);
            account.setLastName(lastName);
            account.setBirthday(birthday);
            account.setRole(Account.Role.CLIENT);
            account.setBill(bill);
            accountDAO.insertAccount(account);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    /**
     * Check if the passwords are equals.
     * @param pass password
     * @param confirmPassword password to confirm
     * @return boolean
     */
    public boolean checkPasswords(String pass, String confirmPassword) {
        return pass.equals(confirmPassword);
    }

    /**
     * Gets all client's accounts.
     * @return ArrayList
     * @throws ServiceException
     */
    public ArrayList<Account> takeAllAccounts()throws ServiceException {
        try {
            return accountDAO.selectAll();
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    /**
     * Updates the password of account with login.
     * @param newPassword new password
     * @param login login of the account
     * @return boolean
     * @throws ServiceException
     */
    public boolean updatePassword(String newPassword, String login) throws ServiceException {
        try{
            return accountDAO.updatePassword(newPassword, login);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }
}
