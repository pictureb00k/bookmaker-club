package com.epam.totalizator.service;

import com.epam.totalizator.dao.*;
import com.epam.totalizator.entity.Bet;

import java.util.ArrayList;

import static com.epam.totalizator.service.Constants.*;

/**
 * Created by Pavel on 14.09.2016.
 */
public class ResultService {
    private EventDAO eventDAO;
    private BetDAO betDAO;
    private BillDAO billDAO;

    public ResultService(){
        eventDAO = new EventDAO();
        betDAO = new BetDAO();
        billDAO = new BillDAO();
    }
    public void putResults(int eventID, int firstScore, int secondScore) throws ServiceException{
        try {
            eventDAO.insertEventResults(eventID, firstScore, secondScore);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public void putBetsResults(int eventID, int firstScore, int secondScore) throws ServiceException {
        ArrayList<Bet> allBetsOfMatch;
        ArrayList<Bet> wonBets = new ArrayList<>();
        try{
            allBetsOfMatch = betDAO.selectAllBetsOnMatch(eventID);
            int result;
            for (Bet b: allBetsOfMatch){
                result = determineTheResult(b.getTypeBet(), firstScore, secondScore);
                b.setResult(result);
                if (result == WON_BET){
                    wonBets.add(b);
                }
            }
            betDAO.updateBetsResults(allBetsOfMatch);
            billDAO.updateWonBills(wonBets);

        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    private int determineTheResult(String typeBet, int firstScore, int secondScore) throws ServiceException{
        int result;
        switch (typeBet){
            case TYPE_FIRST_VICTORY:
                if(firstScore > secondScore){
                    result = WON_BET;
                }else {
                    result = LOST_BET;
                }
                break;
            case TYPE_SECOND_VICTORY:
                if(secondScore > firstScore){
                    result = WON_BET;
                }else {
                    result = LOST_BET;
                }
                break;
            case TYPE_DEAD_HEAT:
                if (firstScore == secondScore){
                    result = WON_BET;
                }else {
                    result = LOST_BET;
                }
                break;
            case TYPE_FIRST_OR_NOBODY:
                if (firstScore >= secondScore){
                    result = WON_BET;
                }else {
                    result = LOST_BET;
                }
                break;
            case TYPE_SECOND_OR_NOBODY:
                if (secondScore >= firstScore){
                    result = WON_BET;
                }else {
                    result = LOST_BET;
                }
                break;
            case TYPE_FIRST_OR_SECOND:
                if (firstScore != secondScore){
                    result = WON_BET;
                }
                else {
                    result = LOST_BET;
                }
                break;
            default:
                throw new ServiceException("No such typeBet when determining the result.");
        }
        return result;
    }
}
