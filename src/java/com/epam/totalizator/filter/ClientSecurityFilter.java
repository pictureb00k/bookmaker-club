package com.epam.totalizator.filter;

import com.epam.totalizator.entity.Account;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The class (@code ClientSecurityFilter) protects all client's pages
 * from the direct access. If the role of account isn't Client, it
 * redirects to the main page.
 * @author Pavel
 */

@WebFilter(urlPatterns = { "/jsp/client/*" },
        initParams = {
                @WebInitParam(name = "MAIN_PAGE", value = "/controller?command=redirect_to_main_page")})
public class ClientSecurityFilter implements Filter {
    private String redirect;
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        redirect = filterConfig.getInitParameter("MAIN_PAGE");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        Account account = (Account) request.getSession().getAttribute("account");
        if (account == null || account.getRole()!= Account.Role.CLIENT){
            response.sendRedirect(redirect);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        redirect = null;
    }
}
