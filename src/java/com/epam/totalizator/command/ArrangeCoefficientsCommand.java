package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.service.EventService;
import com.epam.totalizator.service.ServiceException;
import com.epam.totalizator.util.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static com.epam.totalizator.command.Constants.*;

/**
 * Command that arrange the coefficients to the match.
 * All coefficients pass the validator.
 * Can be used only by Bookmaker.
 * @author Pavel
 */

public class ArrangeCoefficientsCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(ArrangeCoefficientsCommand.class.getName());
    private EventService eventService;

    public ArrangeCoefficientsCommand(){
        eventService = new EventService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        Account account = (Account) request.getSession().getAttribute(ACCOUNT);
        String page;
        if (account != null && account.getRole() == Account.Role.BOOKMAKER) {
            page = request.getSession().getAttribute(PAGE).toString();
            int eventId = Integer.parseInt(request.getParameter(EVENTID));
            String first = Validator.checkCoefficient(request.getParameter(WIN_FIRST));
            String nobody = Validator.checkCoefficient(request.getParameter(NOBODY));
            String second = Validator.checkCoefficient(request.getParameter(WIN_SECOND));
            String fon = Validator.checkCoefficient(request.getParameter(FIRST_OR_NOBODY));
            String fos = Validator.checkCoefficient(request.getParameter(FIRST_OR_SECOND));
            String son = Validator.checkCoefficient(request.getParameter(SECOND_OR_NOBODY));
            try {
                eventService.arrangeCoefficients(eventId, first, nobody, second, fon, fos, son);
            } catch (ServiceException e) {
                LOG.error(e);
            }
        }else {
            page = CONTROLLER_COMMAND_REDIRECT_TO_MAIN_PAGE;
        }
        return page;
    }
}
