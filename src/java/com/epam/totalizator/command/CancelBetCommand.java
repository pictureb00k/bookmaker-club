package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.Bet;
import com.epam.totalizator.service.BetService;
import com.epam.totalizator.service.ServiceException;
import com.epam.totalizator.util.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import java.math.BigDecimal;

import static com.epam.totalizator.command.Constants.*;

/**
 * Cancels the bet. Validator checks if the match can
 * be canceled.
 * @author Pavel
 */

public class CancelBetCommand implements Command{
    private static final Logger LOG = LogManager.getLogger(CancelBetCommand.class.getName());
    private BetService betService;

    public CancelBetCommand(){
        betService = new BetService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        Account account = (Account) request.getSession().getAttribute(ACCOUNT);
        if (account != null && account.getRole() == Account.Role.CLIENT) {
            int betID = Integer.parseInt(request.getParameter(BET));
            BigDecimal amount = new BigDecimal(request.getParameter(AMOUNT));
            try {
                Bet bet = betService.takeBetById(betID);
                if (Validator.checkEventTime(bet.getEvent())) {
                    betService.cancelBet(account, betID, amount);
                }else {
                    request.setAttribute(FAIL, CANCEL_BET_FAIL);
                }
            } catch (ServiceException e) {
                LOG.error(e);
            }
            page = request.getSession().getAttribute(PAGE).toString();
        }else {
            page = CONTROLLER_COMMAND_REDIRECT_TO_MAIN_PAGE;
        }
        return page;
    }
}
