package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.SameTypeEvents;
import com.epam.totalizator.service.EventService;
import com.epam.totalizator.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static com.epam.totalizator.command.Constants.SAME_DESCRIPTION_EVENTS;

/**
 * Redirect to the page where displayed the results
 * of ended matches.
 * @author Pavel
 */

public class ResultsPageCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(ResultsPageCommand.class.getName());
    private EventService eventService;

    public ResultsPageCommand(){
        eventService = new EventService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        Account account = (Account) request.getSession().getAttribute(ACCOUNT);
        try {
            SameTypeEvents events = eventService.takeAllFinishedMatches();
            request.setAttribute(SAME_DESCRIPTION_EVENTS, events.getDescriptions());
        }catch (ServiceException e){
            LOG.error(e);
        }
        page = eventService.determineResultsPage(account);
        return page;
    }
}
