package com.epam.totalizator.command;

public class Constants {
    static final String LOGIN = "login";
    static final String FAIL = "fail";
    static final String SUCCESS = "success";
    static final String PASSWORD = "password";
    static final String CONFIRM_PASSWORD = "confirm";
    static final String EMAIL = "email";
    static final String FIRST_NAME = "first-name";
    static final String LAST_NAME = "last-name";
    static final String BIRTHDAY = "birthday";
    static final String CURRENCY = "currency";
    static final String SAME_DESCRIPTION_EVENTS = "sameDescriptionEvents";
    static final String SUCCESS_REGISTRATION = "reg.success";
    static final String EXISTED_LOGIN = "reg.exist";
    static final String PASSWORDS_NO_MATCH = "reg.pass.no.match";
    static final String TOO_SMALL = "reg.small";
    static final String INCORRECT_REGISTER_INFO = "reg.bad";
    static final String INCORRECT_PASS_OR_LOGIN = "login.fail";
    static final String REDIRECT_TO_MAIN_PAGE = "redirect_to_main_page";
    static final String KIND_OF_SPORT = "kindOfSport";
    static final String EVENT_TYPE = "event_type";
    static final String EVENT_TYPE_SPORT = "sport";
    static final String EVENT_TYPE_CYBERSPORT = "cybersport";
    static final String TRANSACTION_TYPE = "transType";
    static final String DEPOSIT = "deposit";
    static final String WITHDRAW = "withdraw";
    static final String TYPE = "type";
    static final String DEP_AMOUNT = "dep-amount";
    static final String TRANSACTION = "transaction";
    static final String BET = "bet";
    static final String BETS = "bets";
    static final String EVENT = "event";
    static final String COEFFICIENT = "coef";
    static final String AMOUNT = "amount";
    static final String BET_TYPE = "betType";
    static final String NO_FUNDS = "no.funds";
    static final String NEW_BET = "newBet";
    static final String DESCRIPTION = "description";
    static final String FIRST_COMPETITOR = "first_competitor";
    static final String SECOND_COMPETITOR = "second_competitor";
    static final String DATE = "date";
    static final String TIME = "time";
    static final String SPORT = "sport";
    static final String SPORT_SUCCESS = "sportSuccess";
    static final String CYBER_SUCCESS = "cyberSuccess";
    static final String CYBER_FAIL = "cyberFail";
    static final String SPORT_FAIL = "sportFail";
    static final String ADDING_SUCCESS = "add.success";
    static final String ADDING_FAIL = "add.fail";
    static final String EVENT_ID = "event_id";
    static final String WIN_FIRST = "win_first";
    static final String NOBODY = "nobody";
    static final String WIN_SECOND = "win_second";
    static final String FIRST_OR_NOBODY = "first_or_nobody";
    static final String FIRST_OR_SECOND = "first_or_second";
    static final String SECOND_OR_NOBODY = "second_or_nobody";
    static final String WIN = "win";
    static final String LOSE = "lose";
    static final String IN_GAME = "in_game";
    static final String PARAM_INFO_TYPE = "info_type";
    static final String TRANSACTIONS = "transactions";
    static final String LANGUAGE = "language";
    static final String OLD_PASSWORD = "oldPass";
    static final String NEW_PASSWORD = "newPass";
    static final String PASSWORD_BAD = "pass.bad";
    static final String PASSWORD_CONFIRMED = "pass.confirmed";
    static final String PASSWORD_INCORRECT = "pass.incorrect";
    static final String COEF = "coef";
    static final String FIRST_SCORE = "score-first";
    static final String SECOND_SCORE = "score-second";
    static final String EVENTID = "eventID";
    static final String BILLS = "bills";
    static final String MATCH_BEGUN = "match.begun";
    static final String CANCEL_BET_FAIL = "bet.fail";
    static final String ACCOUNTS = "accounts";
    static final int BAD_RESULT = -1;
}
