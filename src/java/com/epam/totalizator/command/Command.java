package com.epam.totalizator.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The (@code Command) interface contains the most used constants
 * and only one method (@code execute). The method handles the request.
 * This includes business logic, requests to the database, forming results
 * and returns the new page to be redirected.
 * @author Pavel Bortnik
 */

public interface Command {
    String DISPATCHER_TYPE = "type";
    String SEND_REDIRECT = "redirect";
    String ACCOUNT = "account";
    String PAGE = "page";
    String CONTROLLER_COMMAND_REDIRECT_TO_MAIN_PAGE = "/controller?command=redirect_to_main_page";

    /**
     * The main method that preforms full logic of (@code Command).
     * @param       request the request with parameters
     * @return      the new page to be redirected
     */
    String execute(HttpServletRequest request);
}
