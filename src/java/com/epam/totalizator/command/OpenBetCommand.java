package com.epam.totalizator.command;

import com.epam.totalizator.entity.Event;
import com.epam.totalizator.manager.PagesManager;
import com.epam.totalizator.service.EventService;
import com.epam.totalizator.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static com.epam.totalizator.command.Constants.*;

/**
 * Redirects to the page where user can make a bet.
 * Can be used only by Client.
 * @author Pavel
 */

public class OpenBetCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(OpenBetCommand.class.getName());

    private EventService eventService;

    public OpenBetCommand(){
        eventService = new EventService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        Event event = null;
        int eventID = Integer.parseInt(request.getParameter(EVENT));
        double coefficient = Double.parseDouble(request.getParameter(COEF));
        String type = request.getParameter(BET_TYPE);
        try {
            event = eventService.takeEventById(eventID);
        }catch (ServiceException e){
            LOG.error(e);
        }
        request.setAttribute(EVENT, event);
        request.setAttribute(COEF, coefficient);
        request.setAttribute(BET_TYPE, type);
        return PagesManager.getPage(PagesManager.CLIENT_BET_DIALOG);
    }
}
