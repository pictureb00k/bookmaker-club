package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.Bet;
import com.epam.totalizator.entity.Event;
import com.epam.totalizator.manager.PagesManager;
import com.epam.totalizator.service.BetService;
import com.epam.totalizator.service.EventService;
import com.epam.totalizator.service.ServiceException;
import com.epam.totalizator.util.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;

import static com.epam.totalizator.command.Constants.*;

/**
 * Command makes the bet. The bet passes the
 * (@code Validator) class.
 * Can be used only by Client
 * @author Pavel
 */

public class MakeBetCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(MakeBetCommand.class.getName());

    private BetService betService;
    private EventService eventService;

    public MakeBetCommand(){
        betService = new BetService();
        eventService = new EventService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute(ACCOUNT);
        if (account != null && account.getRole() == Account.Role.CLIENT){
            int eventID = Integer.parseInt(request.getParameter(EVENT));
            BigDecimal coefficient = new BigDecimal(request.getParameter(COEFFICIENT));
            BigDecimal amount = new BigDecimal(request.getParameter(AMOUNT));
            String betType = request.getParameter(BET_TYPE);
            try {
                Event event = eventService.selectEventById(eventID);
                if(!Validator.checkEventTime(event)){
                    return getFailBetPage(request, event, coefficient, betType, MATCH_BEGUN);
                }

                if (!Validator.checkAvailableMoney(amount, account.getBill())) {
                    return getFailBetPage(request, event, coefficient, betType, NO_FUNDS);
                }

                Bet bet = betService.makeNewBet(account, betType, coefficient, amount, event);
                if (bet != null){
                    page = PagesManager.getPage(PagesManager.SUCCESS_BET);
                    session.setAttribute(NEW_BET, bet);
                }
            }catch (ServiceException e){
                LOG.error(e);
            }
        }else {
            page = CONTROLLER_COMMAND_REDIRECT_TO_MAIN_PAGE;
        }
        request.setAttribute(DISPATCHER_TYPE, SEND_REDIRECT);
        return page;
    }

    private String getFailBetPage(HttpServletRequest request, Event event,
                                  BigDecimal coef, String betType, String problem){
        request.setAttribute(EVENT, event);
        request.setAttribute(FAIL, problem);
        request.setAttribute(COEFFICIENT, coef);
        request.setAttribute(BET_TYPE, betType);
        return PagesManager.getPage(PagesManager.CLIENT_BET_DIALOG);
    }
}
