package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.SameTypeEvents;
import com.epam.totalizator.manager.PagesManager;
import com.epam.totalizator.service.EventService;
import com.epam.totalizator.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static com.epam.totalizator.command.Constants.EVENT_TYPE_SPORT;
import static com.epam.totalizator.command.Constants.SAME_DESCRIPTION_EVENTS;

/**
 * Redirects to the page with all sport matches.
 * @author Pavel
 */

public class SportPageCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(SportPageCommand.class.getName());
    private EventService eventService;

    public SportPageCommand(){
        eventService = new EventService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        Account account = (Account)request.getSession().getAttribute(ACCOUNT);
        SameTypeEvents events = null;
        try {
            events = eventService.takeAllEventsOfType(EVENT_TYPE_SPORT);
        }catch (ServiceException e){
            LOG.error(e);
        }
        if(account != null){
            page = PagesManager.getPage(PagesManager.CLIENT_SPORT_PAGE);
        }else {
            page = PagesManager.getPage(PagesManager.SPORTS_PAGE);
        }
        request.setAttribute(SAME_DESCRIPTION_EVENTS, events.getDescriptions());
        return page;
    }
}
