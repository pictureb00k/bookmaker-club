package com.epam.totalizator.command;

import java.util.HashMap;
import java.util.Map;

/**
 * The (@code CommandManager) class represents the (@code Operation) storage of all
 * commands that realize (@link Command) interface.
 * @see Command
 * @see Operation
 * @author Pavel Bortnik
 */
public class CommandManager {
    private static Map<Operation, Command> map = new HashMap<Operation, Command>();
    static
    {
        map.put(Operation.LOGIN, new LoginCommand());
        map.put(Operation.LOGOUT, new LogoutCommand());
        map.put(Operation.REGISTER, new RegisterCommand());
        map.put(Operation.EVENT, new EventCommand());
        map.put(Operation.CHANGE_LANG, new ChangeLangCommand());
        map.put(Operation.REDIRECT_TO_MAIN_PAGE, new MainPageCommand());
        map.put(Operation.TRANSACTION, new TransactionCommand());
        map.put(Operation.BILL_INFO, new BillInfoCommand());
        map.put(Operation.OPEN_BET, new OpenBetCommand());
        map.put(Operation.MAKE_BET, new MakeBetCommand());
        map.put(Operation.BET_INFO, new BetInfoCommand());
        map.put(Operation.REDIRECT_TO_SPORT_PAGE, new SportPageCommand());
        map.put(Operation.REDIRECT_TO_CYBERSPORT_PAGE, new CybersportPageCommand());
        map.put(Operation.CHANGE_PASSWORD, new ChangePasswordCommand());
        map.put(Operation.ADD_MATCH, new AddMatchCommand());
        map.put(Operation.REDIRECT_TO_UPDATE, new UpdatePageCommand());
        map.put(Operation.ARRANGE_COEFFS, new ArrangeCoefficientsCommand());
        map.put(Operation.UPDATE_COEFFS, new UpdateCoefficientsCommand());
        map.put(Operation.PUT_RESULT_PAGE, new PutResultsPageCommand());
        map.put(Operation.PUT_RESULTS, new PutResultsCommand());
        map.put(Operation.RESULTS_PAGE, new ResultsPageCommand());
        map.put(Operation.REDIRECT_TO_BILLS_STATUS, new BillsStatusPageCommand());
        map.put(Operation.ADMIN_TRANSACTIONS, new AdminTransactionsCommand());
        map.put(Operation.CANCEL_BET, new CancelBetCommand());
        map.put(Operation.EDIT_EVENT_PAGE, new EditEventPageCommand());
        map.put(Operation.EDIT_MATCH, new EditEventCommand());
        map.put(Operation.DELETE_MATCH, new DeleteMatchCommand());
        map.put(Operation.SORT_RESULTS, new SortResultsCommand());
        map.put(Operation.SHOW_ACCOUNT_BETS, new AccountBetsCommand());
    }

    /**
     * Returns the (@code Command) class, that realizes Command. The correct
     * class is defines by the text value.
     * @param value determines (@code Operation)
     * @return the class which realizes (@code Command) interface.
     * @see Command
     */
    public static Command takeCommand(String value){
        Operation operation = Operation.valueOf(value.toUpperCase());
        return map.get(operation);
    }

    private CommandManager(){

    }

    private enum Operation{
        LOGIN,
        LOGOUT,
        REGISTER,
        REDIRECT_TO_MAIN_PAGE,
        REDIRECT_TO_SPORT_PAGE,
        REDIRECT_TO_CYBERSPORT_PAGE,
        REDIRECT_TO_UPDATE,
        EVENT,
        CHANGE_LANG,
        TRANSACTION,
        BILL_INFO,
        OPEN_BET,
        MAKE_BET,
        BET_INFO,
        CHANGE_PASSWORD,
        ADD_MATCH,
        ARRANGE_COEFFS,
        UPDATE_COEFFS,
        PUT_RESULT_PAGE,
        PUT_RESULTS,
        RESULTS_PAGE,
        REDIRECT_TO_BILLS_STATUS,
        ADMIN_TRANSACTIONS,
        CANCEL_BET,
        EDIT_EVENT_PAGE,
        EDIT_MATCH,
        DELETE_MATCH,
        SORT_RESULTS,
        SHOW_ACCOUNT_BETS
    }
}
