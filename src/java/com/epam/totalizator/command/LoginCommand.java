package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.SameTypeEvents;
import com.epam.totalizator.manager.PagesManager;
import com.epam.totalizator.service.AccountService;
import com.epam.totalizator.service.EventService;
import com.epam.totalizator.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.servlet.http.HttpServletRequest;

import static com.epam.totalizator.command.Constants.*;

/**
 * Command that authenticate user and redirect
 * it to main page depending on it's role.
 * @author Pavel
 */

public class LoginCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(LoginCommand.class.getName());
    private AccountService accountService;

    public LoginCommand(){
        accountService = new AccountService();
    }

    public String execute(HttpServletRequest request) {
        String page = null;
        String login = request.getParameter(LOGIN);
        String pass = request.getParameter(PASSWORD);
        try {
            Account account = accountService.authenticateAccount(login, pass);
            if (account != null) {
                request.getSession().setAttribute(ACCOUNT, account);
                page = CONTROLLER_COMMAND_REDIRECT_TO_MAIN_PAGE;
            } else {
                request.setAttribute(FAIL, INCORRECT_PASS_OR_LOGIN);
                page = PagesManager.getPage(PagesManager.LOGIN_PAGE);
            }
        }catch (ServiceException e){
            LOG.error(e);
        }
        return page;
    }
}
