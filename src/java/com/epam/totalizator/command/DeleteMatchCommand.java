package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.service.EventService;
import com.epam.totalizator.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static com.epam.totalizator.command.Constants.*;

/**
 * Deletes the match. Only matches without coefficients
 * can be deleted.
 * Can be used only by Admin.
 * @author Pavel
 */

public class DeleteMatchCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(DeleteMatchCommand.class.getName());

    private EventService eventService;
    public DeleteMatchCommand(){
        eventService = new EventService();
    }
    @Override
    public String execute(HttpServletRequest request) {
        Account account = (Account) request.getSession().getAttribute(ACCOUNT);
        String page;
        if (account != null && account.getRole() == Account.Role.ADMIN){
            page = request.getSession().getAttribute(PAGE).toString();
            int eventID = Integer.parseInt(request.getParameter(EVENTID));
            try {
                eventService.deleteEventByID(eventID);
            }catch (ServiceException e){
                LOG.error(e);
            }
        }
        else{
            page = CONTROLLER_COMMAND_REDIRECT_TO_MAIN_PAGE;
        }
        return page;
    }
}
