package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.service.EventService;
import com.epam.totalizator.service.ServiceException;
import com.epam.totalizator.util.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static com.epam.totalizator.command.Constants.*;

/**
 * Edit the match. Only matches without coefficients
 * can be edited.
 * Can be used only by Admin.
 * @author Pavel
 */

public class EditEventCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(EditEventCommand.class.getName());

    private EventService eventService;

    public EditEventCommand(){
        eventService = new EventService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        Account account = (Account) request.getSession().getAttribute(ACCOUNT);
        String page;
        if (account != null && account.getRole() == Account.Role.ADMIN){
            page = request.getSession().getAttribute(PAGE).toString();
            int eventID = Integer.parseInt(request.getParameter(EVENTID));
            String kindOfSport = request.getParameter(KIND_OF_SPORT);
            String description = request.getParameter(DESCRIPTION);
            String firstCompetitor = request.getParameter(FIRST_COMPETITOR);
            String secondCompetitor = request.getParameter(SECOND_COMPETITOR);
            String date = request.getParameter(DATE);
            String time = request.getParameter(TIME);
            String dateTime = date + " " + time;
            if (Validator.checkNewMatch(date, time)) {
                try {
                    eventService.updateEvent(eventID, kindOfSport, description,
                            firstCompetitor, secondCompetitor, dateTime);
                    request.setAttribute(EVENTID, eventID);
                }catch (ServiceException e){
                    LOG.error(e);
                }
            }else {
                request.setAttribute(EVENTID, eventID);
                request.setAttribute(FAIL, FAIL);
            }

        }else{
            page = CONTROLLER_COMMAND_REDIRECT_TO_MAIN_PAGE;
        }
        return page;
    }
}
