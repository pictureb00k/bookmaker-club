package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.Transaction;
import com.epam.totalizator.manager.PagesManager;
import com.epam.totalizator.service.BillService;
import com.epam.totalizator.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

import static com.epam.totalizator.command.Constants.*;

/**
 * Shows the information about all transactions are made
 * by the client. Such as deposit and withdraw using one
 * of the available pay methods.
 * @author Pavel
 */

public class BillInfoCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(BillInfoCommand.class.getName());

    private BillService billService;

    public BillInfoCommand(){
        billService = new BillService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        ArrayList<Transaction> result = null;
        Account account = (Account) request.getSession().getAttribute(ACCOUNT);
        if(account != null && account.getRole() == Account.Role.CLIENT) {
            try {
                String infoType = request.getParameter(PARAM_INFO_TYPE);
                switch (infoType) {
                    case DEPOSIT:
                        result = billService.takeAccountDeposits(account);
                        break;
                    case WITHDRAW:
                        result = billService.takeAccountWithdraws(account);
                        break;
                    default:
                        result = billService.takeAllTransactions(account);
                }
            }catch (ServiceException e){
                LOG.error(e);
            }
            page = PagesManager.getPage(PagesManager.CLIENT_BALANCE_INFO);
            request.setAttribute(TRANSACTIONS, result);
        }else {
            page = CONTROLLER_COMMAND_REDIRECT_TO_MAIN_PAGE;
        }
        return page;
    }
}
