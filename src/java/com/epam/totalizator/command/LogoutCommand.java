package com.epam.totalizator.command;

import com.epam.totalizator.manager.PagesManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Command that invalidates current session.
 * This command can be used only by authorized users.
 * @author Pavel
 */

public class LogoutCommand implements Command {
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null){
            session.invalidate();
        }
        String page = PagesManager.getPage(PagesManager.MAIN_PAGE);
        return page;
    }
}
