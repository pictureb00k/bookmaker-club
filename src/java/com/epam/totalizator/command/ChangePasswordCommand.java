package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.service.AccountService;
import com.epam.totalizator.service.ServiceException;
import com.epam.totalizator.util.Encryption;
import com.epam.totalizator.util.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static com.epam.totalizator.command.Constants.*;

/**
 * Command changes the client's password. Can be
 * used only by the Client.
 * @author Pavel
 */

public class ChangePasswordCommand implements Command{
    private static final Logger LOG = LogManager.getLogger(ChangePasswordCommand.class.getName());

    private AccountService accountService;

    public ChangePasswordCommand(){
        accountService = new AccountService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute(ACCOUNT);
        String page = session.getAttribute(PAGE).toString();

        if (account.getRole() == Account.Role.CLIENT) {
            try {
                String oldPassword = request.getParameter(OLD_PASSWORD);
                String newPassword = request.getParameter(NEW_PASSWORD);
                String confirmPassword = request.getParameter(CONFIRM_PASSWORD);
                String barrier = passBarriers(oldPassword, newPassword, confirmPassword, account.getPassword());
                if(barrier.isEmpty()) {
                    newPassword = Encryption.encrypt(newPassword);
                    accountService.updatePassword(newPassword, account.getLogin());
                    request.setAttribute(SUCCESS, PASSWORD_CONFIRMED);
                    account.setPassword(newPassword);
                }else {
                    request.setAttribute(FAIL, barrier);
                }
            } catch (ServiceException e) {
                LOG.error(e);
            }
        }else {
            page = CONTROLLER_COMMAND_REDIRECT_TO_MAIN_PAGE;
        }
        return page;
    }

    private String passBarriers(String old, String updated, String confirm, String password){
        old = Encryption.encrypt(old);
        if (!old.equals(password)){
            return PASSWORD_INCORRECT;
        }
        if (!updated.equals(confirm)){
            return PASSWORDS_NO_MATCH;
        }
        if (!Validator.checkPassword(updated)){
            return PASSWORD_BAD;
        }
        return "";
    }
}
