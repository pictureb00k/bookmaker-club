package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.AccountBill;
import com.epam.totalizator.manager.PagesManager;
import com.epam.totalizator.service.AccountService;
import com.epam.totalizator.service.BillService;
import com.epam.totalizator.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

import static com.epam.totalizator.command.Constants.ACCOUNTS;
import static com.epam.totalizator.command.Constants.BILLS;

/**
 * Redirects to the admin page, where admin could see
 * his bills status.
 * Can be used only by (@code Account.Role.Admin)
 * @author Pavel
 */

public class BillsStatusPageCommand implements Command{
    private static final Logger LOG = LogManager.getLogger(BillsStatusPageCommand.class.getName());
    private BillService billService;
    private AccountService accountService;

    public BillsStatusPageCommand(){
        billService = new BillService();
        accountService = new AccountService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        Account account = (Account) request.getSession().getAttribute(ACCOUNT);
        if (account != null && account.getRole() == Account.Role.ADMIN) {
            page = PagesManager.getPage(PagesManager.BILL_STATUS);
            try {
                ArrayList<AccountBill> bills = billService.takeAdminBills();
                request.setAttribute(BILLS, bills);
                ArrayList<Account> accounts = accountService.takeAllAccounts();
                request.setAttribute(ACCOUNTS, accounts);
            } catch (ServiceException e) {
                LOG.error(e);
            }
        }else {
            page = CONTROLLER_COMMAND_REDIRECT_TO_MAIN_PAGE;
        }
        return page;
    }
}
