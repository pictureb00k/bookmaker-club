package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.Bet;
import com.epam.totalizator.manager.PagesManager;
import com.epam.totalizator.service.BetService;
import com.epam.totalizator.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

import static com.epam.totalizator.command.Constants.*;

/**
 * Command shows the information about WIN, LOSE, ALL bets
 * connected to the account.
 * Can be used only by client.
 * @author Pavel
 */

public class BetInfoCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(BetInfoCommand.class.getName());
    private BetService betService;

    public BetInfoCommand(){
        betService = new BetService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        ArrayList<Bet> result = null;
        Account account = (Account) request.getSession().getAttribute(ACCOUNT);
        if(account != null && account.getRole() == Account.Role.CLIENT) {
            try {
                String betType = request.getParameter(BET_TYPE);
                switch (betType) {
                    case WIN:
                        result = betService.takeWonBets(account);
                        break;
                    case LOSE:
                        result = betService.takeLostBets(account);
                        break;
                    default:
                        result = betService.takeWonAndLostBets(account);
                }
            }catch (ServiceException e){
                LOG.error(e);
            }
            page = PagesManager.getPage(PagesManager.CLIENT_BET_INFO);
            request.setAttribute(BETS, result);
        }else {
            page = CONTROLLER_COMMAND_REDIRECT_TO_MAIN_PAGE;
        }
        return page;
    }
}
