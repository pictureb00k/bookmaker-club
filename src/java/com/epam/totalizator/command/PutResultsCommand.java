package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.Event;
import com.epam.totalizator.service.EventService;
import com.epam.totalizator.service.ResultService;
import com.epam.totalizator.service.ServiceException;
import com.epam.totalizator.util.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static com.epam.totalizator.command.Constants.*;

/**
 * Command puts the results of match.
 * Can be used only by Admin.
 * @author Pavel
 */

public class PutResultsCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(PutResultsCommand.class.getName());
    private ResultService resultService;
    private EventService eventService;

    public PutResultsCommand(){
        resultService = new ResultService();
        eventService = new EventService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        Account account = (Account) request.getSession().getAttribute(ACCOUNT);
        String page;
        if (account != null && account.getRole() == Account.Role.ADMIN) {
            page = request.getSession().getAttribute(PAGE).toString();
            int eventID = Integer.parseInt(request.getParameter(EVENTID));
            try{
                Event event = eventService.selectEventById(eventID);
                if (!Validator.checkEventTime(event)) {
                    int firstScore = Validator.checkScore(request.getParameter(FIRST_SCORE));
                    int secondScore = Validator.checkScore(request.getParameter(SECOND_SCORE));
                    if (firstScore != BAD_RESULT && secondScore != BAD_RESULT) {
                        resultService.putResults(eventID, firstScore, secondScore);
                        resultService.putBetsResults(eventID, firstScore, secondScore);
                    }else {
                        request.setAttribute(EVENTID, eventID);
                    }
                }else {
                    request.setAttribute(EVENTID, eventID);
                }
            } catch (ServiceException e) {
                LOG.error(e);
            }
        }else {
            page = CONTROLLER_COMMAND_REDIRECT_TO_MAIN_PAGE;
        }
        return page;
    }
}
