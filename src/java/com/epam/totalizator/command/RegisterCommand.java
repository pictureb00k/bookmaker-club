package com.epam.totalizator.command;

import com.epam.totalizator.manager.PagesManager;
import com.epam.totalizator.service.AccountService;
import com.epam.totalizator.service.ServiceException;
import com.epam.totalizator.util.Encryption;
import com.epam.totalizator.util.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;

import static com.epam.totalizator.command.Constants.*;

/**
 * Command registers the new user to the system.
 * The user information pass the (@code Validator).
 * @author Pavel
 */

public class RegisterCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(RegisterCommand.class.getName());
    private AccountService accountService;

    public RegisterCommand(){
        accountService = new AccountService();
    }

    public String execute(HttpServletRequest request) {
        String page = PagesManager.getPage(PagesManager.REGISTER_PAGE);
        try {
            String login = request.getParameter(LOGIN);
            String pass = request.getParameter(PASSWORD);
            String confirmPassword = request.getParameter(CONFIRM_PASSWORD);
            String email = request.getParameter(EMAIL);
            String firstName = request.getParameter(FIRST_NAME);
            String lastName = request.getParameter(LAST_NAME);
            String curr = request.getParameter(CURRENCY);
            Date date = Date.valueOf(request.getParameter(BIRTHDAY));
            String barrier = passBarriers(pass, confirmPassword, login, date);
            if (!barrier.isEmpty()){
                request.setAttribute(FAIL, barrier);
                return page;
            }

            if (Validator.checkNewAccount(login, pass, email, firstName, lastName)) {
                pass = Encryption.encrypt(pass);
                accountService.createNewAccount(login, pass, email, firstName, lastName, date, curr);
                request.setAttribute(SUCCESS, SUCCESS_REGISTRATION);
                page = PagesManager.getPage(PagesManager.LOGIN_PAGE);
            } else {
                request.setAttribute(FAIL, INCORRECT_REGISTER_INFO);
            }
        }catch (ServiceException e){
            LOG.error(e);
        }
        return page;
    }

    private String passBarriers(String password, String confirmPassword, String login, Date date) throws ServiceException{
        if (!accountService.checkPasswords(password, confirmPassword)){
            return  PASSWORDS_NO_MATCH;
        }
        if (accountService.checkAccountExist(login)){
            return EXISTED_LOGIN;
        }
        if (!Validator.checkAge(date)){
            return TOO_SMALL;
        }
        return "";
    }
}
