package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.SameTypeEvents;
import com.epam.totalizator.manager.PagesManager;
import com.epam.totalizator.service.EventService;
import com.epam.totalizator.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static com.epam.totalizator.command.Constants.SAME_DESCRIPTION_EVENTS;

/**
 * Redirects to the page where the Admin can put the
 * results of the match.
 * Can be used only by Admin.
 * @author Pavel
 */

public class PutResultsPageCommand implements Command{
    private static final Logger LOG = LogManager.getLogger(PutResultsPageCommand.class.getName());
    private EventService eventService;

    public PutResultsPageCommand(){
        eventService = new EventService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        Account account = (Account) request.getSession().getAttribute(ACCOUNT);
        String page = null;
        if (account != null && account.getRole() == Account.Role.ADMIN) {
            try {
                SameTypeEvents events = eventService.takeAllUnfinishedMatches();
                request.setAttribute(SAME_DESCRIPTION_EVENTS, events.getDescriptions());
                page = PagesManager.getPage(PagesManager.PUT_RESULTS);
            } catch (ServiceException e) {
                LOG.error(e);
            }
        }else{
            page = PagesManager.getPage(PagesManager.LOGIN_PAGE);
        }
        return page;
    }
}
