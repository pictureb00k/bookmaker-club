package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.Bet;
import com.epam.totalizator.service.BetService;
import com.epam.totalizator.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * Command that shows all bets which have been made by client
 * to admin. Bets are taken using the client's login. So admin
 * can see all information about bets and control them.
 * The command can be used only by Admin.
 * @author Pavel
 */
public class AccountBetsCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(AccountBetsCommand.class.getName());
    private BetService betService;

    public AccountBetsCommand(){
        betService = new BetService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        Account account = (Account) request.getSession().getAttribute(ACCOUNT);
        if(account != null && account.getRole() == Account.Role.ADMIN){
            page = request.getSession().getAttribute(PAGE).toString();
            String login = request.getParameter(Constants.LOGIN);
            try{
                ArrayList<Bet> bets = betService.takeAllBetsOfAccount(login);
                request.setAttribute(Constants.BETS, bets);
            }catch (ServiceException e){
                LOG.error(e);
            }
        }else {
            page = CONTROLLER_COMMAND_REDIRECT_TO_MAIN_PAGE;
        }
        return page;
    }
}
