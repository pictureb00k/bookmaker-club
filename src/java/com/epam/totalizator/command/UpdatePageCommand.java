package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.SameTypeEvents;
import com.epam.totalizator.manager.PagesManager;
import com.epam.totalizator.service.EventService;
import com.epam.totalizator.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static com.epam.totalizator.command.Constants.SAME_DESCRIPTION_EVENTS;

/**
 * Redirects to the page where Bookmaker can update
 * the coefficients only for not finished matches.
 * Can be used only by Bookmaker.
 * @author Pavel
 */

public class UpdatePageCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(UpdatePageCommand.class.getName());
    private EventService eventService;

    public UpdatePageCommand(){
        eventService = new EventService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        SameTypeEvents events = null;
        Account account = (Account) request.getSession().getAttribute(ACCOUNT);
        if (account != null && account.getRole() == Account.Role.BOOKMAKER) {
            page = PagesManager.getPage(PagesManager.BOOKMAKER_UPDATE_PAGE);
            try {
                events = eventService.takeAllUnfinishedMatches();
            } catch (ServiceException e) {
                LOG.error(e);
            }
            request.setAttribute(SAME_DESCRIPTION_EVENTS, events.getDescriptions());
        }else {
            page = CONTROLLER_COMMAND_REDIRECT_TO_MAIN_PAGE;
        }
        return page;
    }
}
