package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.service.EventService;
import com.epam.totalizator.service.ServiceException;
import com.epam.totalizator.util.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import static com.epam.totalizator.command.Constants.*;

/**
 * Command, that adds a new match. The match information
 * is validated.
 * Can be used only by admin.
 * @author Pavel Bortnik
 */

public class AddMatchCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(AddMatchCommand.class.getName());

    private EventService eventService;

    public AddMatchCommand(){
        eventService = new EventService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        Account account = (Account) request.getSession().getAttribute(ACCOUNT);
        if (account != null && account.getRole().equals(Account.Role.ADMIN)) {
            page = request.getSession().getAttribute(PAGE).toString();
            String eventType = request.getParameter(EVENT_TYPE);
            String kindOfSport = request.getParameter(KIND_OF_SPORT);
            String description = request.getParameter(DESCRIPTION);
            String firstCompetitor = request.getParameter(FIRST_COMPETITOR);
            String secondCompetitor = request.getParameter(SECOND_COMPETITOR);
            String date = request.getParameter(DATE);
            String time = request.getParameter(TIME);
            String dateTime = date + " " + time;
            if (Validator.checkNewMatch(date, time)) {
                try {
                    eventService.addNewEvent(eventType, kindOfSport, description,
                            firstCompetitor, secondCompetitor, dateTime);
                        if (eventType.equals(SPORT)) {
                            request.setAttribute(SPORT_SUCCESS, ADDING_SUCCESS);
                        } else {
                            request.setAttribute(CYBER_SUCCESS, ADDING_SUCCESS);
                        }
                }
                catch (ServiceException e) {
                    LOG.error(e);
                }

            }else {
                if (eventType.equals(SPORT)) {
                    request.setAttribute(SPORT_FAIL, ADDING_FAIL);
                } else {
                    request.setAttribute(CYBER_FAIL, ADDING_FAIL);
                }
            }

        }else {
            page = CONTROLLER_COMMAND_REDIRECT_TO_MAIN_PAGE;
        }
        return page;
    }
}
