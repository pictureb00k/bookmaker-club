package com.epam.totalizator.command;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static com.epam.totalizator.command.Constants.LANGUAGE;

/**
 * Command changes the localisation of the site.
 * @author Pavel
 */

public class ChangeLangCommand implements Command {

    public ChangeLangCommand() {
    }

    public String execute(HttpServletRequest request) {
        String lang = request.getParameter(LANGUAGE);
        HttpSession session = request.getSession();
        session.setAttribute(LANGUAGE, lang);
        return session.getAttribute(PAGE).toString();
    }
}
