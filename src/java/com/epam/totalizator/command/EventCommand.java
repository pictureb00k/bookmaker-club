package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.SameTypeEvents;
import com.epam.totalizator.manager.PagesManager;
import com.epam.totalizator.service.EventService;
import com.epam.totalizator.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static com.epam.totalizator.command.Constants.*;

/**
 * Command gives the information about the matches
 * of concrete kind of sport.
 * @author Pavel
 */

public class EventCommand implements Command{
    private static final Logger LOG = LogManager.getLogger(EventCommand.class.getName());

    private EventService eventService;

    public EventCommand(){
        eventService = new EventService();
    }

    public String execute(HttpServletRequest request) {
        String page = null;
        String eventType = request.getParameter(EVENT_TYPE);
        String kindOfSport = request.getParameter(KIND_OF_SPORT);
        SameTypeEvents events = new SameTypeEvents();
        try {
            events = eventService.takeMatchesOfType(kindOfSport);
            events.setEventType(eventType);
            request.setAttribute(SAME_DESCRIPTION_EVENTS, events.getDescriptions());
        }catch (ServiceException e){
            LOG.error(e);
        }
        Account account = (Account) request.getSession().getAttribute(ACCOUNT);
        if (account != null) {
            if(EVENT_TYPE_SPORT.equals(events.getEventType())) {
                page = PagesManager.getPage(PagesManager.CLIENT_SPORT_PAGE);
            }else if (EVENT_TYPE_CYBERSPORT.equals(events.getEventType())){
                page = PagesManager.getPage(PagesManager.CLIENT_CYBERSPORT_PAGE);
            }
        }
        else{
            if(EVENT_TYPE_SPORT.equals(events.getEventType())) {
                page = PagesManager.getPage(PagesManager.SPORTS_PAGE);
            }else if (EVENT_TYPE_CYBERSPORT.equals(events.getEventType())){
                page = PagesManager.getPage(PagesManager.CYBERSPORTS_PAGE);
            }
        }
        return page;
    }
}
