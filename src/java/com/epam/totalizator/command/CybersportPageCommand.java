package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.SameTypeEvents;
import com.epam.totalizator.manager.PagesManager;
import com.epam.totalizator.service.EventService;
import com.epam.totalizator.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static com.epam.totalizator.command.Constants.EVENT_TYPE_CYBERSPORT;
import static com.epam.totalizator.command.Constants.SAME_DESCRIPTION_EVENTS;

/**
 * Redirects to the page with all cybersport matches.
 * @author Pavel
 */

public class CybersportPageCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(CybersportPageCommand.class.getName());
    private EventService eventService;

    public CybersportPageCommand(){
        eventService = new EventService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        Account account = (Account)request.getSession().getAttribute(ACCOUNT);
        SameTypeEvents events = null;
        try {
            events = eventService.takeAllEventsOfType(EVENT_TYPE_CYBERSPORT);
        }catch (ServiceException e){
            LOG.error(e);
        }
        if(account != null){
            page = PagesManager.getPage(PagesManager.CLIENT_CYBERSPORT_PAGE);
        }else {
            page = PagesManager.getPage(PagesManager.CYBERSPORTS_PAGE);
        }
        request.setAttribute(SAME_DESCRIPTION_EVENTS, events.getDescriptions());
        return page;
    }
}
