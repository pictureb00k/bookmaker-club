package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.SameTypeEvents;
import com.epam.totalizator.manager.PagesManager;
import com.epam.totalizator.service.EventService;
import com.epam.totalizator.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import javax.servlet.http.HttpServletRequest;

import static com.epam.totalizator.command.Constants.SAME_DESCRIPTION_EVENTS;

/**
 * Redirects to the page with the matches without coefficient
 * where they can be edited by Admin.
 * Can be used only by Admin.
 * @author Pavel
 */

public class EditEventPageCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(EditEventPageCommand.class.getName());
    private EventService eventService;

    public EditEventPageCommand(){
        eventService = new EventService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        Account account = (Account) request.getSession().getAttribute(ACCOUNT);
        String page;
        if (account != null && account.getRole() == Account.Role.ADMIN) {
            page = PagesManager.getPage(PagesManager.ADMIN_EDIT_EVENT);
            try {
                SameTypeEvents events = eventService.takeMatchesWithoutCoefficients();
                request.setAttribute(SAME_DESCRIPTION_EVENTS, events.getDescriptions());
            } catch (ServiceException e) {
                LOG.error(e);
            }
        }else {
            page = CONTROLLER_COMMAND_REDIRECT_TO_MAIN_PAGE;
        }
        return page;
    }
}
