package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.SameTypeEvents;
import com.epam.totalizator.service.EventService;
import com.epam.totalizator.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static com.epam.totalizator.command.Constants.KIND_OF_SPORT;
import static com.epam.totalizator.command.Constants.SAME_DESCRIPTION_EVENTS;

/**
 * Shows the results of ended matches with the special kind of sport.
 * @author Pavel
 */

public class SortResultsCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(SortResultsCommand.class.getName());
    private EventService eventService;

    public SortResultsCommand(){
        eventService = new EventService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        Account account = (Account) request.getSession().getAttribute(ACCOUNT);
        try {
            String kindOfSport = request.getParameter(KIND_OF_SPORT);
            SameTypeEvents events = eventService.takeResultsOfKind(kindOfSport);
            request.setAttribute(SAME_DESCRIPTION_EVENTS, events.getDescriptions());
        }catch (ServiceException e){
            LOG.error(e);
        }
        page = eventService.determineResultsPage(account);
        return page;
    }
}
