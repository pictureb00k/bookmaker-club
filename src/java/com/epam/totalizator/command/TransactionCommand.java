package com.epam.totalizator.command;
import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.Transaction;
import com.epam.totalizator.manager.PagesManager;
import com.epam.totalizator.service.BillService;
import com.epam.totalizator.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;

import static com.epam.totalizator.command.Constants.*;

/**
 * Command handles the client's transaction.
 * Used the defence from sending a second request.
 * Can be used only by Client.
 * @author Pavel
 */

public class TransactionCommand implements Command{
    private static final Logger LOG = LogManager.getLogger(TransactionCommand.class.getName());

    private BillService billService;

    public TransactionCommand(){
        billService = new BillService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute(ACCOUNT);
        if (account != null && account.getRole() == Account.Role.CLIENT) {
            String type = request.getParameter(TYPE);
            BigDecimal amount = new BigDecimal(request.getParameter(DEP_AMOUNT));
            String transactionType = request.getParameter(TRANSACTION_TYPE);
            try {
                switch (transactionType) {
                    case DEPOSIT:
                        Transaction deposit = billService.makeDeposit(account, type, amount);
                        if (deposit != null) {
                            deposit.setTransactionType(transactionType);
                            session.setAttribute(TRANSACTION, deposit);
                            page = PagesManager.getPage(PagesManager.CLIENT_SUCCESS_PAGE);
                        }
                        break;
                    case WITHDRAW:
                        Transaction withdraw = billService.withdraw(account, type, amount);
                        if (withdraw != null) {
                            withdraw.setTransactionType(transactionType);
                            session.setAttribute(TRANSACTION, withdraw);
                            page = PagesManager.getPage(PagesManager.CLIENT_SUCCESS_PAGE);
                        } else {
                            page = PagesManager.getPage(PagesManager.CLIENT_FAIL_PAGE);
                        }
                        break;
                }
            }catch (ServiceException e){
                LOG.error(e);
            }
        }else {
            page = CONTROLLER_COMMAND_REDIRECT_TO_MAIN_PAGE;
        }
        request.setAttribute(Command.DISPATCHER_TYPE, Command.SEND_REDIRECT);
        return page;
    }
}
