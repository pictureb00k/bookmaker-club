package com.epam.totalizator.command;

import com.epam.totalizator.entity.Account;
import com.epam.totalizator.entity.SameTypeEvents;
import com.epam.totalizator.manager.PagesManager;
import com.epam.totalizator.service.EventService;
import com.epam.totalizator.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static com.epam.totalizator.command.Constants.SAME_DESCRIPTION_EVENTS;

/**
 * Command redirects to the main page depending on users's role.
 * @author Pavel
 */

public class MainPageCommand implements  Command {
    private static final Logger LOG = LogManager.getLogger(MainPageCommand.class.getName());
    private static EventService eventService;

    public MainPageCommand(){
        eventService = new EventService();
    }
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        HttpSession session = request.getSession();
        Account account = (Account)session.getAttribute(ACCOUNT);
        if(account != null){
            try {
                SameTypeEvents events;
                switch (account.getRole()){
                    case ADMIN:
                        page = PagesManager.getPage(PagesManager.ADMIN_MAIN_PAGE);
                        events = eventService.takeAllUnfinishedMatches();
                        request.setAttribute(SAME_DESCRIPTION_EVENTS, events.getDescriptions());
                        break;
                    case BOOKMAKER:
                        page = PagesManager.getPage(PagesManager.BOOKMAKER_ARRANGE_PAGE);
                        events = eventService.takeMatchesWithoutCoefficients();
                        request.setAttribute(SAME_DESCRIPTION_EVENTS, events.getDescriptions());
                        break;
                    case CLIENT:
                        page = PagesManager.getPage(PagesManager.CLIENT_MAIN_PAGE);
                        break;
                    default:
                        page = PagesManager.getPage(PagesManager.MAIN_PAGE);
                }
            }catch (ServiceException e){
                LOG.error(e);
            }
        }else {
            page = PagesManager.getPage(PagesManager.MAIN_PAGE);
        }
        return page;
    }
}
