package com.epam.totalizator.command;

import com.epam.totalizator.entity.Bet;
import com.epam.totalizator.service.BetService;
import com.epam.totalizator.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;

import static com.epam.totalizator.command.Constants.BETS;
import static com.epam.totalizator.command.Constants.CURRENCY;

/**
 * Command shows all bets related to the bill with
 * currency received in the parameter. So, Admin can
 * control all transactions connected to the bill.
 * Can be used only by Admin.
 * @author Pavel
 */

public class AdminTransactionsCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(AdminTransactionsCommand.class.getName());
    private BetService betService;

    public AdminTransactionsCommand(){
        betService = new BetService();
    }

    @Override
    public String execute(HttpServletRequest request) {
        String currency = request.getParameter(CURRENCY);
        ArrayList<Bet> bets = null;
        try {
            bets = betService.takeAllBetsOfCurrency(currency);
        }catch (ServiceException e){
            LOG.error(e);
        }
        String page = request.getSession().getAttribute(PAGE).toString();
        request.setAttribute(BETS, bets);
        return page;
    }
}
