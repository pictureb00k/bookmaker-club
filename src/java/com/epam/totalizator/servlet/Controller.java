package com.epam.totalizator.servlet;

import com.epam.totalizator.command.Command;
import com.epam.totalizator.command.CommandManager;
import com.epam.totalizator.pool.ConnectionPool;
import com.epam.totalizator.test.PoolTest;
import com.epam.totalizator.test.ResourcesTest;
import com.epam.totalizator.test.ValidatorTest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * (@Controller) is the single entry point into web-application.
 * Coordinates further behavior based on the request parameters.
 * @author Pavel Bortnik
 */
@WebServlet("/controller")
public class Controller extends HttpServlet{
    private static final Logger LOGGER = LogManager.getLogger(Controller.class.getName());
    static final String COMMAND = "command";

    public Controller() {
        super();
    }

    @Override
    public void init() throws ServletException {
        super.init();
        if(!test()){
            throw new RuntimeException("Tests haven't been completed successfully!");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter(COMMAND);
        Command command = CommandManager.takeCommand(action);
        String page = command.execute(request);
        if(request.getAttribute(Command.DISPATCHER_TYPE) == Command.SEND_REDIRECT){
            response.sendRedirect(page);
        }else {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
            dispatcher.forward(request, response);
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        ConnectionPool.getInstance().closePool();
    }

    private boolean test() {
        JUnitCore runner = new JUnitCore();
        Result result = runner.run(ValidatorTest.class);
        String className;
        if (result.wasSuccessful()) {
            className = ValidatorTest.class.getSimpleName();
            logTestsResults(className, result);
        }else {
            return false;
        }
        result = runner.run(PoolTest.class);
        if (result.wasSuccessful()) {
            className = PoolTest.class.getSimpleName();
            logTestsResults(className, result);
        }else {
            return false;
        }
        result = runner.run(ResourcesTest.class);
        if (result.wasSuccessful()){
            className = ResourcesTest.class.getSimpleName();
            logTestsResults(className, result);
        }else {
            return false;
        }
        return true;
    }

    private void logTestsResults(String className, Result result){
        LOGGER.info("(" + className + ") run tests: " + result.getRunCount());
        LOGGER.info("(" + className + ") failed tests: " + result.getFailureCount());
        LOGGER.info("(" + className + ") success: " + result.wasSuccessful());
    }


}
