package com.epam.totalizator.manager;


import java.util.ResourceBundle;

/**
 * Manages pages.
 * @author Pavel
 */

public class PagesManager {

    private static PagesManager pagesManager;
    private static ResourceBundle resourceBundle;
    private static final String BUNDLE_NAME = "pages";
    public static final String MAIN_PAGE = "MAIN_PAGE";
    public static final String LOGIN_PAGE = "LOGIN_PAGE";
    public static final String REGISTER_PAGE = "REGISTER_PAGE";
    public static final String SPORTS_PAGE = "SPORTS_PAGE";
    public static final String CYBERSPORTS_PAGE = "CYBERSPORTS_PAGE";
    public static final String CLIENT_SPORT_PAGE = "CLIENT_SPORT_PAGE";
    public static final String CLIENT_CYBERSPORT_PAGE = "CLIENT_CYBERSPORT_PAGE";
    public static final String CLIENT_BET_DIALOG = "CLIENT_BET_DIALOG";
    public static final String CLIENT_BET_INFO = "CLIENT_BET_INFO";
    public static final String CLIENT_BALANCE_INFO = "CLIENT_BALANCE_INFO";
    public static final String CLIENT_MAIN_PAGE = "CLIENT_MAIN_PAGE";
    public static final String CLIENT_SUCCESS_PAGE = "CLIENT_SUCCESS_PAGE";
    public static final String CLIENT_FAIL_PAGE = "CLIENT_FAIL_PAGE";
    public static final String ADMIN_MAIN_PAGE = "ADMIN_MAIN_PAGE";
    public static final String BOOKMAKER_ARRANGE_PAGE = "BOOKMAKER_ARRANGE_PAGE";
    public static final String BOOKMAKER_UPDATE_PAGE = "BOOKMAKER_UPDATE_PAGE";
    public static final String PUT_RESULTS = "PUT_RESULTS";
    public static final String ADMIN_RESULTS = "ADMIN_RESULTS";
    public static final String RESULTS_CLIENT = "CLIENT_RESULTS";
    public static final String SUCCESS_BET = "SUCCESS_BET";
    public static final String RESUTLS_PAGE = "RESUTLS_PAGE";
    public static final String BILL_STATUS = "BILL_STATUS";
    public static final String ADMIN_EDIT_EVENT = "ADMIN_EDIT_EVENT";

    private PagesManager(){
        resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME);
    }

    /**
     * Get the page from the properties file using it's name.
     * @param key page name
     * @return String address of the page
     */
    public static String getPage(String key){
        if (pagesManager == null) {
            pagesManager = new PagesManager();
        }
        return resourceBundle.getString(key);
    }
}
