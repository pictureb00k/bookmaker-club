package com.epam.totalizator.entity;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;

/**
 * @author Pavel Bortnik
 */
public class Transaction {
    private Date date;
    private Time time;
    private String depositType;
    private BigDecimal amount;
    private TransactionType transactionType;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDepositType() {
        return depositType;
    }

    public void setDepositType(String depositType) {
        this.depositType = depositType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public String getTransactionType() {
        return transactionType.getValue();
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = TransactionType.valueOf(transactionType.toUpperCase());
    }

    public enum TransactionType {
        DEPOSIT("Deposit"),
        WITHDRAW("Withdraw");

        private String value;

        TransactionType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
