package com.epam.totalizator.entity;

import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;

/**
 * @author Pavel Bortnik
 */
public class Bet {
    private int id;
    private String accountLogin;
    private Event event;
    private String typeBet;
    private String dateOfMatch;
    private Date betDate;
    private Time betTime;
    private BigDecimal coefficient;
    private BigDecimal amount;
    private BigDecimal expectedWin;
    private String currency;
    private int result;

    public BigDecimal getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(BigDecimal coefficient) {
        this.coefficient = coefficient;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getExpectedWin() {
        return expectedWin;
    }

    public void setExpectedWin(BigDecimal expectedWin) {
        this.expectedWin = expectedWin;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public String getTypeBet() {
        return typeBet;
    }

    public void setTypeBet(String typeBet) {
        this.typeBet = typeBet;
    }

    public String getDateOfMatch() {
        return dateOfMatch;
    }

    public void setDateOfMatch(String dateOfMatch) {
        this.dateOfMatch = dateOfMatch;
    }

    public String getAccountLogin() {
        return accountLogin;
    }

    public void setAccountLogin(String accountLogin) {
        this.accountLogin = accountLogin;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public Date getBetDate() {
        return betDate;
    }

    public void setBetDate(Date betDate) {
        this.betDate = betDate;
    }

    public Time getBetTime() {
        return betTime;
    }

    public void setBetTime(Time betTime) {
        this.betTime = betTime;
    }
}
