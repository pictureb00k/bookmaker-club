package com.epam.totalizator.entity;

import java.util.ArrayList;

/**
 * List of events with same descriptions, that have same event type.
 * @author Pavel Bortnik
 */
public class SameTypeEvents {
    private String eventType;
    private String kindOfSport;
    private ArrayList<SameDescriptionEvents> descriptions;

    public SameTypeEvents() {
        descriptions = new ArrayList<>();
    }

    public boolean addListOfSameDescriptionEvents(SameDescriptionEvents sameDescriptionEvents){
        return descriptions.add(sameDescriptionEvents);
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getKindOfSport() {
        return kindOfSport;
    }

    public void setKindOfSport(String kindOfSport) {
        this.kindOfSport = kindOfSport;
    }

    public ArrayList<SameDescriptionEvents> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(ArrayList<SameDescriptionEvents> descriptions) {
        this.descriptions = descriptions;
    }
}
