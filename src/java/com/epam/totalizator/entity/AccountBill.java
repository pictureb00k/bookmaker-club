package com.epam.totalizator.entity;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * @author Pavel Bortnik
 */
public class AccountBill{
    private int id;
    private String currency;
    private BigDecimal balance;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
