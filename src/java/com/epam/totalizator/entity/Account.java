package com.epam.totalizator.entity;

import java.sql.Date;
import java.util.ArrayList;

/**
 * @author Pavel Bortnik
 */
public class Account {
    private String login;
    private String password;
    private String email;
    private String firstName;
    private String lastName;
    private Date birthday;
    private Date regDate;
    private Role role;
    private AccountBill bill;
    private ArrayList<Bet> bets;

    public Account() {
        bets = new ArrayList<>();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public AccountBill getBill() {
        return bill;
    }

    public void setBill(AccountBill bill) {
        this.bill = bill;
    }

    public void addBet(Bet bet){
        bets.add(0,bet);
    }

    public ArrayList<Bet> getBets() {
        return bets;
    }

    public void setBets(ArrayList<Bet> bets) {
        this.bets = bets;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public enum Role {
        ADMIN,
        BOOKMAKER,
        CLIENT
    }
}
