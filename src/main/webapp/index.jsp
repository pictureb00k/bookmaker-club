<%@include file="/WEB-INF/jspf/root.jspf"%>

<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="js/scripts.js"></script>
</head>
    <body>
    <c:set var="page" value="${pageContext.request.contextPath}/controller?command=redirect_to_main_page" scope="session"/>

    <div class="logo" id="main-logo">
        <a href="/controller?command=redirect_to_main_page">
            <img src="${pageContext.request.contextPath}/images/logo.png" alt="logo">
        </a>
    </div>

    <div class="lang" id="main-lang">
        <a href="#openLang">
            <div class="val"><fmt:message key="site.language"/></div>
        </a>
        <div id="openLang" class="langDialog">
            <div class="language">
                <a href="#close" title="Close" class="close">X</a>
                <br><h1><fmt:message key="language.choose"/></h1><br>
                <div class="en-picture">
                    <a href="/controller?command=change_Lang&language=en_EN">
                        <img src="${pageContext.request.contextPath}/images/en-us.png" alt="eng">
                        <figcaption>English</figcaption>
                    </a>
                </div>
                <div class="ru-picture">
                    <a href="/controller?command=change_Lang&language=ru_RU">
                        <img src="${pageContext.request.contextPath}/images/ru-ru.png" alt="rus">
                        <figcaption><fmt:message key="russian"/> </figcaption>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="header" id="main-header">
        <div class="links">
            <div class="link"><a href="/controller?command=redirect_to_sport_page"><fmt:message key="link.sport"/> </a></div>
            <div class="link"><a href="/controller?command=redirect_to_cybersport_page"><fmt:message key="link.cyber"/> </a></div>
            <div class="link"><a href="/controller?command=results_page"><fmt:message key="link.results"/> </a></div>
        </div>
        <div class="authorization">
            <a href="${pageContext.request.contextPath}/jsp/login.jsp">
                <span><fmt:message key="link.login"/></span>
            </a>
            /
            <a href="${pageContext.request.contextPath}/jsp/register.jsp">
                <fmt:message key="link.register"/>
            </a>
        </div>
    </div>

    <div class="content">
        <div class="picture"></div>
        <div class="wrap-text">
            <div class="wrap-text-value">
                <fmt:message key="hello.dep"/>
            </div>
        </div>
        <div class="wrap-info">
            <h1><fmt:message key="hello.msg"/></h1>
            <div class="wrap-info-value">
                <fmt:message key="hello.text"/>
            </div>
        </div>
        <div class="wrap-sport">
            <a href="/controller?command=redirect_to_sport_page">
                <div class="wrap-sport-value">
                    <fmt:message key="client.bet"/><br><fmt:message key="link.sport"/>
                </div>
            </a>
        </div>
        <div class="wrap-cybersport">
            <a href="/controller?command=redirect_to_cybersport_page">
                <div class="wrap-cybersport-value">
                    <fmt:message key="client.bet"/> <br><fmt:message key="link.cyber"/>
                </div>
            </a>
        </div>
        <div class="wrap-pay-methods">
            <table class="wrap-pay-methods-value">
                <tr>
                    <th><fmt:message key="hello.dep.method"/></th>
                    <th><img src="${pageContext.request.contextPath}/images/skrill.png"></th>
                    <th><img src="${pageContext.request.contextPath}/images/webmoney.png"></th>
                    <th><img src="${pageContext.request.contextPath}/images/visa.png"></th>
                    <th><img src="${pageContext.request.contextPath}/images/mastercard.png"></th>
                </tr>
            </table>
        </div>
    </div>
    <div class="footer">
        &copy; 2016 Copyright Pavel Bortnik
        <img src="${pageContext.request.contextPath}/images/age-warning.jpg">
    </div>
</body>
</html>