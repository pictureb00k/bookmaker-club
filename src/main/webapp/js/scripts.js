function time(){
	var tm=new Date();
	var h=tm.getHours();
	var m=tm.getMinutes();
	var s=tm.getSeconds();
	m=checkTime(m);
	s=checkTime(s);
	document.getElementById('time').innerHTML = h + ":" + m + ":" + s;
	setTimeout('time()', 500);
}

function checkTime(i)
{
	if (i<10)
	{
		i="0" + i;
	}
	return i;
}

function date(){
	time();
}

function hide() {
	document.getElementById("text").classList.add("hidden");
}

function changeInput() {
	var value = document.getElementById("inputBet").value;
	var coef = document.getElementById("coeff").innerHTML.valueOf();
	var expectedWin = value * coef;
	expectedWin = expectedWin.toFixed(2);
	document.getElementById("expected-win").innerHTML = expectedWin.toString();
}
