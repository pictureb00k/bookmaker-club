<%@include file="/WEB-INF/jspf/root.jspf"%>

<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="../js/scripts.js"></script>
</head>
<body onload="date()">
<%@include file="/WEB-INF/jspf/content.jspf"%>
<c:set var="page" value="${pageContext.request.contextPath}/controller?command=results_page" scope="session"/>
<div class="header">
    <div class="links">
        <div class="link"><a href="${pageContext.request.contextPath}/controller?command=redirect_to_sport_page"><fmt:message key="link.sport"/> </a></div>
        <div class="link"><a href="${pageContext.request.contextPath}/controller?command=redirect_to_cybersport_page"><fmt:message key="link.cyber"/> </a></div>
        <div class="link"><a href="${pageContext.request.contextPath}/controller?command=results_page"><fmt:message key="link.results"/> </a></div>
    </div>

    <div class="authorization">
        <a href="${pageContext.request.contextPath}/jsp/login.jsp">
            <img src="../images/log.png" alt="Login" class="authorization-icon">
            <span><fmt:message key="link.login"/></span>
        </a>
        /
        <a href="${pageContext.request.contextPath}/jsp/register.jsp">
            <img src="../images/acc.png" alt="Register" class="authorization-icon">
            <fmt:message key="link.register"/>
        </a>
    </div>
</div>


<div class="head" id="result-head">
    <table>
        <tr>
            <th style="text-align: center"><fmt:message key="date"/></th>
            <th style="text-align: center"><fmt:message key="event"/></th>
            <th style="text-align: center"><fmt:message key="result"/></th>
        </tr>
    </table>
</div>

<div class="sport-info">
    <c:forEach var="sameDescription" items="${sameDescriptionEvents}">
    <table>
        <thead class="thead" id="result-thead">
        <tr>
            <td></td>
            <td>
                <img src="${pageContext.request.contextPath}/images/${sameDescription.kindOfSport}.png" class="icon" id="result-icon">
                    ${sameDescription.description}
            </td>
            <td></td>
        </tr>
        </thead>
        <tbody class="tbody" id="result-tbody">
        <c:forEach var="event" items="${sameDescription.events}">
            <tr>
                <td>
                    <fmt:formatDate value="${event.startDate}" type="date"/>
                </td>
                <td>${event.firstCompetitor} - ${event.secondCompetitor}</td>
                <td>${event.firstScore}:${event.secondScore}</td>
            </tr>
        </c:forEach>
        </tbody>
        </c:forEach>
    </table>
</div>
<div class="type">
    <div class="football">
        <a href="${pageContext.request.contextPath}/controller?command=sort_results&event_type=sport&kindOfSport=football">
            <img src="${pageContext.request.contextPath}/images/football.png" alt="football" class="icon">
            <div class="value"><fmt:message key="type.football"/> </div>
        </a>
    </div>
    <div class="basket">
        <a href="${pageContext.request.contextPath}/controller?command=sort_results&event_type=sport&kindOfSport=basketball">
            <img src="${pageContext.request.contextPath}/images/basketball.png" alt="basket" class="icon">
            <div class="value"><fmt:message key="type.basketball"/></div>
        </a>
    </div>
    <div class="volley">
        <a href="${pageContext.request.contextPath}/controller?command=sort_results&event_type=sport&kindOfSport=volley">
            <img src="${pageContext.request.contextPath}/images/volley.png" alt="volley" class="icon">
            <div class="value"><fmt:message key="type.volleyball"/></div>
        </a>
    </div>
    <div class="hockey">
        <a href="${pageContext.request.contextPath}/controller?command=sort_results&event_type=sport&kindOfSport=hockey">
            <img src="${pageContext.request.contextPath}/images/hockey.png" alt="hockey" class="icon">
            <div class="value"><fmt:message key="type.hockey"/></div>
        </a>
    </div>
    <div class="dota">
        <a href="${pageContext.request.contextPath}/controller?command=sort_results&event_type=cybersport&kindOfSport=dota">
            <img src="${pageContext.request.contextPath}/images/dota.png" alt="dota" class="icon">
            <div class="value"><fmt:message key="type.dota"/></div>
        </a>
    </div>
    <div class="cs">
        <a href="${pageContext.request.contextPath}/controller?command=sort_results&event_type=cybersport&kindOfSport=cs">
            <img src="${pageContext.request.contextPath}/images/cs.png" alt="cs" class="icon">
            <div class="value"><fmt:message key="type.cs"/></div>
        </a>
    </div>
    <div class="world_of_tanks">
        <a href="${pageContext.request.contextPath}/controller?command=sort_results&event_type=cybersport&kindOfSport=wot">
            <img src="${pageContext.request.contextPath}/images/wot.png" alt="cs" class="icon">
            <div class="value"><fmt:message key="type.wot"/></div>
        </a>
    </div>
</div>
</body>
</html>