<%@include file="/WEB-INF/jspf/root.jspf"%>
<%@include file="/WEB-INF/jspf/account.jspf"%>
<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<body onload="date()">
<%@include file="/WEB-INF/jspf/content.jspf"%>
<%@include file="/WEB-INF/jspf/headerClient.jspf"%>
<c:set var="page" value="/controller?command=redirect_to_sport_page" scope="session"/>
<div class="head-client">
    <table>
        <tr>
            <th><fmt:message key="event"/></th>
            <th>1</th>
            <th>X</th>
            <th>2</th>
            <th>1X</th>
            <th>12</th>
            <th>X2</th>
        </tr>
    </table>
</div>

<c:set var="newBet" value="${newBet}"/>
<div class="sport-info-client" id="betting">
    <h1><fmt:message key="bet.info"/></h1>
    <h4 id="green"><fmt:message key="bet.success"/></h4>
        <label><fmt:message key="bill.status"/>:</label>
        <span><fmt:formatNumber value="${account.bill.balance}"/> ${account.bill.currency}</span><br>
        <label><fmt:message key="bet.description"/>:</label><span>${newBet.event.description}</span><br>
        <label><fmt:message key="bet.match"/>:</label><span>${newBet.event.firstCompetitor} - ${newBet.event.secondCompetitor}</span><br>
        <label><fmt:message key="bet.date"/>:</label><span>
            <fmt:formatDate value="${newBet.event.startDate}" type="date"/>
            <fmt:formatDate value="${newBet.event.startTime}" type="time" timeStyle="SHORT"/>
        </span><br>
        <label><fmt:message key="bet.type"/>:</label><span>${newBet.typeBet}</span><br>
        <label><fmt:message key="bet.coeff"/>:</label><span id="coeff">${newBet.coefficient}</span><br>
        <label><fmt:message key="bet"/>: </label>
            <span>
                ${newBet.amount}
            </span><br>
        <label><fmt:message key="bet.expected"/>:</label><span id="expected-win">${newBet.expectedWin}</span><br>
    </form>
</div>

<div class="bets-hat">
    <div><fmt:message key="bet.ticket"/></div>
</div>

<div class="bets-block">
    <div class="no-bets-block" hidden><p><fmt:message key="bet.no"/></p></div>
    <c:forEach var="bet" items="${account.bets}">
        <div id="bet-info-block">
            <label><fmt:message key="bet.match"/>:</label><span>${bet.event.firstCompetitor} - ${bet.event.secondCompetitor}</span><br>
            <label><fmt:message key="bet.type"/>:</label><span>${bet.typeBet}</span><br>
            <label><fmt:message key="bet"/>:</label><span>${bet.amount} ${account.bill.currency}</span><br>
            <label><fmt:message key="bet.date"/>:</label>
            <span>
                <fmt:formatDate value="${bet.event.startDate}" type="date"/>
                <fmt:formatDate value="${bet.event.startTime}" type="time" timeStyle="SHORT"/>
            </span><br>
            <label class="label-money"><img src="${pageContext.request.contextPath}/images/money.png"></label><span>${bet.expectedWin}  ${account.bill.currency}</span>
            <label class="label-coeff"><img src="${pageContext.request.contextPath}/images/coeff.png"></label><span class="span-coef">${bet.coefficient}</span>
            <form role="form" action="/controller" method="POST">
                <input type="hidden" name="command" value="cancel_bet">
                <input type="hidden" name="bet" value="${bet.id}">
                <input type="hidden" name="amount" value="${bet.amount}">

                <fmt:message key="bet.cancel" var="cancel"/>
                <input type="submit" id="cancel-btn" value="${cancel}">
            </form>
        </div><br>
    </c:forEach>
</div>
<c:if test="${newBet.event.eventType eq 'sport'}">
    <%@include file="/WEB-INF/jspf/sports.jspf"%>
</c:if>
<c:if test="${newBet.event.eventType eq 'cybersport'}">
    <%@include file="/WEB-INF/jspf/cybersports.jspf"%>
</c:if>
</body>
</html>