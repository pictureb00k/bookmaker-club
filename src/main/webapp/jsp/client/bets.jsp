<%@include file="/WEB-INF/jspf/root.jspf"%>
<%@include file="/WEB-INF/jspf/account.jspf"%>
<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>

<body onload="date()">
<%@include file="/WEB-INF/jspf/content.jspf"%>
<c:set var="page" value="${pageContext.request.contextPath}/jsp/client/bets.jsp" scope="session"/>
<%@include file="/WEB-INF/jspf/headerClient.jspf"%>

<div class="head">
    <div class="text"><fmt:message key="client.bet"/>. <fmt:message key="bill.id"/> ${account.bill.id}</div>
</div>

<div class="balance-info">
    <div class="bill-status"><fmt:message key="bill.status"/>:
        <span><fmt:formatNumber value="${account.bill.balance}"/> ${account.bill.currency}</span>
    </div>
    <form role="form" method="post" action="/controller">
        <input type="hidden" name="command" value="bet_info"/>
        <select class="bill-info-select" name="betType">
            <option value="all"><fmt:message key="bill.status.all"/></option>
            <option value="win"><fmt:message key="bet.status.win"/></option>
            <option value="lose"><fmt:message key="bet.status.loss"/></option>
        </select>
        <fmt:message key="bill.btn.show" var="shw"/>
        <input type="submit" class="btn" id="bill-info-btn" value="${shw}">
    </form>
</div>
<div class="balance-info-result" id="balance-info-result" hidden>
    <table>
        <thead class="thead" id="balance-info-thead">
        <tr>
            <td><fmt:message key="bet.match"/></td>
            <td><fmt:message key="result"/></td>
            <td><fmt:message key="bet.type"/></td>
            <td><fmt:message key="deposit.amount"/></td>
            <td><fmt:message key="bet.coeff"/></td>
            <td><fmt:message key="bet.status"/></td>
        </tr>
        </thead>
        <c:forEach var="bet" items="${bets}">
            <script type="text/javascript" src="${pageContext.request.contextPath}/js/show-block.js"></script>
            <tbody id="balance-info-tbody">
            <tr>
                <td style="width: 220px"><img id="betImage" src="${pageContext.request.contextPath}/images/${bet.event.kindOfSport}.png" style="width: 30px">
                 <span id="match-value">${bet.event.firstCompetitor} - ${bet.event.secondCompetitor}</span>
                </td>
                <td>
                   ${bet.event.firstScore}:${bet.event.secondScore}
                </td>
                <td>${bet.typeBet}</td>
                <td>${bet.amount} ${account.bill.currency}</td>
                <td>${bet.coefficient}</td>
                <td>
                    <c:choose>
                        <c:when test="${bet.result == -1}">
                            Playing
                        </c:when>
                        <c:when test="${bet.result == 1}">
                            <fmt:message key="bet.status.win"/>: <span id="win-money">+${bet.expectedWin} ${account.bill.currency}</span>
                        </c:when>
                        <c:when test="${bet.result == 0}">
                            <fmt:message key="bet.status.loss"/>: <span id="lose-money">-${bet.amount} ${account.bill.currency}</span>
                        </c:when>
                    </c:choose>
                </td>
            </tr>
            </tbody>
        </c:forEach>
    </table>
</div>

<div class="bets-hat">
    <div><fmt:message key="bet.ticket"/></div>
</div>
<div class="bets-block">
    <div class="no-bets-block" id="no-bet"><p><fmt:message key="bet.no"/></p></div>
    <c:forEach var="bet" items="${account.bets}">
        <script type="text/javascript">
            document.getElementById("no-bet").classList.add("hidden");
        </script>
        <div id="bet-info-block">
            <label><fmt:message key="bet.match"/>:</label><span>${bet.event.firstCompetitor} - ${bet.event.secondCompetitor}</span><br>
            <label><fmt:message key="bet.type"/>:</label><span>${bet.typeBet}</span><br>
            <label><fmt:message key="bet"/>:</label><span>${bet.amount} ${account.bill.currency}</span><br>
            <label><fmt:message key="bet.date"/>:</label>
            <span>
                <fmt:formatDate value="${bet.event.startDate}" type="date"/>
                <fmt:formatDate value="${bet.event.startTime}" type="time" timeStyle="SHORT"/>
            </span><br>
            <label class="label-money"><img src="${pageContext.request.contextPath}/images/money.png"></label><span>${bet.expectedWin}  ${account.bill.currency}</span>
            <label class="label-coeff"><img src="${pageContext.request.contextPath}/images/coeff.png"></label><span class="span-coef">${bet.coefficient}</span>
            <form role="form" action="/controller" method="POST">
                <input type="hidden" name="command" value="cancel_bet">
                <input type="hidden" name="bet" value="${bet.id}">
                <input type="hidden" name="amount" value="${bet.amount}">

                <fmt:message key="bet.cancel" var="cancel"/>
                <input type="submit" id="cancel-btn" value="${cancel}">
            </form>
        </div><br>
    </c:forEach>
</div>

<%@include file="/WEB-INF/jspf/clientActions.jspf"%>
</body>
</html>