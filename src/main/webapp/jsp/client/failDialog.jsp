<%@include file="/WEB-INF/jspf/root.jspf"%>
<%@include file="/WEB-INF/jspf/account.jspf"%>
<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<body onload="date()">

<%@include file="/WEB-INF/jspf/content.jspf"%>
<c:set var="page" value="${pageContext.request.contextPath}/jsp/client/withdrawClient.jsp" scope="session"/>
<%@include file="/WEB-INF/jspf/headerClient.jspf"%>

<div class="head">
    <div class="text"><fmt:message key="client.cashout"/>. <fmt:message key="bill.id"/> ${account.bill.id}</div>
</div>

<div class="deposit-info">
    <div class="dep-header">
        <div class="pay-syst"><fmt:message key="witdraw"/></div>
        <div class="commission"><fmt:message key="deposit.fee"/></div>
        <div class="proc-time"><fmt:message key="deposit.time"/></div>
        <div class="amount"><fmt:message key="deposit.amount"/></div>
        <div class="dep"></div>
    </div>
    <div class="dep-body" id="skrill">
        <div class="pay-syst">
            <img src="${pageContext.request.contextPath}/images/skrill.png" alt="skrill"/>
        </div>
        <div class="commission">0%</div>
        <div class="proc-time"><fmt:message key="deposit.inst"/></div>
        <form method="post" action="/controller">
            <input type="hidden" name="command" value="transaction"/>
            <input type="hidden" name="type" value="skrill"/>
            <input type="hidden" name="transType" value="withdraw"/>
            <div class="amount">
                <input type="number" id="dep-amount" name="dep-amount" value="0.00" min ="0.01" step="0.01" autocomplete="off"/>
                ${account.bill.currency}
            </div>
            <div class="dep">
                <fmt:message key="withdraw.btn" var="dep"/>
                <input type="submit" class="btn" id="dep-btn" value=${dep}>
            </div>
        </form>
    </div>

    <div class="dep-body" id="webmoney">
        <div class="pay-syst">
            <img src="${pageContext.request.contextPath}/images/webmoney.png" />
        </div>
        <div class="commission">0%</div>
        <div class="proc-time"><fmt:message key="deposit.inst"/></div>
        <form method="get" action="/controller">
            <input type="hidden" name="command" value="transaction"/>
            <input type="hidden" name="type" value="webmoney"/>
            <input type="hidden" name="transType" value="withdraw"/>
            <div class="amount">
                <input type="number" id="dep-amount" name="dep-amount" value="0.00" min ="0.01" step="0.01" autocomplete="off"/>
                ${account.bill.currency}
            </div>
            <div class="dep">
                <fmt:message key="withdraw.btn" var="dep"/>
                <input type="submit" class="btn" id="dep-btn" value=${dep}>
            </div>
        </form>
    </div>

    <div class="dep-body" id="mastercard">
        <div class="pay-syst">
            <img src="${pageContext.request.contextPath}/images/mastercard.png" alt="mastercatd"/>
        </div>
        <div class="commission">0%</div>
        <div class="proc-time"><fmt:message key="deposit.inst"/></div>
        <form method="get" action="/controller">
            <input type="hidden" name="command" value="transaction"/>
            <input type="hidden" name="type" value="mastercard"/>
            <input type="hidden" name="transType" value="withdraw"/>
            <div class="amount">
                <input type="number" id="dep-amount" name="dep-amount" value="0.00" min ="0.01" step="0.01" autocomplete="off"/>
                ${account.bill.currency}
            </div>
            <div class="dep">
                <fmt:message key="withdraw.btn" var="dep"/>
                <input type="submit" class="btn" id="dep-btn" value=${dep}>
            </div>
        </form>
    </div>
    <div class="dep-body" id="visa">
        <div class="pay-syst">
            <img src="${pageContext.request.contextPath}/images/visa.png" alt="visa"/>
        </div>
        <div class="commission">0%</div>
        <div class="proc-time"><fmt:message key="deposit.inst"/></div>
        <form method="get" action="/controller">
            <input type="hidden" name="command" value="transaction"/>
            <input type="hidden" name="type" value="visa"/>
            <input type="hidden" name="transType" value="withdraw"/>
            <div class="amount">
                <input type="number" id="dep-amount" name="dep-amount" value="0.00" min ="0.01" step="0.01" autocomplete="off"/>
                ${account.bill.currency}
            </div>
            <div class="dep">
                <fmt:message key="withdraw.btn" var="dep"/>
                <input type="submit" class="btn" id="dep-btn" value=${dep}>
            </div>
        </form>
    </div>
</div>

<div class="infoDialog">
    <c:set var="transaction" value="${transaction}"/>
    <div class="status_value">
            <h1><fmt:message key="with.fail"/> </h1>
            <h3><fmt:message key="with.fail.info"/> </h3>
        <div class="bill-status" id="success"><fmt:message key="bill.status"/>:
            <span><fmt:formatNumber value="${account.bill.balance}"/> ${account.bill.currency}</span>
        </div>
        <a href="${pageContext.request.contextPath}/jsp/client/withdraw.jsp">
            <input type="submit" id="ok-submit" class="btn" value="OK">
        </a>
    </div>
</div>

<%@include file="/WEB-INF/jspf/clientActions.jspf"%>
</body>
</html>