<%@include file="/WEB-INF/jspf/root.jspf"%>
<%@include file="/WEB-INF/jspf/account.jspf"%>
<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<body onload="date()">

<%@include file="/WEB-INF/jspf/content.jspf"%>
<c:set var="page" value="${pageContext.request.contextPath}/jsp/client/deposit.jsp" scope="session"/>
<%@include file="/WEB-INF/jspf/headerClient.jspf"%>

<div class="head" id="main">
    <div class="text"><fmt:message key="client.deposit"/>. <fmt:message key="bill.id"/> ${account.bill.id}</div>
</div>

<div class="deposit-info">
    <div class="dep-header">
        <div class="pay-syst"><fmt:message key="trans.paySystem"/></div>
        <div class="commission"><fmt:message key="deposit.fee"/></div>
        <div class="proc-time"><fmt:message key="deposit.time"/></div>
        <div class="amount"><fmt:message key="deposit.amount"/></div>
        <div class="dep"></div>
    </div>
    <div class="dep-body" id="skrill">
        <div class="pay-syst">
            <img src="${pageContext.request.contextPath}/images/skrill.png" alt="skrill"/>
        </div>
        <div class="commission">0%</div>
        <div class="proc-time"><fmt:message key="deposit.inst"/></div>
        <form method="post" action="/controller">
            <input type="hidden" name="command" value="transaction"/>
            <input type="hidden" name="type" value="skrill"/>
            <input type="hidden" name="transType" value="deposit"/>
            <div class="amount">
                <input type="number" id="dep-amount" name="dep-amount" value="5.00" min ="5.00" max="50000"
                       step="0.01" autocomplete="off"/>
                ${account.bill.currency}
            </div>
            <div class="dep">
                <fmt:message key="deposit.short" var="dep"/>
                <input type="submit" class="btn" id="dep-btn" value=${dep}>
            </div>
        </form>
    </div>

    <div class="dep-body" id="webmoney">
        <div class="pay-syst">
            <img src="${pageContext.request.contextPath}/images/webmoney.png" />
        </div>
        <div class="commission">0%</div>
        <div class="proc-time"><fmt:message key="deposit.inst"/></div>
        <form method="get" action="/controller">
            <input type="hidden" name="command" value="transaction"/>
            <input type="hidden" name="type" value="webmoney"/>
            <input type="hidden" name="transType" value="deposit"/>
            <div class="amount">
                <input type="number" id="dep-amount" name="dep-amount" value="5.00" min ="5.00" max="50000"
                       step="0.01" autocomplete="off"/>
                ${account.bill.currency}
            </div>
            <div class="dep">
                <fmt:message key="deposit.short" var="dep"/>
                <input type="submit" class="btn" id="dep-btn" value=${dep}>
            </div>
        </form>
    </div>

    <div class="dep-body" id="mastercard">
        <div class="pay-syst">
            <img src="${pageContext.request.contextPath}/images/mastercard.png" alt="mastercatd"/>
        </div>
        <div class="commission">0%</div>
        <div class="proc-time"><fmt:message key="deposit.inst"/></div>
        <form method="get" action="/controller">
            <input type="hidden" name="command" value="transaction"/>
            <input type="hidden" name="type" value="mastercard"/>
            <input type="hidden" name="transType" value="deposit"/>
            <div class="amount">
                <input type="number" id="dep-amount" name="dep-amount" value="5.00" min ="5.00" max="50000"
                       step="0.01" autocomplete="off"/>
                ${account.bill.currency}
            </div>
            <div class="dep">
                <fmt:message key="deposit.short" var="dep"/>
                <input type="submit" class="btn" id="dep-btn" value=${dep}>
            </div>
        </form>
    </div>
    <div class="dep-body" id="visa">
        <div class="pay-syst">
            <img src="${pageContext.request.contextPath}/images/visa.png" alt="visa"/>
        </div>
        <div class="commission">0%</div>
        <div class="proc-time"><fmt:message key="deposit.inst"/></div>
        <form method="get" action="/controller">
            <input type="hidden" name="command" value="transaction"/>
            <input type="hidden" name="type" value="visa"/>
            <input type="hidden" name="transType" value="deposit"/>
            <div class="amount">
                <input type="number" id="dep-amount" name="dep-amount" value="5.00" min ="5.00" max="50000"
                       step="0.01" autocomplete="off"/>
                ${account.bill.currency}
            </div>
            <div class="dep">
                <fmt:message key="deposit.short" var="dep"/>
                <input type="submit" class="btn" id="dep-btn" value=${dep}>
            </div>
        </form>
    </div>
</div>

<%@include file="/WEB-INF/jspf/clientActions.jspf"%>
</body>
</html>