<%@include file="/WEB-INF/jspf/root.jspf"%>
<%@include file="/WEB-INF/jspf/account.jspf"%>
<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<body onload="date()">
<%@include file="/WEB-INF/jspf/content.jspf"%>
<c:set var="page" value="${pageContext.request.contextPath}/jsp/client/settings.jsp" scope="session"/>
<%@include file="/WEB-INF/jspf/headerClient.jspf"%>
<div class="head" id="main">
    <div class="text"><fmt:message key="client.settings"/>. <fmt:message key="bill.id"/> ${account.bill.id}</div>
</div>

<div class="settings-info">
    <div class="birthday"><fmt:message key="birthday"/> <fmt:formatDate value="${account.birthday}"/></div>
    <div class="reg-date"><fmt:message key="reg-date"/> <fmt:formatDate value="${account.regDate}"/></div>
</div>

<div class="change-pass">
        <h2><fmt:message key="pass.change"/></h2>
        <ctg:info-tag message="${fail}">
            <h2 id="red"><fmt:message key="${fail}"/></h2>
        </ctg:info-tag>
        <ctg:info-tag message="${success}">
            <h2 class="sportSuccess"><fmt:message key="${success}"/></h2>
        </ctg:info-tag>
        <form role="form" method="post" action="/controller">
            <input type="hidden" name="command" value="change_password"/>
            <label><fmt:message key="pass.old"/></label><br>
            <input type="password" class="required" id="oldPass" name="oldPass" required><br>
            <label><fmt:message key="pass.new"/></label><br>
            <input type="password" class="required" id="newPass" name="newPass" required autocomplete = "off" pattern="[A-z0-9_-]{6,18}"
                   title="Choose a new password. Six or more characters"><br>
            <label><fmt:message key="pass.confirm"/></label><br>
            <input type="password" class="required" id="confirm" name="confirm" required autocomplete = "off"
                   pattern="[A-z0-9_-]{6,18}" title="Choose a new password. Six or more characters"><br>
            <fmt:message key="save" var="sv"/>
            <input type="submit" id="save-btn" value="${sv}">
        </form>
</div>
<%@include file="/WEB-INF/jspf/clientActions.jspf"%>
</body>
</html>