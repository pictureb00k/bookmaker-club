<%@include file="/WEB-INF/jspf/root.jspf"%>
<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<body onload="date()">
<%@include file="/WEB-INF/jspf/content.jspf"%>
<%@include file="/WEB-INF/jspf/account.jspf"%>
<c:set var="page" value="${pageContext.request.contextPath}/controller?command=results_page" scope="session"/>
<%@include file="/WEB-INF/jspf/headerClient.jspf"%>

<div class="head-client">
    <table id="results">
        <tr>
            <th style="text-align: center"><fmt:message key="date"/></th>
            <th style="text-align: center"><fmt:message key="event"/></th>
            <th style="text-align: center"><fmt:message key="result"/></th>
        </tr>
    </table>
</div>

<div class="sport-info-client">
    <c:forEach var="sameDescription" items="${sameDescriptionEvents}">
    <table>
        <thead class="thead" id="result-thead">
        <tr>
            <td colspan="3"><img src="${pageContext.request.contextPath}/images/${sameDescription.kindOfSport}.png" class="icon" id="result-icon">
                    ${sameDescription.description}
            </td>
        </tr>
        </thead>
        <tbody class="tbody" id="result-tbody">
        <c:forEach var="event" items="${sameDescription.events}">
            <tr>
                <td>
                    <fmt:formatDate value="${event.startDate}" type="date"/>
                </td>
                <td>${event.firstCompetitor} - ${event.secondCompetitor}</td>
                <td>${event.firstScore}:${event.secondScore}</td>
            </tr>
        </c:forEach>
        </tbody>
        </c:forEach>
    </table>
</div>

<div class="bets-hat">
    <div><fmt:message key="bet.ticket"/></div>
</div>
<div class="bets-block">
    <div class="no-bets-block" id="no-bet"><p><fmt:message key="bet.no"/></p></div>
    <c:forEach var="bet" items="${account.bets}">
        <script type="text/javascript">
            document.getElementById("no-bet").classList.add("hidden");
        </script>
        <div id="bet-info-block">
            <label><fmt:message key="bet.match"/>:</label><span>${bet.event.firstCompetitor} - ${bet.event.secondCompetitor}</span><br>
            <label><fmt:message key="bet.type"/>:</label><span>${bet.typeBet}</span><br>
            <label><fmt:message key="bet"/>:</label><span>${bet.amount} ${account.bill.currency}</span><br>
            <label><fmt:message key="bet.date"/>:</label>
            <span>
                <fmt:formatDate value="${bet.event.startDate}" type="date"/>
                <fmt:formatDate value="${bet.event.startTime}" type="time" timeStyle="SHORT"/>
            </span><br>
            <label class="label-money"><img src="${pageContext.request.contextPath}/images/money.png"></label><span>${bet.expectedWin}  ${account.bill.currency}</span>
            <label class="label-coeff"><img src="${pageContext.request.contextPath}/images/coeff.png"></label><span class="span-coef">${bet.coefficient}</span>
            <form role="form" action="/controller" method="POST">
                <input type="hidden" name="command" value="cancel_bet">
                <input type="hidden" name="bet" value="${bet.id}">
                <input type="hidden" name="amount" value="${bet.amount}">

                <fmt:message key="bet.cancel" var="cancel"/>
                <input type="submit" id="cancel-btn" value="${cancel}">
            </form>
        </div><br>
    </c:forEach>
</div>

<div class="type">
    <div class="football">
        <a href="${pageContext.request.contextPath}/controller?command=sort_results&event_type=sport&kindOfSport=football">
            <img src="${pageContext.request.contextPath}/images/football.png" alt="football" class="icon">
            <div class="value"><fmt:message key="type.football"/> </div>
        </a>
    </div>
    <div class="basket">
        <a href="${pageContext.request.contextPath}/controller?command=sort_results&event_type=sport&kindOfSport=basketball">
            <img src="${pageContext.request.contextPath}/images/basketball.png" alt="basket" class="icon">
            <div class="value"><fmt:message key="type.basketball"/></div>
        </a>
    </div>
    <div class="volley">
        <a href="${pageContext.request.contextPath}/controller?command=sort_results&event_type=sport&kindOfSport=volley">
            <img src="${pageContext.request.contextPath}/images/volley.png" alt="volley" class="icon">
            <div class="value"><fmt:message key="type.volleyball"/></div>
        </a>
    </div>
    <div class="hockey">
        <a href="${pageContext.request.contextPath}/controller?command=sort_results&event_type=sport&kindOfSport=hockey">
            <img src="${pageContext.request.contextPath}/images/hockey.png" alt="hockey" class="icon">
            <div class="value"><fmt:message key="type.hockey"/></div>
        </a>
    </div>
    <div class="dota">
        <a href="${pageContext.request.contextPath}/controller?command=sort_results&event_type=cybersport&kindOfSport=dota">
            <img src="${pageContext.request.contextPath}/images/dota.png" alt="dota" class="icon">
            <div class="value"><fmt:message key="type.dota"/></div>
        </a>
    </div>
    <div class="cs">
        <a href="${pageContext.request.contextPath}/controller?command=sort_results&event_type=cybersport&kindOfSport=cs">
            <img src="${pageContext.request.contextPath}/images/cs.png" alt="cs" class="icon">
            <div class="value"><fmt:message key="type.cs"/></div>
        </a>
    </div>
    <div class="world_of_tanks">
        <a href="${pageContext.request.contextPath}/controller?command=sort_results&event_type=cybersport&kindOfSport=wot">
            <img src="${pageContext.request.contextPath}/images/wot.png" alt="cs" class="icon">
            <div class="value"><fmt:message key="type.wot"/></div>
        </a>
    </div>
</div>
</body>
</html>