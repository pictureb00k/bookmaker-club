<%@include file="/WEB-INF/jspf/root.jspf"%>
<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<body onload="date()">
<%@include file="/WEB-INF/jspf/content.jspf"%>
<%@include file="/WEB-INF/jspf/account.jspf"%>
<c:set var="page" value="${pageContext.request.contextPath}/controller?command=redirect_to_sport_page" scope="session"/>
<%@include file="/WEB-INF/jspf/headerClient.jspf"%>
<div class="head-client">
    <table>
        <tr>
            <th colspan="2"><fmt:message key="event"/></th>
            <th>1</th>
            <th>X</th>
            <th>2</th>
            <th>1X</th>
            <th>12</th>
            <th>X2</th>
        </tr>
    </table>
</div>

<div class="sport-info-client" >
    <div class="sport-info-text" id="info-text">
        <p><fmt:message key="info.matches"/></p>
    </div>
    <table>
        <c:forEach var="sameDescription" items="${sameDescriptionEvents}">
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/hide-text.js"></script>
        <thead class="thead">
        <tr>
            <td class="thead-icon"><img src="images/${sameDescription.kindOfSport}.png" alt="img"></td>
            <td class="match-name" colspan="8">${sameDescription.description}</td>
        </tr>
        </thead>
        <tbody class="tbody">
        <c:forEach var="event" items="${sameDescription.events}">
        <tr>
            <td class="thead-icon">${event.id}</td>
            <td class="match-name">${event.firstCompetitor} - ${event.secondCompetitor}</td>
            <td class="match-time">
                <fmt:formatDate value="${event.startDate}" type="date"/>
                <fmt:formatDate value="${event.startTime}" type="time" timeStyle="SHORT"/>
            </td>
            <td class="coef">
                <a href="/controller?command=open_bet&event=${event.id}&coef=${event.coefficients.winFirst}&betType=First victory">
                    ${event.coefficients.winFirst}
            </td>
            <td class="coef">
                <a href="/controller?command=open_bet&event=${event.id}&coef=${event.coefficients.nobody}&betType=Dead heat">
                ${event.coefficients.nobody}
            </td>
            <td class="coef">
                <a href="/controller?command=open_bet&event=${event.id}&coef=${event.coefficients.winSecond}&betType=Second victory">
                ${event.coefficients.winSecond}
            </td>
            <td class="coef">
                <a href="/controller?command=open_bet&event=${event.id}&coef=${event.coefficients.firstOrNobody}&betType=First victory or dead heat">
                ${event.coefficients.firstOrNobody}
            </td>
            <td class="coef">
                <a href="/controller?command=open_bet&event=${event.id}&coef=${event.coefficients.firstOrSecond}&betType=First or Second victory">
                ${event.coefficients.firstOrSecond}
            </td>
            <td class="coef">
                <a href="/controller?command=open_bet&event=${event.id}&coef=${event.coefficients.secondOrNobody}&betType=Second victory or dead heat">
                ${event.coefficients.secondOrNobody}
            </td>
        </tr>
        </c:forEach>
        </tbody>
        </c:forEach>
    </table>
</div>

<div class="bets-hat">
    <div><fmt:message key="bet.ticket"/></div>
</div>

<div class="bets-block">
    <div class="no-bets-block" id="no-bet"><p><fmt:message key="bet.no"/></p></div>
    <c:forEach var="bet" items="${account.bets}">
        <script type="text/javascript">
            document.getElementById("no-bet").classList.add("hidden");
        </script>
    <div id="bet-info-block">
        <label><fmt:message key="bet.match"/>:</label><span>${bet.event.firstCompetitor} - ${bet.event.secondCompetitor}</span><br>
        <label><fmt:message key="bet.type"/>:</label><span>${bet.typeBet}</span><br>
        <label><fmt:message key="bet"/>:</label><span>${bet.amount} ${account.bill.currency}</span><br>
        <label><fmt:message key="bet.date"/>:</label>
            <span>
                <fmt:formatDate value="${bet.event.startDate}" type="date"/>
                <fmt:formatDate value="${bet.event.startTime}" type="time" timeStyle="SHORT"/>
            </span><br>
        <label class="label-money"><img src="${pageContext.request.contextPath}/images/money.png"></label><span>${bet.expectedWin}  ${account.bill.currency}</span>
        <label class="label-coeff"><img src="${pageContext.request.contextPath}/images/coeff.png"></label><span class="span-coef">${bet.coefficient}</span><br>
        <form role="form" action="/controller" method="POST">
            <input type="hidden" name="command" value="cancel_bet">
            <input type="hidden" name="bet" value="${bet.id}">
            <input type="hidden" name="amount" value="${bet.amount}">
            <ctg:info-tag message="${fail}">
                <h3 id="red" style="margin: inherit"><fmt:message key="${fail}"/></h3>
            </ctg:info-tag>
            <fmt:message key="bet.cancel" var="cancel"/>
            <input type="submit" id="cancel-btn" value="${cancel}">
        </form>
    </div><br>
    </c:forEach>
</div>

<%@include file="/WEB-INF/jspf/sports.jspf"%>
</body>
</html>