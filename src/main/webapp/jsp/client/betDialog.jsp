<%@include file="/WEB-INF/jspf/root.jspf"%>
<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<body onload="date()">

<%@include file="/WEB-INF/jspf/content.jspf"%>
<%@include file="/WEB-INF/jspf/account.jspf"%>
<%@include file="/WEB-INF/jspf/headerClient.jspf"%>

<c:set var="page" value="${pageContext.request.contextPath}/jsp/client/betDialog.jsp" scope="session"/>
<div class="head-client">
    <table>
        <tr>
            <th><fmt:message key="event"/></th>
            <th>1</th>
            <th>X</th>
            <th>2</th>
            <th>1X</th>
            <th>12</th>
            <th>X2</th>
        </tr>
    </table>
</div>

<c:set var="event" value="${event}"/>
<div class="sport-info-client" id="betting">
    <h1><fmt:message key="bet.info"/></h1>
    <ctg:info-tag message="${fail}">
        <h4 id="red"><fmt:message key="${fail}"/></h4>
    </ctg:info-tag>
    <form role="form" method="post" action="/controller">
        <input type="hidden" name="command" value="make_bet"/>
        <input type="hidden" name="event" value="${event.id}"/>
        <input type="hidden" name="coef" value="${coef}"/>
        <input type="hidden" name="betType" value="${betType}"/>
        <label><fmt:message key="bill.status"/>:</label>
        <span><fmt:formatNumber value="${account.bill.balance}"/> ${account.bill.currency}</span><br>
        <label><fmt:message key="bet.description"/>:</label><span>${event.description}</span><br>
        <label><fmt:message key="bet.match"/>:</label><span>${event.firstCompetitor} - ${event.secondCompetitor}</span><br>
        <label><fmt:message key="bet.date"/>:</label><span>
            <fmt:formatDate value="${event.startDate}" type="date"/>
            <fmt:formatDate value="${event.startTime}" type="time" timeStyle="SHORT"/>
        </span><br>
        <label><fmt:message key="bet.type"/>:</label><span>${betType}</span><br>
        <label><fmt:message key="bet.coeff"/>:</label><span id="coeff">${coef}</span><br>
        <label><fmt:message key="bet"/> (0.01 - ... ${account.bill.currency}): </label>
            <span>
                <input onchange=changeInput() class="inputBet" id="inputBet" name="amount" type="number" value="0.01" min="0.01" step="0.01" >
            </span><br>
        <label><fmt:message key="bet.expected"/>:</label><span id="expected-win">0.00</span><br>
        <fmt:message key="bet.submit" var="sb"/>
        <input type="submit" id="bet-submit" class="btn" value="${sb}">
    </form>
</div>

<div class="bets-hat">
    <div><fmt:message key="bet.ticket"/></div>
</div>

<div class="bets-block">
    <div class="no-bets-block" id="no-bet"><p><fmt:message key="bet.no"/></p></div>
    <c:forEach var="bet" items="${account.bets}">
        <script type="text/javascript">
            document.getElementById("no-bet").classList.add("hidden");
        </script>
        <div id="bet-info-block">
            <label><fmt:message key="bet.match"/>:</label><span>${bet.event.firstCompetitor} - ${bet.event.secondCompetitor}</span><br>
            <label><fmt:message key="bet.type"/>:</label><span>${bet.typeBet}</span><br>
            <label><fmt:message key="bet"/>:</label><span>${bet.amount} ${account.bill.currency}</span><br>
            <label><fmt:message key="bet.date"/>:</label>
            <span>
                <fmt:formatDate value="${bet.event.startDate}" type="date"/>
                <fmt:formatDate value="${bet.event.startTime}" type="time" timeStyle="SHORT"/>
            </span><br>
            <label class="label-money"><img src="${pageContext.request.contextPath}/images/money.png"></label><span>${bet.expectedWin}  ${account.bill.currency}</span>
            <label class="label-coeff"><img src="${pageContext.request.contextPath}/images/coeff.png"></label><span class="span-coef">${bet.coefficient}</span>
            <form role="form" action="/controller" method="POST">
                <input type="hidden" name="command" value="cancel_bet">
                <input type="hidden" name="bet" value="${bet.id}">
                <input type="hidden" name="amount" value="${bet.amount}">

                <fmt:message key="bet.cancel" var="cancel"/>
                <input type="submit" id="cancel-btn" value="${cancel}">
            </form>
        </div><br>
    </c:forEach>
</div>

<c:if test="${event.eventType eq 'sport'}">
    <%@include file="/WEB-INF/jspf/sports.jspf"%>
</c:if>
<c:if test="${event.eventType eq 'cybersport'}">
    <%@include file="/WEB-INF/jspf/cybersports.jspf"%>
</c:if>
</body>
</html>