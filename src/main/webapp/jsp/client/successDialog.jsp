<%@include file="/WEB-INF/jspf/root.jspf"%>
<%@include file="/WEB-INF/jspf/account.jspf"%>
<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<body onload="date()">
<%@include file="/WEB-INF/jspf/content.jspf"%>
<c:set var="page" value="${pageContext.request.contextPath}/jsp/client/balanceClient.jsp" scope="session"/>
<%@include file="/WEB-INF/jspf/headerClient.jspf"%>
<div class="head">
    <div class="text"><fmt:message key="client.balance"/>. <fmt:message key="bill.id"/> ${account.bill.id}</div>
</div>
<div class="balance-info">
    <div class="bill-status"><fmt:message key="bill.status"/>:
        <span><fmt:formatNumber value="${account.bill.balance}"/> ${account.bill.currency}</span>
    </div>
    <form role="form" method="post" action="/controller">
        <input type="hidden" name="command" value="bill_info"/>
        <select class="bill-info-select" name="info_type">
            <option value="all"><fmt:message key="bill.status.all"/></option>
            <option value="deposit"><fmt:message key="bill.status.deposits"/></option>
            <option value="withdraw"><fmt:message key="bill.status.withdraws"/></option>
        </select>
        <fmt:message key="bill.btn.show" var="shw"/>
        <input type="submit" class="btn" id="bill-info-btn" value="${shw}">
    </form>
</div>

<div class="infoDialog">
    <c:set var="transaction" value="${transaction}"/>
    <div class="status_value">
        <c:if test="${transaction.transactionType == 'Deposit'}">
            <h1><fmt:message key="dep.success"/>!</h1>
            <label><fmt:message key="trans.paySystem"/>:</label><span>${transaction.depositType}</span><br>
            <label><fmt:message key="trans.info.dep"/>:</label>
            <span id="win-money">
                <fmt:formatNumber value="${transaction.amount}"/> ${account.bill.currency}
            </span><br>
        </c:if>
        <c:if test="${transaction.transactionType == 'Withdraw'}">
            <h1><fmt:message key="with.success"/>!</h1>
            <label><fmt:message key="trans.paySystem"/>:</label><span>${transaction.depositType}</span><br>
            <label><fmt:message key="trans.info.withdraw"/>:</label> <span id="win-money"><fmt:formatNumber value="${transaction.amount}"/> ${account.bill.currency}</span><br>
        </c:if>
         <div class="bill-status" id="success"><fmt:message key="bill.status"/>:
             <span><fmt:formatNumber value="${account.bill.balance}"/> ${account.bill.currency}</span>
         </div>
           <a href="${pageContext.request.contextPath}/jsp/client/balance.jsp">
               <input type="submit" id="ok-submit" class="btn" value="OK">
           </a>
        </div>
</div>

<%@include file="/WEB-INF/jspf/clientActions.jspf"%>
</body>
</html>