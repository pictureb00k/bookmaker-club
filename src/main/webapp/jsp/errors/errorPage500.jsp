<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isErrorPage="true" isELIgnored="false" %>
<% response.setStatus(500); %>

<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="js/scripts.js"></script>
</head>
<body>
    <div class="error-page">
        <h1>OOPS... There is an 500 internal server error.</h1>
        <button type="button" name="back" onclick="history.back()" class="btn">Back</button>
    </div>
</body>
</html>