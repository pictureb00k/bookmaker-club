<%@ page isErrorPage="true" import="java.io.*" contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<fmt:setBundle basename="text"/>

<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="js/scripts.js"></script>
</head>
    <body>
        <h1 style="color: wheat">The reason of the fatal problem is:</h1>
        <h1> <%=exception.getMessage() != null ? exception.getMessage() : "Some internal problem. Check the logs"%> </h1>
    </body>
</html>


