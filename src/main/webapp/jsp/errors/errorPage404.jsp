<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isErrorPage="true" isELIgnored="false" %>
<% response.setStatus(404); %>
<html>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="js/scripts.js"></script>
</head>
<body>
<div class="container">
    <div class="error-page">
        <h1>404 Not found</h1>
        <img src="${pageContext.request.contextPath}/images/no.gif"><br>
        <button type="button" name="back" onclick="history.back()" class="btn">Back</button>
    </div>
</div>
</body>
</html>