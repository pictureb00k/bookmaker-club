<%@include file="/WEB-INF/jspf/root.jspf"%>
<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<body onload="date()">
<%@include file="/WEB-INF/jspf/content.jspf"%>
<c:set var="page" value="${pageContext.request.contextPath}/controller?command=edit_event_page" scope="session"/>
<%@include file="/WEB-INF/jspf/headerAdmin.jspf"%>

<div class="head" id="main">
    <table>
        <tr>
            <th colspan="5"><fmt:message key="admin.edit"/></th>
        </tr>
    </table>
</div>

<div class="sport-info">
    <c:forEach var="sameDescription" items="${sameDescriptionEvents}">
    <table>
        <thead class="thead">
        <tr>
            <td class="thead-icon"><img src="images/${sameDescription.kindOfSport}.png" alt="img"></td>
            <td class="match-name" colspan="8">${sameDescription.description}</td>
        </tr>
        </thead>
        <tbody class="tbody">
            <c:forEach var="event" items="${sameDescription.events}">
            <form role="form" method="post" action="/controller" id="${event.id}_edit">
                <input name="command" value="edit_match" hidden>
                <input name="eventID" value="${event.id}" hidden>
                <tr>
                    <td class="thead-icon">
                        <c:choose>
                            <c:when test="${event.eventType eq 'sport'}">
                                <select name="kindOfSport" required>
                                    <option value="football"><fmt:message key="type.football"/></option>
                                    <option value="basketball"><fmt:message key="type.basketball"/></option>
                                    <option value="volley"><fmt:message key="type.volleyball"/></option>
                                    <option value="hockey"><fmt:message key="type.hockey"/></option>
                                </select><br>
                            </c:when>
                            <c:otherwise>
                                <select name="kindOfSport" required>
                                    <option value="dota"><fmt:message key="type.dota"/></option>
                                    <option value="wot"><fmt:message key="type.wot"/></option>
                                    <option value="cs"><fmt:message key="type.cs"/></option>
                                </select>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td class="score"><input class="score-input" type="text" name="description" value="${sameDescription.description}" required autocomplete="off"></td>
                    <td class="score"><input class="score-input" type="text" name="first_competitor" value="${event.firstCompetitor}" required autocomplete="off"></td>
                    <td class="score"><input class="score-input" type="text" name="second_competitor" value="${event.secondCompetitor}" required autocomplete="off"></td>
                    <td class="score"><input class="score-input" id="matchDate" name="date" type="date" required></td>
                    <td class="score"><input class="score-input" type="time" name="time" id="matchTime" required></td>
                    <td class="match-time">
                        <fmt:formatDate value="${event.startDate}" type="date"/>
                        <fmt:formatDate value="${event.startTime}" type="time" timeStyle="SHORT"/>
                    </td>
                    <td class="coef">
                        <fmt:message key="save" var="save"/>
                            <input type="submit" value="${save}" id="input-save">
                            <c:if test="${event.id eq eventID}">
                                ${not empty fail ? "&#10006;" : "&#10004;"}
                            </c:if>
                        </a>
                    </td>
                    <td class="coef">
                        <a href="/controller?command=delete_match&eventID=${event.id}"><fmt:message key="delete"/></a>
                    </td>
                </tr>
            </form>
            </c:forEach>
        </tbody>
        </c:forEach>
    </table>
</div>

<%@include file="/WEB-INF/jspf/adminActions.jspf"%>
</body>
</html>