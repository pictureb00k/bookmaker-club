<%@include file="/WEB-INF/jspf/root.jspf"%>
<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<body onload="date()">
<%@include file="/WEB-INF/jspf/content.jspf"%>
<c:set var="page" value="${pageContext.request.contextPath}/controller?command=redirect_to_main_page" scope="session"/>
<%@include file="/WEB-INF/jspf/headerAdmin.jspf"%>

<div class="head" id="main">
    <table>
        <tr>
            <th colspan="2"><fmt:message key="event.notPlayed"/></th>
            <th>1</th>
            <th>X</th>
            <th>2</th>
            <th>1X</th>
            <th>12</th>
            <th>X2</th>
        </tr>
    </table>
</div>

<div class="sport-info">
    <c:forEach var="sameDescription" items="${sameDescriptionEvents}">
    <table id="table">
        <thead class="thead">
        <tr>
            <td class="thead-icon"><img src="images/${sameDescription.kindOfSport}.png" alt="img"></td>
            <td class="match-name" colspan="8">${sameDescription.description}</td>

        </tr>
        </thead>
        <tbody class="tbody">
        <c:forEach var="event" items="${sameDescription.events}">
            <tr>
                <td class="thead-icon">${event.id}</td>
                <td class="match-name">${event.firstCompetitor} - ${event.secondCompetitor}</td>
                <td class="match-time">
                    <fmt:formatDate value="${event.startDate}" type="date"/>
                    <fmt:formatDate value="${event.startTime}" type="time" timeStyle="SHORT"/>
                </td>
                <td class="coef">${event.coefficients.winFirst}</td>
                <td class="coef">${event.coefficients.nobody}</td>
                <td class="coef">${event.coefficients.winSecond}</td>
                <td class="coef">${event.coefficients.firstOrNobody}</td>
                <td class="coef">${event.coefficients.firstOrSecond}</td>
                <td class="coef">${event.coefficients.secondOrNobody}</td>
            </tr>
        </c:forEach>
        </tbody>
        </c:forEach>
    </table>
</div>
<%@include file="/WEB-INF/jspf/adminActions.jspf"%>
</body>
</html>