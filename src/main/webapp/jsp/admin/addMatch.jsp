<%@include file="/WEB-INF/jspf/root.jspf"%>
<%@include file="/WEB-INF/jspf/account.jspf"%>
<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<body onload="date()">
<%@include file="/WEB-INF/jspf/content.jspf"%>
<c:set var="page" value="${pageContext.request.contextPath}/jsp/admin/addMatch.jsp" scope="session"/>
<%@include file="/WEB-INF/jspf/headerAdmin.jspf"%>
<div class="head" id="main">
    <table>
        <tr>
            <th colspan="5"><fmt:message key="admin.add"/></th>
        </tr>
    </table>
</div>
<div class="addingSport">
    <h1><fmt:message key="admin.add.sport"/></h1>

    <ctg:info-tag message="${sportFail}">
        <h2 class="error"><fmt:message key="${sportFail}"/></h2>
    </ctg:info-tag>

    <ctg:info-tag message="${sportSuccess}">
        <h2 class="success"><fmt:message key="${sportSuccess}"/></h2>
    </ctg:info-tag>

    <form role="form" method="post" action="/controller" id="add">
        <input type="hidden" name="command" value="add_match">
        <input type="hidden" name="event_type" value="sport">
        <select name="kindOfSport" required>
            <option value="football" selected><fmt:message key="type.football"/></option>
            <option value="basketball"><fmt:message key="type.basketball"/></option>
            <option value="volley"><fmt:message key="type.volleyball"/></option>
            <option value="hockey"><fmt:message key="type.hockey"/></option>
        </select><br>
        <fmt:message key="bet.description" var="descr"/>
        <input id="description" type="text" name="description" placeholder="${descr}" required autocomplete="off"><br>
        <fmt:message key="first.compet" var="fc"/>
        <input type="text" name="first_competitor" placeholder="${fc}" required autocomplete="off"><br>
        <fmt:message key="second.compet" var="sc"/>
        <input type="text" name="second_competitor" placeholder="${sc}" required autocomplete="off"><br>
        <input id="matchDate" name="date" type="date" required>
        <input type="time" name="time" id="matchTime" required>
        <div class="add-img">
            <input type="submit" value="">
        </div>
    </form>
</div>
<div class="addingCybersport">
    <h1><fmt:message key="admin.add.cyber"/></h1>

    <ctg:info-tag message="${cyberFail}">
        <h2 class="error"><fmt:message key="${cyberFail}"/></h2>
    </ctg:info-tag>

    <ctg:info-tag message="${cyberSuccess}">
        <h2 class="success"><fmt:message key="${cyberSuccess}"/></h2>
    </ctg:info-tag>

    <form role="form" method="post" action="/controller" id="addCyber">
        <input type="hidden" name="command" value="add_match">
        <input type="hidden" name="event_type" value="cybersport">
        <select name="kindOfSport" required>
            <option value="dota" selected><fmt:message key="type.dota"/></option>
            <option value="wot"><fmt:message key="type.wot"/></option>
            <option value="cs"><fmt:message key="type.cs"/></option>
        </select>
        <fmt:message key="bet.description" var="descr"/>
        <input id="description" type="text" name="description" placeholder="${descr}" required autocomplete="off"><br>
        <fmt:message key="first.compet" var="fc"/>
        <input type="text" name="first_competitor" placeholder="${fc}" required autocomplete="off"><br>
        <fmt:message key="second.compet" var="sc"/>
        <input type="text" name="second_competitor" placeholder="${sc}" required autocomplete="off"><br>
        <input type="date" name="date" required id="matchDate">
        <input type="time" name="time" required id="matchTime">
        <div class="add-img">
            <input type="submit" value="" id="second">
        </div>
    </form>
</div>

<%@include file="/WEB-INF/jspf/adminActions.jspf"%>
</body>
</html>