<%@include file="/WEB-INF/jspf/root.jspf"%>
<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<body onload="date()">
<%@include file="/WEB-INF/jspf/content.jspf"%>
<c:set var="page" value="${pageContext.request.contextPath}/controller?command=put_result_page" scope="session"/>
<%@include file="/WEB-INF/jspf/headerAdmin.jspf"%>

<div class="head" id="main">
    <table>
        <tr>
            <th colspan="4"><fmt:message key="link.results"/></th>
            <th class="th-results"><fmt:message key="first.compet"/></th>
            <th class="th-results"><fmt:message key="second.compet"/></th>
            <th></th>
        </tr>
    </table>
</div>

<div class="sport-info">
    <c:forEach var="sameDescription" items="${sameDescriptionEvents}">
    <table>
        <thead class="thead">
        <tr>
            <td class="thead-icon"><img src="images/${sameDescription.kindOfSport}.png" alt="img"></td>
            <td class="match-name" colspan="5">${sameDescription.description}</td>
        </tr>
        </thead>
        <tbody class="tbody">
        <c:forEach var="event" items="${sameDescription.events}">
            <tr>
                <td class="thead-icon">${event.id}</td>
                <td class="match-name">${event.firstCompetitor} - ${event.secondCompetitor}</td>
                <td class="match-time">
                    <fmt:formatDate value="${event.startDate}" type="date"/>
                    <fmt:formatDate value="${event.startTime}" type="time" timeStyle="SHORT"/>
                </td>
                <form role="form" method="get" action="/controller" id="${event.id}">
                    <input name="command" value="put_results" hidden>
                    <input name="eventID" value="${event.id}" hidden>
                    <td class="score"><input class="score-input" name="score-first" type="number" placeholder="0" min="0" step="1" required></td>
                    <td class="score"><input class="score-input" name="score-second" type="number" placeholder="0" min="0" step="1" required></td>
                    <td class="coef">
                        <a href="#" onclick="document.getElementById('${event.id}').submit()"><fmt:message key="save"/></a>
                        <c:if test="${event.id eq eventID}">
                            &#10006;
                        </c:if>
                    </td>
                </form>
            </tr>
        </c:forEach>
        </tbody>
        </c:forEach>
    </table>
</div>

<%@include file="/WEB-INF/jspf/adminActions.jspf"%>
</body>
</html>