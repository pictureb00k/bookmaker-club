<%@include file="/WEB-INF/jspf/root.jspf"%>
<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<body onload="date()">
<%@include file="/WEB-INF/jspf/content.jspf"%>
<c:set var="page" value="/controller?command=redirect_to_bills_status" scope="session"/>
<%@include file="/WEB-INF/jspf/headerAdmin.jspf"%>
<div class="head" id="main">
    <table>
        <tr>
            <th colspan="5"><fmt:message key="admin.bills"/></th>
        </tr>
    </table>
</div>
<div class="bills-info">
        <div class="bill-div">
            <table>
            <c:forEach var="bill" items="${bills}">
                <tr>
                    <td><fmt:message key="bill.status"/> (${bill.currency}):</td>
                    <td><span><fmt:formatNumber value="${bill.balance}"/> ${bill.currency}</span></td>
                    <td><a class="show-admin" href="/controller?command=admin_transactions&currency=${bill.currency}">
                        <fmt:message key="bill.btn.show"/></a></td>
                </tr>
            </c:forEach>
                <form role="form" method="post" action="/controller">
                    <input type="hidden" name="command" value="show_account_bets">
                    <tr>
                        <td><label><fmt:message key="placeholder.login"/></label></td>
                        <td>
                            <select name="login" id="select-login" required>
                                <c:forEach var="account" items="${accounts}">
                                    <option value="${account.login}">${account.login}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td>
                            <fmt:message key="bill.btn.show" var="show"/>
                            <input type="submit" class="show-admin-submit" value="${show}">
                        </td>
                    </tr>
                </form>
            </table>
        </div>
</div>


<div class="admin-bills-info">
    <div class="no-bets-block" id="show-transactions"><p><fmt:message key="show.trans"/></p></div>
    <table>
        <thead class="thead" id="balance-info-thead" hidden>
        <tr>
            <td><fmt:message key="placeholder.login"/></td>
            <td><fmt:message key="bet.match"/></td>
            <td><fmt:message key="date"/> <fmt:message key="client.bet"/></td>
            <td><fmt:message key="result"/></td>
        </tr>
        </thead>
            <tbody id="balance-info-tbody">
            <c:set var="flag" value="false"/>
            <c:forEach var="bet" items="${bets}">
                <c:if test="${flag eq false}">
                    <script type="text/javascript">
                        document.getElementById("show-transactions").classList.add("hidden");
                        document.getElementById("balance-info-thead").classList.add("show");
                    </script>
                    <c:set var="flag" value="true"/>
                </c:if>
            <tr>
                <td>${bet.accountLogin}</td>
                <td style="width: 220px"><img id="betImage" src="${pageContext.request.contextPath}/images/${bet.event.kindOfSport}.png" style="width: 30px">
                    <span id="match-value">${bet.event.firstCompetitor} - ${bet.event.secondCompetitor}</span>
                </td>
                <td>
                    <fmt:formatDate value="${bet.betDate}" type="date" dateStyle="FULL"/>
                    <fmt:formatDate value="${bet.betTime}" type="time"/>
                </td>
                <td>
                    <c:choose>
                        <c:when test="${bet.result == -1}">
                            Playing
                        </c:when>
                        <c:when test="${bet.result == 1}">
                            <fmt:message key="bet.status.win"/>: <span id="win-money">+${bet.expectedWin} ${account.bill.currency}</span>
                        </c:when>
                        <c:when test="${bet.result == 0}">
                            <fmt:message key="bet.status.loss"/>: <span id="lose-money">-${bet.amount} ${account.bill.currency}</span>
                        </c:when>
                    </c:choose>
                </td>
            </tr>
            </c:forEach>
            </tbody>
    </table>
</div>
<%@include file="/WEB-INF/jspf/adminActions.jspf"%>
</body>
</html>