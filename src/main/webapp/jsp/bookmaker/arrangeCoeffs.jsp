<%@include file="/WEB-INF/jspf/root.jspf"%>
<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<body onload="date()">
<%@include file="/WEB-INF/jspf/content.jspf"%>
<c:set var="page" value="${pageContext.request.contextPath}/controller?command=redirect_to_main_page" scope="session"/>
<%@include file="/WEB-INF/jspf/headerBookmaker.jspf"%>

<div class="head" id="main">
    <table>
        <tr>
            <th colspan="2"><fmt:message key="event.noCoeffs"/></th>
            <th>1</th>
            <th>X</th>
            <th>2</th>
            <th>1X</th>
            <th>12</th>
            <th>X2</th>
            <th></th>
        </tr>
    </table>
</div>

<div class="sport-info">
    <div class="sport-info-text" id="info-text">
        <p><fmt:message key="info.nocoeffs"/></p>
    </div>
    <c:forEach var="sameDescription" items="${sameDescriptionEvents}">
    <script type="text/javascript" src="../js/hide-text.js"></script>

    <table>
        <thead class="thead">
        <tr>
            <td class="thead-icon"><img src="images/${sameDescription.kindOfSport}.png" alt="img"></td>
            <td class="match-name" colspan=9">${sameDescription.description}</td>
        </tr>
        </thead>
        <tbody class="tbody">
        <c:forEach var="event" items="${sameDescription.events}">
            <tr>
                <td class="thead-icon">${event.id}</td>
                <td class="match-name">${event.firstCompetitor} - ${event.secondCompetitor}</td>
                <td class="match-time" id="arrange">
                    <fmt:formatDate value="${event.startDate}" type="date"/>
                    <fmt:formatDate value="${event.startTime}" type="time" timeStyle="SHORT"/>
                </td>
                <form role="form" method="post" action="/controller" id="${event.id}">
                    <input hidden name="command" value="arrange_coeffs">
                    <input hidden name="eventID" value="${event.id}">
                    <td class="coef">
                        <input class="coef-input" name="win_first" type="number" placeholder="1" step="0.01" min="1">
                    </td>
                    <td class="coef">
                        <input class="coef-input" name="nobody" type="number" placeholder="X" step="0.01" min="1">
                    </td>
                    <td class="coef">
                        <input class="coef-input" name="win_second" type="number" placeholder="2" step="0.01" min="1">
                    </td>
                    <td class="coef">
                        <input class="coef-input" name="first_or_nobody" type="number" placeholder="1X"  step="0.01" min="1">
                    </td>
                    <td class="coef">
                        <input class="coef-input" name="first_or_second" type="number" placeholder="12" step="0.01" min="1">
                    </td>
                    <td class="coef">
                        <input class="coef-input" name="second_or_nobody" type="number" placeholder="X2" step="0.01" min="1">
                    </td>
                    <td class="coef">
                        <a href="#" onclick="document.getElementById('${event.id}').submit()"><fmt:message key="save"/></a>
                    </td>
                </form>
            </tr>
        </c:forEach>
        </tbody>
        </c:forEach>
    </table>
</div>

<%@include file="/WEB-INF/jspf/bookmakerActions.jspf"%>
</body>
</html>