<%@include file="/WEB-INF/jspf/root.jspf"%>
<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<body onload="date()">
<%@include file="/WEB-INF/jspf/content.jspf"%>
<c:set var="page" value="${pageContext.request.contextPath}/controller?command=redirect_to_update" scope="session"/>
<%@include file="/WEB-INF/jspf/headerBookmaker.jspf"%>

<div class="head" id="main">
    <table>
        <tr>
            <th colspan="2"><fmt:message key="event.update"/></th>
            <th>1</th>
            <th>X</th>
            <th>2</th>
            <th>1X</th>
            <th>12</th>
            <th>X2</th>
            <th></th>
        </tr>
    </table>
</div>

<div class="sport-info">
    <c:forEach var="sameDescription" items="${sameDescriptionEvents}">
    <table>
        <thead class="thead">
        <tr>
            <td class="thead-icon"><img src="images/${sameDescription.kindOfSport}.png" alt="img"></td>
            <td class="match-name" colspan="9">${sameDescription.description}</td>
        </tr>
        </thead>
        <tbody class="tbody">
        <c:forEach var="event" items="${sameDescription.events}">
            <tr>
                <td class="thead-icon">${event.id}</td>
                <td class="match-name">${event.firstCompetitor} - ${event.secondCompetitor}</td>
                <td class="match-time">
                    <fmt:formatDate value="${event.startDate}" type="date"/>
                    <fmt:formatDate value="${event.startTime}" type="time" timeStyle="SHORT"/>
                </td>
                <form role="form" method="post" action="/controller" id="${event.id}">
                    <input hidden name="command" value="update_coeffs">
                    <input hidden name="event_id" value="${event.id}">
                    <td class="coef">
                        <input class="coef-input" name="win_first" type="number" value="${event.coefficients.winFirst}" min="1" step="0.01" >
                    </td>
                    <td class="coef">
                        <input class="coef-input" name="nobody" type="number" value="${event.coefficients.nobody}" min="1" step="0.01" >
                    </td>
                    <td class="coef">
                        <input class="coef-input" name="win_second" type="number" value="${event.coefficients.winSecond}" min="1" step="0.01" >
                    </td>
                    <td class="coef">
                        <input class="coef-input" name="first_or_nobody" type="number" value="${event.coefficients.firstOrNobody}" min="1" step="0.01" >
                    </td>
                    <td class="coef">
                        <input class="coef-input" name="first_or_second" type="number" value="${event.coefficients.firstOrSecond}" min="1" step="0.01" >
                    </td>
                    <td class="coef">
                        <input class="coef-input" name="second_or_nobody" type="number" value="${event.coefficients.secondOrNobody}" min="1" step="0.01" >
                    </td>
                    <td class="coef">
                        <a href="#" onclick="document.getElementById('${event.id}').submit()"><fmt:message key="event.upd"/>
                            <c:if test="${event.id eq eventID}">
                                ${not empty fail ? "&#10006;" : "&#10004;"}
                            </c:if>
                        </a>
                    </td>
                </form>
            </tr>
        </c:forEach>
        </tbody>
        </c:forEach>
    </table>
</div>

<%@include file="/WEB-INF/jspf/bookmakerActions.jspf"%>
</body>
</html>