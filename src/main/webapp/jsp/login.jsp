<%@include file="/WEB-INF/jspf/root.jspf"%>

<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/login.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/register.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>

<body>
    <div class="logo" id="main-logo">
        <a href="">
            <img src="${pageContext.request.contextPath}/images/logo.png" alt="logo"></a>
    </div>

    <div class="lang" id="main-lang">
        <a href="#openLang">
            <div class="val"><fmt:message key="site.language"/></div>
        </a>
    </div>

    <div class="header" id="main-header">
        <div class="links">
            <div class="link"><a href=""><fmt:message key="link.sport"/> </a></div>
            <div class="link"><a href=""><fmt:message key="link.cyber"/> </a></div>
            <div class="link"><a href=""><fmt:message key="link.results"/> </a></div>
        </div>
        <div class="authorization">
            <a href="${pageContext.request.contextPath}/jsp/login.jsp">
                <span><fmt:message key="link.login"/></span>
            </a>
            /
            <a href="${pageContext.request.contextPath}/jsp/register.jsp">
                <fmt:message key="link.register"/>
            </a>
        </div>
        <div class="loginDialog">
            <div class="log-in">
                <a href="/index.jsp" title="Close" class="close">X</a>
                <div class="login-register">
                    <a><fmt:message key="link.login"/> </a> /
                    <a href="${pageContext.request.contextPath}/jsp/register.jsp"><fmt:message key="link.register"/> </a>
                </div>
                <h1><fmt:message key="header.login"/></h1>
                <ctg:info-tag message="${success}">
                    <h3 id="green"><fmt:message key="${success}"/></h3>
                </ctg:info-tag>
                <ctg:info-tag message="${fail}">
                    <h3 id="red"><fmt:message key="${fail}"/></h3>
                </ctg:info-tag>
                <form role="form" method="post" action="/controller">
                    <input type="hidden" name="command" value="login"/>
                    <fmt:message key="placeholder.login" var="login"/>
                    <input class="user" type="text" name="login" placeholder="${login}" autocomplete="off"/>
                    <fmt:message key="placeholder.pass" var="pass"/>
                    <input class="pass" type="password" name="password" placeholder="${pass}" autocomplete="off"/>
                    <fmt:message key="btn.sign-in" var="in"/>
                    <input type="submit" class="btn" value="${in}">
                </form>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="picture"></div>
        <div class="wrap-text">
            <div class="wrap-text-value">
                <fmt:message key="hello.dep"/>
            </div>
        </div>
        <div class="wrap-info">
            <h1><fmt:message key="hello.msg"/></h1>
            <div class="wrap-info-value">
                <fmt:message key="hello.text"/>
            </div>
        </div>
        <div class="wrap-sport">
            <a href="">
                <div class="wrap-sport-value">
                    <fmt:message key="client.bet"/><br><fmt:message key="link.sport"/>
                </div>
            </a>
        </div>
        <div class="wrap-cybersport">
            <a href="">
                <div class="wrap-cybersport-value">
                    <fmt:message key="client.bet"/> <br><fmt:message key="link.cyber"/>
                </div>
            </a>
        </div>
        <div class="wrap-pay-methods">
            <table class="wrap-pay-methods-value">
                <tr>
                    <th><fmt:message key="hello.dep.method"/></th>
                    <th><img src="${pageContext.request.contextPath}/images/skrill.png"></th>
                    <th><img src="${pageContext.request.contextPath}/images/webmoney.png"></th>
                    <th><img src="${pageContext.request.contextPath}/images/visa.png"></th>
                    <th><img src="${pageContext.request.contextPath}/images/mastercard.png"></th>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>