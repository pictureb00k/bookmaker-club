<%@include file="/WEB-INF/jspf/root.jspf"%>

<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/login.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/register.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>

<body>
    <div class="logo" id="main-logo">
        <a href="">
            <img src="${pageContext.request.contextPath}/images/logo.png" alt="logo"></a>
    </div>

    <div class="lang" id="main-lang">
        <a href="#openLang">
            <div class="val"><fmt:message key="site.language"/></div>
        </a>
    </div>

    <div class="header" id="main-header">
        <div class="links">
            <div class="link"><a href=""><fmt:message key="link.sport"/> </a></div>
            <div class="link"><a href=""><fmt:message key="link.cyber"/> </a></div>
            <div class="link"><a href=""><fmt:message key="link.results"/> </a></div>
        </div>
        <div class="authorization">
            <a href="#">
                <span><fmt:message key="link.login"/></span>
            </a>
            /
            <a href="#">
                <fmt:message key="link.register"/>
            </a>
        </div>

        <div class="registerDialog">
            <div class="reg-in">
                <a href="/index.jsp" title="Close" class="close">X</a>
                <div class="register-login">
                    <a href="${pageContext.request.contextPath}/jsp/login.jsp"><fmt:message key="link.login"/> </a> /
                    <a><fmt:message key="link.register"/> </a>
                </div>
                <h1><fmt:message key="registration.header"/> </h1>
                <h3><fmt:message key="reg.information"/> <br>
                    <fmt:message key="reg.information2"/> </h3>
                <c:if test="${not empty fail}">
                    <h3 class="register-error"><fmt:message key="${fail}"/></h3>
                </c:if>
                <form role="form" method="post" action="/controller">
                    <input type="hidden" name="command" value="register">
                    <fmt:message key="placeholder.login" var="login"/>
                    <input class="login-reg" type="text" name="login" placeholder="${login}" required autocomplete="off" pattern="[A-z0-9_-]{3,16}"
                           title = "Choose your login from 3 to 16 characters">
                    <fmt:message key="placeholder.pass" var="pass"/>
                    <input class="pass-reg" type="password" name="password" placeholder="${pass}" required autocomplete="off" pattern="[A-z0-9_-]{6,18}"
                           title="Choose your password. Six or more characters">
                    <fmt:message key="pass.confirm" var="confirm"/>
                    <input class="confirm-pass" type="password" name="confirm" placeholder="${confirm}" required autocomplete="off" pattern="[A-z0-9_-]{6,18}"
                           title="Choose your password. Six or more characters">
                    <fmt:message key="placeholder.email" var="email"/>
                    <input type="email" class="email" name="email" placeholder="${email}" autocomplete="off"
                           pattern="[A-z0-9_\.-]+@[a-z]+\.[a-z]{2,4}" required title="Enter your email">
                    <fmt:message key="placeholder.firstName" var="name"/>
                    <input class="first-name" type="text" name="first-name" placeholder="${name}" required autocomplete="off"
                           pattern="[A-zА-я]+">
                    <fmt:message key="placeholder.lastName" var="surname"/>
                    <input class="last-name" type="text" id="last-name" name="last-name" placeholder="${surname}" required autocomplete="off"
                           pattern="[A-zА-я]+">
                    <input class="bday" type="date" name="birthday" max="2000-01-01" required>
                    <select class="regSelectCurrency" name="currency" required>
                        <option value="USD">USD. US Dollar</option>
                        <option value="EUR">EUR. Euro</option>
                        <option value="RUB">RUB. Russian Ruble</option>
                        <option value="BYN">BYN. Belarus Ruble</option>
                    </select>
                    <fmt:message key="btn.sign-up" var="reg"/>
                    <input type="submit" class="reg-btn" value="${reg}">
                </form>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="picture"></div>
        <div class="wrap-text">
            <div class="wrap-text-value">
                <fmt:message key="hello.dep"/>
            </div>
        </div>
        <div class="wrap-info">
            <h1><fmt:message key="hello.msg"/></h1>
            <div class="wrap-info-value">
                <fmt:message key="hello.text"/>
            </div>
        </div>
        <div class="wrap-sport">
            <a href="">
                <div class="wrap-sport-value">
                    <fmt:message key="client.bet"/><br><fmt:message key="link.sport"/>
                </div>
            </a>
        </div>
        <div class="wrap-cybersport">
            <a href="">
                <div class="wrap-cybersport-value">
                    <fmt:message key="client.bet"/> <br><fmt:message key="link.cyber"/>
                </div>
            </a>
        </div>
        <div class="wrap-pay-methods">
            <table class="wrap-pay-methods-value">
                <tr>
                    <th><fmt:message key="hello.dep.method"/></th>
                    <th><img src="${pageContext.request.contextPath}/images/skrill.png"></th>
                    <th><img src="${pageContext.request.contextPath}/images/webmoney.png"></th>
                    <th><img src="${pageContext.request.contextPath}/images/visa.png"></th>
                    <th><img src="${pageContext.request.contextPath}/images/mastercard.png"></th>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>