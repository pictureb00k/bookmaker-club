<%@include file="/WEB-INF/jspf/root.jspf"%>

<!DOCTYPE html>
<html lang=${language}>
<head>
    <title>Tottaly win</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lang.css">
    <script type="text/javascript" src="../js/scripts.js"></script>
</head>
<body onload="date()">
<%@include file="/WEB-INF/jspf/content.jspf"%>
<c:set var="page" value="${pageContext.request.contextPath}/controller?command=redirect_to_cybersport_page" scope="session"/>
<div class="header">
    <div class="links">
        <div class="link"><a href="/controller?command=redirect_to_sport_page"><fmt:message key="link.sport"/> </a></div>
        <div class="link"><a href="/controller?command=redirect_to_cybersport_page"><fmt:message key="link.cyber"/> </a></div>
        <div class="link"><a href="/controller?command=results_page"><fmt:message key="link.results"/> </a></div>
    </div>

    <div class="authorization">
        <a href="${pageContext.request.contextPath}/jsp/login.jsp">
            <img src="${pageContext.request.contextPath}/images/log.png" alt="Login" class="authorization-icon">
            <span><fmt:message key="link.login"/></span>
        </a>
        /
        <a href="${pageContext.request.contextPath}/jsp/register.jsp">
            <img src="${pageContext.request.contextPath}/images/acc.png" alt="Register" class="authorization-icon">
            <fmt:message key="link.register"/>
        </a>
    </div>
</div>

<div class="head" id="main">
    <table>
        <tr>
            <th colspan="2"><fmt:message key="event"/></th>
            <th>1</th>
            <th>X</th>
            <th>2</th>
            <th>1X</th>
            <th>12</th>
            <th>X2</th>
        </tr>
    </table>
</div>

<div class="sport-info">
    <div class="sport-info-text" id="info-text">
        <p><fmt:message key="info.matches"/></p>
    </div>
    <c:forEach var="sameDescription" items="${sameDescriptionEvents}">
    <script type="text/javascript" src="../js/hide-text.js"></script>
    <table>
        <thead class="thead">
        <tr>
            <td class="thead-icon"><img src="images/${sameDescription.kindOfSport}.png" alt="img"></td>
            <td class="match-name">${sameDescription.description}</td>
            <td class="match-time"></td>
            <td class="coef"></td>
            <td class="coef"></td>
            <td class="coef"></td>
            <td class="coef"></td>
            <td class="coef"></td>
            <td class="coef"></td>
        </tr>
        </thead>
        <tbody class="tbody">
        <c:forEach var="event" items="${sameDescription.events}">
            <tr>
                <td class="thead-icon">${event.id}</td>
                <td class="match-name">${event.firstCompetitor} - ${event.secondCompetitor}</td>
                <td class="match-time">
                    <fmt:formatDate value="${event.startDate}" type="date"/>
                    <fmt:formatDate value="${event.startTime}" type="time" timeStyle="SHORT"/>
                </td>
                <td class="coef"><a href="/controller?command=redirect_To_Login_Page">
                        ${event.coefficients.winFirst}</a></td>
                <td class="coef"><a href="/controller?command=redirect_To_Login_Page">
                        ${event.coefficients.nobody}</a></td>
                <td class="coef"><a href="/controller?command=redirect_To_Login_Page">
                        ${event.coefficients.winSecond}</a></td>
                <td class="coef"><a href="/controller?command=redirect_To_Login_Page">
                        ${event.coefficients.firstOrNobody}</a></td>
                <td class="coef"><a href="/controller?command=redirect_To_Login_Page">
                        ${event.coefficients.firstOrSecond}</a></td>
                <td class="coef"><a href="/controller?command=redirect_To_Login_Page">
                        ${event.coefficients.secondOrNobody}</a></td>
            </tr>
        </c:forEach>
        </tbody>
        </c:forEach>
    </table>
</div>
<%@include file="/WEB-INF/jspf/cybersports.jspf"%>
</body>
</html>