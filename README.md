# Totalizator online - sports and cybersports betting. #

Welcome to our Web site, the best online bookmaker's shop. This Web site is a reliable source of the line of official international sports events, and their results, on which can place sports bet online. Now, your extensive knowledge, observation skill and analysis of sports events, will produce results. If your choice is correct, we are happy to increase your balance. Here, you will find a variety of odds and offers, ranging from classic betting on football, hockey, basketball or betting on volleyball, handball, as well as betting on e-sports and disciplines, such as Dota 2, CS GO, or World of Tanks. It is quite simple to place a bet. If your age is over 18, You shall get registered on the Web site, filling in the fields offered. After the first authorization, please, top up your account bill. And then, you can plunge into the world of online sports betting.

### Types of users: ###

* Guest
* Client
* Bookmaker
* Administrator


### Guest can do: ###

* Watch the list of actual matches and their coefficients.
* Watch the results of ended matches.
* Pass the authorization or registration.
* Change the interface language between two languages (English, Russian).


### Client can do: ###

* Watch the list of actual matches and their coefficients.
* Watch results of ended matches.
* Change the interface language between two languages (English, Russian).
* Make a deposit using different payment systems.
* Make a new betting on match if it is actual yet.
* Cancel the betting if the match doesn’t start yet.
* Watch the results of matches and own bets. 
* Make a withdraw using different payment systems.
* Watch the list of operations with the bill. 
* Change password.

### Bookmaker can do: ###

* Approve the information about the new match and arrange the coefficients to it.
* Update the coefficients on the match if it doesn't start yet.
* Change the interface language between two languages (English, Russian).

### Administrator can do: ###
* Watch the list of actual matches.
* Change the interface language between two languages (English, Russian).
* Create the new match and send it to further Bookmaker's approving.
* Update the match's information if it isn't approved yet.
* Arrange the results for the ended matches. 
* Watch all bills' statuses and all operations with them.
