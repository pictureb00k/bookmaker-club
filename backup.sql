-- MySQL dump 10.13  Distrib 5.7.13, for Win64 (x86_64)
--
-- Host: localhost    Database: totalizator
-- ------------------------------------------------------
-- Server version	5.7.13-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `login` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `creating_date` datetime NOT NULL,
  `role` enum('ADMIN','CLIENT','BOOKMAKER') CHARACTER SET utf8 NOT NULL,
  `first_name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES ('Admin','15c7cad2701a201161a0a168d930884','rrrizon@gmail.com','1996-07-12','2016-08-17 23:50:39','ADMIN','Pavel','Bortnik'),('Bookmaker','15c7cad2701a201161a0a168d930884','rizom@mail.ru','1996-07-12','2016-08-25 22:51:44','BOOKMAKER','Pavel','Bortnik'),('MrOlympia','38e27eb250bd3fe54ff75dacddef618e','mr.olympia@gmail.com','1963-07-12','2016-10-01 18:01:20','CLIENT','Phil','Heath'),('Zver','15c7cad2701a201161a0a168d930884','rizon-paxa@mail.ru','1996-07-12','2016-09-21 21:32:40','CLIENT','Pavel','Bortnik');
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bet`
--

DROP TABLE IF EXISTS `bet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bet` (
  `bet_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_login` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `event_id` int(10) unsigned NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `expected_win` decimal(12,2) NOT NULL,
  `result` tinyint(1) DEFAULT '-1',
  `bet_date` datetime NOT NULL,
  `bet_coef` decimal(5,2) NOT NULL,
  `bet_type` varchar(45) CHARACTER SET utf8 NOT NULL,
  `bet_currency` enum('USD','EUR','RUB','BYN') CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`bet_id`),
  KEY `fk_bet_event_idx` (`event_id`),
  KEY `fk_bet_account_idx` (`account_login`),
  CONSTRAINT `fk_bet_acc` FOREIGN KEY (`account_login`) REFERENCES `account` (`login`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_bet_event` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bet`
--

LOCK TABLES `bet` WRITE;
/*!40000 ALTER TABLE `bet` DISABLE KEYS */;
INSERT INTO `bet` VALUES (1,'Zver',10,20.00,23.00,1,'2016-09-21 23:12:46',1.15,'First victory','USD'),(2,'Zver',1,80.00,133.60,1,'2016-09-21 23:28:03',1.67,'First victory','USD'),(3,'Zver',2,20.00,62.40,0,'2016-09-21 23:28:20',3.12,'Second victory','USD'),(4,'Zver',10,20.00,23.00,1,'2016-09-21 23:28:29',1.15,'First victory','USD'),(5,'Zver',8,30.00,39.90,0,'2016-09-21 23:28:42',1.33,'First victory','USD'),(6,'Zver',3,30.00,37.20,1,'2016-09-21 23:28:51',1.24,'First victory','USD'),(17,'Zver',19,52.25,94.57,1,'2016-09-28 23:10:16',1.81,'First victory','USD'),(18,'Zver',34,22.00,50.60,1,'2016-10-01 18:32:58',2.30,'First victory','USD'),(19,'Zver',39,20.00,33.60,0,'2016-10-01 18:33:12',1.68,'First victory','USD'),(20,'Zver',33,20.32,34.95,1,'2016-10-01 18:33:27',1.72,'First victory','USD'),(21,'Zver',27,50.00,67.50,0,'2016-10-01 18:33:54',1.35,'First victory','USD'),(22,'Zver',37,10.00,24.40,0,'2016-10-01 18:34:08',2.44,'Second victory','USD'),(23,'Zver',28,2.00,19.00,0,'2016-10-01 18:34:26',9.50,'First victory','USD'),(24,'Zver',38,58.00,204.16,-1,'2016-10-01 18:34:42',3.52,'Dead heat','USD'),(25,'Zver',31,10.00,15.50,0,'2016-10-01 18:35:17',1.55,'First victory','USD'),(26,'Zver',32,10.00,16.00,1,'2016-10-01 18:35:24',1.60,'Second victory','USD'),(27,'Zver',30,2.00,37.00,0,'2016-10-01 18:36:12',18.50,'Second victory','USD'),(29,'MrOlympia',27,50.00,67.50,0,'2016-10-01 18:38:37',1.35,'First victory','EUR'),(30,'MrOlympia',36,50.00,65.00,-1,'2016-10-01 18:38:46',1.30,'First victory','EUR'),(31,'MrOlympia',34,50.00,115.00,1,'2016-10-01 18:38:58',2.30,'First victory','EUR'),(32,'MrOlympia',33,100.00,172.00,1,'2016-10-01 18:39:10',1.72,'First victory','EUR'),(33,'Zver',36,39.55,51.41,-1,'2016-10-05 21:34:03',1.30,'First victory','USD'),(34,'Zver',40,15.00,38.85,0,'2016-10-06 13:04:26',2.59,'First victory','USD'),(35,'Zver',41,15.00,26.25,1,'2016-10-06 13:05:56',1.75,'First victory','USD'),(36,'Zver',42,5.00,15.00,1,'2016-10-06 13:06:14',3.00,'Second victory','USD'),(37,'Zver',43,15.00,24.00,1,'2016-10-06 13:06:24',1.60,'First victory','USD'),(38,'Zver',44,10.00,16.60,0,'2016-10-06 13:06:44',1.66,'Second victory','USD'),(39,'Zver',45,40.00,47.20,1,'2016-10-06 13:06:52',1.18,'Second victory','USD'),(40,'MrOlympia',40,80.00,124.00,1,'2016-10-06 13:08:26',1.55,'Second victory','EUR'),(43,'MrOlympia',42,200.00,310.00,0,'2016-10-06 13:09:26',1.55,'First victory','EUR');
/*!40000 ALTER TABLE `bet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill`
--

DROP TABLE IF EXISTS `bill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bill_currency` enum('USD','EUR','RUB','BYN') CHARACTER SET utf8 NOT NULL,
  `balance` decimal(12,2) unsigned NOT NULL DEFAULT '0.00',
  `account_login` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_bill_idx` (`account_login`),
  CONSTRAINT `fk_bill_acc` FOREIGN KEY (`account_login`) REFERENCES `account` (`login`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill`
--

LOCK TABLES `bill` WRITE;
/*!40000 ALTER TABLE `bill` DISABLE KEYS */;
INSERT INTO `bill` VALUES (1,'USD',100087.55,'Admin'),(2,'EUR',100119.00,'Admin'),(3,'BYN',100000.00,'Admin'),(4,'RUB',1000000.00,'Admin'),(5,'USD',0.00,'Bookmaker'),(6,'USD',112.45,'Zver'),(7,'EUR',124.00,'MrOlympia');
/*!40000 ALTER TABLE `bill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coef`
--

DROP TABLE IF EXISTS `coef`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coef` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `win_first` decimal(5,2) unsigned DEFAULT NULL,
  `win_second` decimal(5,2) unsigned DEFAULT NULL,
  `nobody` decimal(5,2) unsigned DEFAULT NULL,
  `first_or_nobody` decimal(5,2) unsigned DEFAULT NULL,
  `second_or_nobody` decimal(5,2) unsigned DEFAULT NULL,
  `first_or_second` decimal(5,2) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coef`
--

LOCK TABLES `coef` WRITE;
/*!40000 ALTER TABLE `coef` DISABLE KEYS */;
INSERT INTO `coef` VALUES (0,NULL,NULL,NULL,NULL,NULL,NULL),(1,1.67,5.80,4.05,1.16,1.28,2.31),(2,2.45,3.12,3.50,1.41,1.34,1.61),(3,1.24,15.00,7.00,1.03,1.12,4.45),(4,6.30,1.37,5.90,3.02,1.12,1.11),(5,1.75,3.30,4.80,1.31,1.16,2.01),(6,2.41,2.25,4.50,1.61,1.18,1.53),(7,1.21,4.23,NULL,NULL,NULL,NULL),(8,1.33,3.15,NULL,NULL,NULL,NULL),(9,1.24,3.80,NULL,NULL,NULL,NULL),(10,1.15,4.01,NULL,NULL,NULL,NULL),(11,1.48,2.39,NULL,NULL,NULL,NULL),(13,3.20,2.36,3.10,NULL,NULL,NULL),(14,1.36,3.38,NULL,NULL,NULL,NULL),(15,1.81,2.04,NULL,NULL,NULL,NULL),(16,1.75,2.13,NULL,NULL,NULL,NULL),(17,1.38,3.44,NULL,NULL,NULL,NULL),(18,3.00,2.21,3.98,NULL,NULL,NULL),(19,1.20,5.00,2.00,NULL,NULL,NULL),(20,1.35,10.50,5.60,NULL,NULL,NULL),(21,9.50,1.40,5.10,NULL,NULL,NULL),(22,3.10,2.44,3.52,NULL,NULL,NULL),(23,1.59,6.30,4.35,NULL,NULL,NULL),(24,1.30,14.50,6.60,NULL,NULL,NULL),(25,2.22,3.55,3.52,NULL,NULL,NULL),(26,1.55,2.59,NULL,NULL,NULL,NULL),(27,2.46,1.60,NULL,NULL,NULL,NULL),(28,1.83,2.06,NULL,NULL,NULL,NULL),(29,1.02,18.50,NULL,NULL,NULL,NULL),(30,1.72,2.17,NULL,NULL,NULL,NULL),(31,2.30,1.65,NULL,NULL,NULL,NULL),(32,1.68,2.24,NULL,NULL,NULL,NULL),(33,2.59,1.55,NULL,NULL,NULL,NULL),(34,1.75,2.18,NULL,NULL,NULL,NULL),(35,1.55,3.00,NULL,NULL,NULL,NULL),(36,1.60,2.46,NULL,NULL,NULL,NULL),(37,2.33,1.66,NULL,NULL,NULL,NULL),(38,5.40,1.18,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `coef` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_type` enum('sport','cybersport') CHARACTER SET utf8 DEFAULT NULL,
  `kind_of_sport` varchar(16) CHARACTER SET utf8 NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_competitor` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `second_competitor` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `first_score` smallint(5) unsigned NOT NULL DEFAULT '0',
  `second_score` smallint(5) unsigned NOT NULL DEFAULT '0',
  `start_date` datetime NOT NULL,
  `coef_id` int(10) NOT NULL DEFAULT '0',
  `is_played` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_event_coef_idx` (`coef_id`),
  CONSTRAINT `fk_event_coef` FOREIGN KEY (`coef_id`) REFERENCES `coef` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (1,'sport','football','Англия. Премьер-лига','Манчестер Юнайтед','Лестер Сити',4,1,'2016-09-24 14:30:00',1,1),(2,'sport','football','Англия. Премьер-лига','Арсенал','Челси',3,0,'2016-09-24 19:30:00',2,1),(3,'sport','football','Англия. Премьер-лига','Ливерпуль','Халл Сити',5,1,'2016-09-24 17:00:00',3,1),(4,'sport','hockey','Беларусь. Экстралига','Брест','Лида',1,4,'2016-09-22 19:00:00',4,1),(5,'sport','hockey','Canada. OHL','Flint Firebirds','Saginaw Spirit',4,0,'2016-09-22 02:00:00',5,1),(6,'sport','hockey','Canada. OHL','Windsor Spitfires','Erie Otters',4,1,'2016-09-23 02:05:00',6,1),(7,'sport','volley','Men. National teams. EC-2017. Qualification','Ukraine','Moldova',3,1,'2016-09-22 18:30:00',7,1),(8,'sport','volley','Men. National teams. EC-2017. Qualification','Slovakia','Latvia',1,3,'2016-09-22 18:30:00',8,1),(9,'sport','volley','Men. National teams. EC-2017. Qualification','Croatia','Israel',2,3,'2016-09-22 21:00:00',9,1),(10,'cybersport','cs','Counter-Strike ESL','Liquid','CLG',16,5,'2016-09-22 03:00:00',10,1),(11,'cybersport','cs','Counter-Strike ESL','OpTic','Echo Fox',16,5,'2016-09-22 03:00:00',11,1),(14,'sport','football','Лига чемпионов УЕФА. Групповой этап','Атлетико Мадрид','Бавария',1,0,'2016-09-28 21:45:00',13,1),(15,'sport','basketball','Филиппины. Мужчины. PBA. Кубок губернатора. Плей-офф. 1/2 финала. Кесон-Сити','Ток-н-Тест Тропанг Текстерс','Мералко Болтс',91,101,'2016-09-29 14:00:00',14,1),(16,'sport','hockey','КХЛ','Лада','Динамо Минск',2,3,'2016-09-28 18:00:00',18,1),(17,'sport','basketball','ВНБА. Плей-офф. 1/2 финала','Финикс Меркьюри','Миннесота Линкс',95,113,'2016-09-29 03:00:00',17,1),(18,'cybersport','dota','MarsTV лига','Team Secret','LGD.Forever Young',5,2,'2016-09-29 05:00:00',16,1),(19,'cybersport','dota','MarsTV лига','Vici Gaming','iG Vitality',2,1,'2016-09-29 08:30:00',15,1),(20,'sport','football','Testing time match','test','test',0,0,'2016-09-28 18:20:00',19,0),(27,'sport','football','Англия. Премьер-лига','Манчестер Юнайтед','Сток Сити',1,1,'2016-10-02 14:20:00',20,1),(28,'sport','football','Англия. Премьер-лига','Бернли','Арсенал',0,1,'2016-10-02 18:30:00',21,1),(29,'sport','volley','Италия. Мужчины. СуперЛига','Латина','Верона',0,3,'2016-10-02 19:00:00',28,1),(30,'sport','volley','Италия. Мужчины. СуперЛига','Модена','Сора',3,0,'2016-10-02 19:00:00',29,1),(31,'sport','basketball','НБА. Предсезонные матчи','Голден Стэйт Уорриорз','Торонто Рэпторз',93,97,'2016-10-02 02:30:00',26,1),(32,'sport','basketball','НБА. Предсезонные матчи','Даллас Маверикс','Нью-Орлеан Пеликанс',102,116,'2016-10-02 03:00:00',27,1),(33,'cybersport','dota','MarsTV лига. Матч из 3-х карт','Evil Geniuses','OG',2,1,'2016-10-02 07:00:00',30,1),(34,'cybersport','cs','ESL Ван. Матч из одной карты','Natus Vincere','SK Gaming',1,0,'2016-10-01 21:00:00',31,1),(35,'sport','football','Англия. Премьер-лига','Челси','Лестер Сити',0,0,'2016-10-15 14:30:00',23,0),(36,'sport','football','Англия. Премьер-лига','Арсенал','Суонси Сити',0,0,'2016-10-15 17:00:00',24,0),(37,'sport','football','Англия. Премьер-лига','Тоттенхэм Хотспур','Манчестер Сити',2,0,'2016-10-02 16:15:00',22,1),(38,'sport','football','Англия. Премьер-лига','Ливерпуль','Манчестер Юнайтед',0,0,'2016-10-17 22:00:00',25,0),(39,'cybersport','cs','ESL Ван. Матч из одной карты','Astralis','fnatic',0,1,'2016-10-01 19:30:00',32,1),(40,'sport','basketball','NBA. Предсезонные матчи','Чикаго Буллз','Индиана Пейсерз',108,115,'2016-10-07 02:00:00',33,1),(41,'sport','basketball','NBA. Предсезонные матчи','Вашингтон Уизардс','Филадельфия 76-е',125,119,'2016-10-07 02:00:00',34,1),(42,'sport','basketball','NBA. Предсезонные матчи','Детройт Пистонс','Бруклин Нетс',94,101,'2016-10-07 02:30:00',35,1),(43,'sport','basketball','NBA. Предсезонные матчи','Бостон Селтикс','Шарлотт Хорнетс',107,92,'2016-10-07 02:30:00',36,1),(44,'sport','basketball','NBA. Предсезонные матчи','Атланта Хокс','Мемфис Гризлиз',104,83,'2016-10-07 03:00:00',37,1),(45,'sport','basketball','NBA. Предсезонные матчи','Сакраменто Кингз','Голден Стейт Уорриорз',96,105,'2016-10-07 05:30:00',38,1);
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `date` datetime NOT NULL,
  `type` enum('skrill','webmoney','mastercard','visa') CHARACTER SET utf8 DEFAULT NULL,
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bill_id` int(11) unsigned NOT NULL,
  `trans_type` enum('Deposit','Withdraw') CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dep_bill_idx` (`bill_id`),
  CONSTRAINT `fk_dep_bill` FOREIGN KEY (`bill_id`) REFERENCES `bill` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` VALUES ('2016-09-21 23:11:19','webmoney',200.00,1,6,'Deposit'),('2016-09-26 11:35:20','skrill',16.80,2,6,'Withdraw'),('2016-10-01 18:01:40','mastercard',250.00,4,7,'Deposit'),('2016-10-04 20:30:15','skrill',7.00,5,7,'Withdraw');
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-07 14:19:33
